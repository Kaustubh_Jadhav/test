<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of RTDController
 *
 * @author cxb106u
 */
class RTDController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $lang = $this->session->userdata("lang_det");
        if (!isset($lang)) {
            $this->session->set_userdata('lang_det', "english");
        }
    }

    public function LoadNationalSummary() {

        //if ($this->session->userdata('UserData') != "") {
            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Hours = '';
            $Minutes = '';
            $Data = $this->GetNLDCCSVData($Date, $Hours, $Minutes);
            $this->session->set_userdata('DataForDateRTD', date_format(date_create($Data->Result[0][0]->Date_), 'd-M-Y h:i A'));
            $arr1[] = null;
            foreach ($Data->Result[0] as $element) {

                $arr1[] = array("Key" => $element->Key, "Value" => $element->Value);
            }
            $arr = array_combine(array_column($arr1, 'Key'), array_column($arr1, 'Value'));
            $lang = $this->session->userdata("lang_det");
            $Lang1 = $this->LangaugeChange($lang, 'NationalSummary');
            $Result1 = array('NewData' => $arr, 'Lang' => $Lang1);
            $data = array('Lang' => $Lang1);
            $this->load->view('MasterLayout/MasterHeader_RTD', $data);
            $this->load->view('RTD/NationalSummary', $Result1);
            $this->load->view('MasterLayout/MasterFooter_RTD', $data);
//        } else {
//            redirect('/login');
//        }
    }

    public function NationalSummary() {

        try {
            //if ($this->session->userdata('UserData') != "") {
                $Date = date('Y-m-d', strtotime(date('Y-m-d')));
                $Hours = '';
                $Minutes = '';
                $Data = $this->GetNLDCCSVData($Date, $Hours, $Minutes);
                $Data = (array) $Data;
                $output = array_map(function($val) {
                    return $val == 0 ? '-' : $val;
                }, array_column($Data['Result'][0], 'Value'));

                $arr = array_combine(array_column($Data['Result'][0], 'Key'), $output);
                var_dump($arr);
                $lang = $this->session->userdata("lang_det");
                $Lang1 = $this->LangaugeChange($lang, 'NationalSummary');
                $Result1 = array('NewData' => $arr, 'Lang' => $Lang1);
                $data = array('Lang' => $Lang1);


                $this->load->view('MasterLayout/MasterHeader_RTD', $data);
                $this->load->view('RTD/test_Summary', $Result1);
                $this->load->view('MasterLayout/MasterFooter_RTD', $data);
//            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            echo "Test";
        }
    }

    public function InterRegionalExchage() {
        //if ($this->session->userdata('UserData') != "") {
            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Hours = '';
            $Minutes = '';
            $Data = $this->GetNLDCCSVData($Date, $Hours, $Minutes);
            $this->session->set_userdata('DataForDateRTD', date_format(date_create($Data->Result[0][0]->Date_), 'd-M-Y h:i A'));
            $arr1[] = null;
            foreach ($Data->Result[0] as $element) {

                $arr1[] = array("Key" => $element->Key, "Value" => $element->Value);
            }
            $arr = array_combine(array_column($arr1, 'Key'), array_column($arr1, 'Value'));
            $Result1 = array('NewData' => $arr);
            $lang = $this->session->userdata("lang_det");
            $Lang1 = $this->LangaugeChange($lang, 'InterRegionalExchange');
            $data = array('Lang' => $Lang1);
            $this->load->view('MasterLayout/MasterHeader_RTD', $data);
            $this->load->view('RTD/InterRegionalExchange', $Result1);
            $this->load->view('MasterLayout/MasterFooter_RTD', $data);
//        } else {
//            redirect('/login');
//        }
    }

    public function ATCMonitoring() {
        //if ($this->session->userdata('UserData') != "") {
            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Hours = '';
            $Minutes = '';
            $Data = $this->GetNLDCCSVData($Date, $Hours, $Minutes);
            $this->session->set_userdata('DataForDateRTD', date_format(date_create($Data->Result[0][0]->Date_), 'd-M-Y h:i A'));
            $arr1[] = null;
            foreach ($Data->Result[0] as $element) {

                $arr1[] = array("Key" => $element->Key, "Value" => $element->Value);
            }
            $arr = array_combine(array_column($arr1, 'Key'), array_column($arr1, 'Value'));
            $Result1 = array('NewData' => $arr);
            $lang = $this->session->userdata("lang_det");
            $Lang1 = $this->LangaugeChange($lang, 'ATCMonitoring');
            $data = array('Lang' => $Lang1);
            $this->load->view('MasterLayout/MasterHeader_RTD', $data);
            $this->load->view('RTD/ATC_Monitoring', $Result1);
            $this->load->view('MasterLayout/MasterFooter_RTD', $data);
//        } else {
//            redirect('/login');
//        }
    }

    public function ImportantLineOfER() {
        //if ($this->session->userdata('UserData') != "") {
            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Hours = '';
            $Minutes = '';
            $Data = $this->GetNLDCCSVData($Date, $Hours, $Minutes);
            $this->session->set_userdata('DataForDateRTD', date_format(date_create($Data->Result[0][0]->Date_), 'd-M-Y h:i A'));
            $arr1[] = null;
            foreach ($Data->Result[0] as $element) {

                $arr1[] = array("Key" => $element->Key, "Value" => $element->Value);
            }
            $arr = array_combine(array_column($arr1, 'Key'), array_column($arr1, 'Value'));
            $Result1 = array('NewData' => $arr);
            $lang = $this->session->userdata("lang_det");
            $Lang1 = $this->LangaugeChange($lang, 'ImpLineOfER');
            $data = array('Lang' => $Lang1);
            $this->load->view('MasterLayout/MasterHeader_RTD', $data);
            $this->load->view('RTD/ImportantLinesOfER', $Result1);
            $this->load->view('MasterLayout/MasterFooter_RTD', $data);
//        } else {
//            redirect('/login');
//        }
    }

    //KCJ 09 Jan 2019 Use to get NLDC File data with the key value pair
    public function GetNLDCCSVData($SelectedDate, $Hours, $Minutes) {

        $curl_handle = curl_init();
        curl_setopt_array($curl_handle, array(
            CURLOPT_URL => URL . "/GetNLDCGenData",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1000,
            CURLOPT_TIMEOUT => 3000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "Date=" . $SelectedDate . "&Hours=" . $Hours . "&Minutes=" . $Minutes,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        $result = json_decode($buffer);
        $response = $result; // array('status' => 'success', 'data' => $result, 'message_' => "");
        return $response;
    }

    public function LangaugeChange($lang, $Title = '') {
        $this->lang->load('content', $lang = '' ? 'english' : $lang);
        $data['Title'] = $this->lang->line($Title);
        $data['InterRegionalTransmission'] = $this->lang->line('InterRegionalTransmission');
        $data['InterRegional'] = $this->lang->line('InterRegional');
        $data['Transmission'] = $this->lang->line('Transmission');
        $data['GenerationData'] = $this->lang->line('GenerationData');
        $data['DemandMet'] = $this->lang->line('DemandMet');
        $data['ISGS'] = $this->lang->line('ISGS');
        $data['Thermal'] = $this->lang->line('Thermal');
        $data['Hydro'] = $this->lang->line('Hydro');
        $data['Gas'] = $this->lang->line('Gas');
        $data['Nuclear'] = $this->lang->line('Nuclear');
        $data['Renewable'] = $this->lang->line('Renewable');
        $data['Solar'] = $this->lang->line('Solar');
        $data['Wind'] = $this->lang->line('Wind');
        $data['FAQ'] = $this->lang->line('FAQ');
        $data['Frequency'] = $this->lang->line('Frequency');
        $data['MW'] = $this->lang->line('MW');
        $data['Generation'] = $this->lang->line('Generation');
        $data['App'] = $this->lang->line('App');
        $data['Today'] = $this->lang->line('Today');
        $data['Yesterday'] = $this->lang->line('Yesterday');
        $data['Diff'] = $this->lang->line('Diff');
        $data['HeaderDate1'] = $this->lang->line('HeaderDate1');
        $data['HeaderDate2'] = $this->lang->line('HeaderDate2');
        $data['Northern'] = $this->lang->line('Northern');
        $data['Southern'] = $this->lang->line('Southern');
        $data['Western'] = $this->lang->line('Western');
        $data['Eastern'] = $this->lang->line('Eastern');
        $data['NorthEastern'] = $this->lang->line('NorthEastern');
        $data['Region'] = $this->lang->line('Region');
        $data['Bangladesh'] = $this->lang->line('Bangladesh');
        $data['Bhutan'] = $this->lang->line('Bhutan');
        $data['Nepal'] = $this->lang->line('Nepal');
        $data['Translation_details'] = $this->lang->line('Translation_details');
        $data['SubHeader'] = $this->lang->line('SubHeader');
        $data['Generation'] = $this->lang->line('Generation');
        $data['Home'] = $this->lang->line('Home');

        $data['ImpLineOfER'] = $this->lang->line('ImpLineOfER');
        $data['ATCMonitoring'] = $this->lang->line('ATCMonitoring');
        $data['InterRegionalExchange'] = $this->lang->line('InterRegionalExchange');
        $data['NationalSummary'] = $this->lang->line('NationalSummary');
        $data['Dashboard'] = $this->lang->line('Dashboard');
        $data['ElectricityPrice'] = $this->lang->line('ElectricityPrice');
        $data['MobileFooterMenuImage'] = $this->lang->line('MobileFooterMenuImage');
        $data['Translation_details_New'] = $this->lang->line('Translation_details_New');
        return $data;
    }

}
