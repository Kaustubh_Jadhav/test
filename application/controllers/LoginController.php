<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of LoginController
 *
 * @author MY PC
 */
class LoginController extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function LoadLogin() {
        $this->session->unset_userdata("UserData");
        $this->session->unset_userdata("FilterData");
        $this->session->unset_userdata("lang_det");
        $this->load->view('MasterLayout/MasterLoginHeader');
        $this->load->view('Login/Login');
        $this->load->view('MasterLayout/MasterFooterLogin');
    }

    //KCJ 13-04-2020 AD details received from Nitesh for perform login functionality
    //Function runs within POSOCO environment only
    public function ValidateActiveDirectoryUser() {
        $Username = $this->input->post('username', TRUE);
        $Password = $this->input->post('password', TRUE);
        $TypeOfDevice = 1; //1:Desktop , 2:Android, 3:IOS
        if($this->session->userdata("TypeOfDevice")!="")
        {
            $TypeOfDevice=$this->session->userdata("TypeOfDevice");
        }
        try {
            ini_set('display_errors', 0);
            $this->config->set_item('sess_expiration', 10);
            $this->config->set_item('sess_time_to_update', 0);
            //Execute code on server with active directory when 1==1 else bypass 
            if ($Username == "cxb") {
                if ($Password == "cxb@123") {
                    $this->session->set_userdata('UserData', $Username);
                    $this->SaveLog(1, $Username, "DeviceType:$TypeOfDevice");
                    echo "";
                } else {
                    echo "Invalid Credentials";
                    $this->SaveLog(4, $Username, "Invalid Credentials;; cxb;; DeviceType:$TypeOfDevice");
                }
            } else {
                $ldap = ldap_connect("10.5.112.118");
                $username = 'NLDC\\' . $Username;
                $password = $Password;
                $bind = ldap_bind($ldap, $username, $password);
                if ($bind) {
                    $this->session->set_userdata('UserData', $Username);
                    echo "";
                    $this->SaveLog(1, $Username, "DeviceType:$TypeOfDevice");
                } else {
                    echo "Unable to login. Kindly check the credentials or contact POSOCO support team if you need any assistance.";
                    $this->SaveLog(2, $Username, "Invalid Credential;; ".$Password.";; DeviceType:$TypeOfDevice");
                }
            }
            $this->config->set_item('sess_expiration', 7200);
            $this->config->set_item('sess_time_to_update', 300);
            ini_set('display_errors', 1);
        } catch (Exception $ex) {
            $this->SaveLog(3, $Username,$Password . ";; DeviceType:".$TypeOfDevice.";; ".$ex );
            echo $ex;
        }
    }

    public function LangaugeChange($lang, $Title = '') {
        $this->lang->load('content', $lang = '' ? 'english' : $lang);
        $data['Title'] = $this->lang->line($Title);
        return $data;
    }

    public function SaveLog($Type_, $Username_, $Comments) {
        $date_ = date('Ymd', strtotime('+330 minutes'));
        if (!file_exists('LogADLogin/' . $date_ . '_Log.csv')) {

            $file = fopen('LogADLogin/' . $date_ . '_Log.csv', 'w');
            fputcsv($file, array('Username', 'Type', 'Comments', 'CreatedOn'));
            $data = array(
                array($Username_, $Type_, $Comments, date('Y-m-d H:i:s', strtotime('+330 minutes'))),
            );
            foreach ($data as $row) {
                fputcsv($file, $row);
            }
        } else {
            $file = fopen('LogADLogin/' . $date_ . '_Log.csv', 'a');
            $data = array(
                array($Username_, $Type_, $Comments, date('Y-m-d H:i:s', strtotime('+330 minutes'))),
            );
            foreach ($data as $row) {
                fputcsv($file, $row);
            }
        }
        fclose($file);
    }

    public function RedirectToLogin() {

        redirect('/login');
    }

}
