<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Auther : KCJ 21 Sept 2018

class DashboardController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $lang = $this->session->userdata("lang_det");
        $SelectedValue = $this->session->userdata('FilterData');
        $IsMobileApp = $this->session->userdata('IsMobileApp');
        if (!isset($lang)) {
            $this->session->set_userdata('lang_det', "english");
        }
        if (!isset($IsMobileApp)) {
            $this->session->set_userdata('IsMobileApp', "0");
        }
        if (!isset($SelectedValue["SelectedDate1"])) {

            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $RegionCode = "0";
            $StateCode = "0";
            $FilterDate = ['SelectedDate1' => $Date, 'SelectedDate2' => $Date2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, "CreatedDate" => $CreatedDate];
            $this->session->set_userdata('FilterData', $FilterDate);
        } else if ($SelectedValue["CreatedDate"] <= date("Y-m-d H:i:s")) {
            //echo "Bhaubali";
            $this->session->unset_userdata("FilterData");
            $Date = date('Y-m-d', strtotime(date('Y-m-d')));
            $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
            $RegionCode = "0";
            $StateCode = "0";
            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $Date, 'SelectedDate2' => $Date2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, "CreatedDate" => $CreatedDate];
            $this->session->set_userdata('FilterData', $FilterDate);
        }
    }

    //Inter Regional Functions/Dashboard
    //KCJ 21 Sept 2018 get dashboard view with inter regional power transfer
    public function index1() {
        try {
            $this->session->set_userdata('TypeOfDevice', 0);
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $FilterData = $this->session->userdata("FilterData");
                $SelectedDate = $FilterData["SelectedDate1"];
                $Lang = $this->LangaugeChange($lang, 'InterRegionalTransmission');
                $Date = $SelectedDate;
                $RegionTransferData = $this->GetInterRegionalTransfer($Date);
                $IsDataAvailable = 0;
                if ($RegionTransferData->status == 0) {
                    $IsDataAvailable = 1;
                }
                $Date = date('Y-m-d', strtotime(date('Y-m-d')));
                $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
                $data = array('RegionTransferData' => $RegionTransferData, 'Date' => $Date, 'Lang' => $Lang, 'IsDataAvailable' => $IsDataAvailable, 'Yesterday' => $Date2, 'Today' => $Date);
                $HeaderData = array('Lang' => $Lang, 'IsMenuShow' => 1, 'IsInterRegional' => 1);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('Dashboard/dashboard', $data);
                $this->load->view('MasterLayout/MasterFooter', array($Lang, "IsOther" => 1));
//            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//KCJ created for testing purpose on live server
    public function index() {
        try {
            $this->session->set_userdata('TypeOfDevice', 0);
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $FilterData = $this->session->userdata("FilterData");
                $SelectedDate = $FilterData["SelectedDate1"];
                $Lang = $this->LangaugeChange($lang, 'InterRegionalTransmission');
                $Date = $SelectedDate;
                $RegionTransferData = $this->GetInterRegionalTransfer($Date);
                $IsDataAvailable = 0;
                if ($RegionTransferData->status == 0) {
                    $IsDataAvailable = 1;
                }
                $Date = date('Y-m-d', strtotime(date('Y-m-d')));
                $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
                $data = array('RegionTransferData' => $RegionTransferData, 'Date' => $Date, 'Lang' => $Lang, 'IsDataAvailable' => $IsDataAvailable, 'Yesterday' => $Date2, 'Today' => $Date);
                $HeaderData = array('Lang' => $Lang, 'IsMenuShow' => 1, 'IsInterRegional' => 1);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('Dashboard/dashboard', $data);
                $this->load->view('MasterLayout/MasterFooter', array($Lang, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//KCJ 26 Sept 2018 get generation data view chart details and Datatable Details
    public function LoadGenerationView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'GenerationData');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->session->set_userdata('DataForDate', "");
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/GenerationData', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
          //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//KCJ 12 Oct 2018 get inter regional power transfer data details to load after certain time interval and during page load
    public function GetInterRegionalTransfer($SelectedDate) {

        $curl_handle = curl_init();
        $CurrentTime = date('H:m:00');
        curl_setopt_array($curl_handle, array(
            CURLOPT_URL => URL . "/GetRegionTransferDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1000,
            CURLOPT_TIMEOUT => 3000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "Date_=" . $SelectedDate . "&Time_=" . $CurrentTime,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        $result = json_decode($buffer);
        $response = $result; // array('status' => 'success', 'data' => $result, 'message_' => "");
        return $response;
    }

//KCJ 29 Oct 2018 load india map view within certain interval of time.
    public function GetIndiaDetails($SelectedDate) {
        try {

            $lang = $this->session->userdata("lang_det");
            $Lang = $this->LangaugeChange($lang, 'DemandMet');
            $SelectedDate1 = $this->security->xss_clean($SelectedDate);
            //$SelectedDate2 = date('Y-m-d', strtotime($SelectedDate1 . ' -1 day'));
            //$FilterData = $this->session->userdata("FilterData");
            //$SelectedRegion = $FilterData["SelectedRegionCode"];
            //$SelectedState = $FilterData["SelectedStateCode"];
            //$this->session->unset_userdata('FilterData');
            // $FilterDate = ['SelectedDate1' => $SelectedDate1,'SelectedDate2'=>$SelectedDate2, 'SelectedRegionCode' => $SelectedRegion, 'SelectedStateCode' => $SelectedState];
            //$this->session->set_userdata('FilterData', $FilterDate);
            $RegionTransferData = $this->GetInterRegionalTransfer($SelectedDate);

            $IsDataAvailable = 0;
            if ($RegionTransferData->status == 0) {
                $IsDataAvailable = 1;
            }
            $data = array('RegionTransferData' => $RegionTransferData, 'Lang' => $Lang, 'IsDataAvailable' => $IsDataAvailable);

            $this->load->view('Dashboard/IndiaMap_partial', $data);
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : set value For change langauge 
    public function LangaugeChange($lang, $Title = '') {
        $this->lang->load('content', $lang = '' ? 'english' : $lang);
        $data['Title'] = $this->lang->line($Title);
        $data['InterRegionalTransmission'] = $this->lang->line('InterRegionalTransmission');
        $data['InterRegional'] = $this->lang->line('InterRegional');
        $data['Transmission'] = $this->lang->line('Transmission');
        $data['GenerationData'] = $this->lang->line('GenerationData');
        $data['DemandMet'] = $this->lang->line('DemandMet');
        $data['ISGS'] = $this->lang->line('ISGS');
        $data['Thermal'] = $this->lang->line('Thermal');
        $data['Hydro'] = $this->lang->line('Hydro');
        $data['Gas'] = $this->lang->line('Gas');
        $data['Nuclear'] = $this->lang->line('Nuclear');
        $data['Renewable'] = $this->lang->line('Renewable');
        $data['Solar'] = $this->lang->line('Solar');
        $data['Wind'] = $this->lang->line('Wind');
        $data['FAQ'] = $this->lang->line('FAQ');
        $data['Frequency'] = $this->lang->line('Frequency');
        $data['MW'] = $this->lang->line('MW');
        $data['Generation'] = $this->lang->line('Generation');
        $data['App'] = $this->lang->line('App');
        $data['Today'] = $this->lang->line('Today');
        $data['Yesterday'] = $this->lang->line('Yesterday');
        $data['Diff'] = $this->lang->line('Diff');
        $data['HeaderDate1'] = $this->lang->line('HeaderDate1');
        $data['HeaderDate2'] = $this->lang->line('HeaderDate2');
        $data['Northern'] = $this->lang->line('Northern');
        $data['Southern'] = $this->lang->line('Southern');
        $data['Western'] = $this->lang->line('Western');
        $data['Eastern'] = $this->lang->line('Eastern');
        $data['NorthEastern'] = $this->lang->line('NorthEastern');
        $data['Region'] = $this->lang->line('Region');
        $data['Bangladesh'] = $this->lang->line('Bangladesh');
        $data['Bhutan'] = $this->lang->line('Bhutan');
        $data['Nepal'] = $this->lang->line('Nepal');
        $data['Translation_details'] = $this->lang->line('Translation_details');
        $data['SubHeader'] = $this->lang->line('SubHeader');
        $data['Generation'] = $this->lang->line('Generation');
        $data['ImpLineOfER'] = $this->lang->line('ImpLineOfER');
        $data['ATCMonitoring'] = $this->lang->line('ATCMonitoring');
        $data['InterRegionalExchange'] = $this->lang->line('InterRegionalExchange');
        $data['NationalSummary'] = $this->lang->line('NationalSummary');
        $data['Dashboard'] = $this->lang->line('Dashboard');
        $data['ElectricityPrice'] = $this->lang->line('ElectricityPrice');
        $data['MobileFooterMenuImage'] = $this->lang->line('MobileFooterMenuImage');
        $data['Translation_details_New'] = $this->lang->line('Translation_details_New');
        $data['Home'] = $this->lang->line('Home');
        return $data;
    }

//Dipti : For bind region ddl from DB
    public function FillDdlRegion() {
        try {

            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, URL . '/GetRegionDetails');
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_POST, 1);
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array());
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array("authorization: Basic " . CREDENTIAL));
            $buffer = curl_exec($curl_handle);
            curl_close($curl_handle);
            $result = json_decode($buffer);
            if (isset($result->status) && $result->status == 'success') {
                $response = $result; // array('status' => 'success', 'data' => $result, 'message_' => "");
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array('result' => $response)));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERROR_BIND_REGION)))));
            }
        } catch (Exception $ex) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERROR_EXPECTION)))));
        }
    }

//Dipti : bind State ddl from Db
    public function FillDdlState() {
        try {
            $curl = curl_init();
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            curl_setopt_array($curl, array(
                CURLOPT_URL => URL . "/GetStateDetails",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "RegionCode=" . $RegionCode,
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic " . CREDENTIAL,
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded"
                ),
            ));

            $buffer = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $result = json_decode($buffer);

            if (isset($result->status) && $result->status == 'success') {
                $response = $result; // array('status' => 'success', 'data' => $result, 'message_' => "");
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array('result' => $response)));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERROR_BIND_STATE)))));
            }
        } catch (Exception $ex) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERROR_EXPECTION)))));
        }
    }

//Demand Met View and function
//Dipti: To load DemandMet View with master view & it will set text depend on language that selected
    public function LoadDemandMetView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'DemandMet');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/DemandMetView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get Demand data from db 
    public function getDemandMetData() {
        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter
            $resultToday = $this->GetDemandMetDataDatewise($StateCode, $RegionCode, $SelectedDate1);
            $resultYesterday = $this->GetDemandMetDataDatewise($StateCode, $RegionCode, $SelectedDate2);
            $this->session->unset_userdata('FilterData');
            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];
            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get Demand Met data as per filter
    public function GetDemandMetDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetDemandMetDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//ISGS Generation
// Dipti: To load ISGS View with master view & it will set text depend on language that selected
    public function LoadISGSView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'ISGS');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/ISGSView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
          //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    //Dipti : to get ISGS data from db 
    public function getISGSData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter


            $resultToday = $this->GetISGSDataDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetISGSDataDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');
            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];
            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get ISGS data as per filter
    public function GetISGSDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetISGSGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Thermal Generation
// Dipti: To load Thermal View with master view & it will set text depend on language that selected
    public function LoadThermalView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Thermal');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/ThermalView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get Thermal data from db 
    public function getThermalData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter

            $resultToday = $this->GetThermalDataDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetThermalDataDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];

            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get Thermal data as per filter
    public function GetThermalDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetThermalGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Hydro Generation
// Dipti: To load Hydro View with master view & it will set text depend on language that selected
    public function LoadHydroView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Hydro');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/HydroView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get Hydro data from db 
    public function getHydroData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter

            $resultToday = $this->GetHydroDataDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetHydroDataDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];


            $this->session->set_userdata('FilterData', $FilterDate);


            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get Hydro data as per filter
    public function GetHydroDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetHydroGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Gas Generation
// Dipti: To load Gas View with master view & it will set text depend on language that selected
    public function LoadGasView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Gas');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/GasView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
//            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get Gas data from db 
    public function getGasData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter

            $resultToday = $this->GetGasDataDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetGasDataDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];

            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get Gas data as per filter
    public function GetGasDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetGasGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Nuclear Generation
// Dipti: To load Nuclear View with master view & it will set text depend on language that selected
    public function LoadNuclearView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Nuclear');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/NuclearView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
    //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get Nuclear data from db 
    public function getNuclearData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter

            $resultToday = $this->GetNuclearDataDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetNuclearDataDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];


            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get Nuclear data as per filter
    public function GetNuclearDataDatewise($StateCode, $RegionCode, $SelectedDate) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetNuclearGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Renewable Generation
// Dipti: To load Renewable View with master view & it will set text depend on language that selected
//    public function LoadRenewableView() {
//        try {
//            $lang = $this->session->userdata("lang_det");
//            $data = $this->LangaugeChange($lang, 'Renewable');
//            $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
//            $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
//            $this->load->view('GenerationData/RenewableView', $data);
//            $this->load->view('MasterLayout/MasterFooter', $data);
//        } catch (Exception $ex) {
//            $this->load->view('ErrorPage');
//        }
//    }




    public function LoadSolarView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Solar');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/SolarGeneration', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 2));
//            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    public function LoadWindView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Wind');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/WindGeneration', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 3));
            //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Faq's
// Dipti: To load FAQ View with master view & it will set text depend on language that selected
    public function LoadFAQView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'FAQ');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 2);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('FAQView', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
            //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti : to get generation data from db 
    public function getGenerationData() {
        try {

            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $FromDate = $this->security->xss_clean($this->input->post('FromDate', TRUE)); //use xss_clean & xss_filter
            $ToDate = $this->security->xss_clean($this->input->post('ToDate', TRUE)); //use xss_clean & xss_filter
            $FromTime = $this->security->xss_clean($this->input->post('FromTime', TRUE)); //use xss_clean & xss_filter
            $ToTime = $this->security->xss_clean($this->input->post('ToTime', TRUE)); //use xss_clean & xss_filter

            $resultToday = $this->GetGenerationDataDatewise($StateCode, $RegionCode, $FromDate, $FromDate, $FromTime, $ToTime);

            $resultYesterday = $this->GetGenerationDataDatewise($StateCode, $RegionCode, $ToDate, $ToDate, $FromTime, $ToTime);

            $this->session->unset_userdata('DataForDate');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $FromDate, 'SelectedDate2' => $ToDate, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, 'CreatedDate' => $CreatedDate];

            $this->session->set_userdata('FilterData', $FilterDate);

            $DataForSelectedDate = date_format(date_create($resultToday->Result[0][0]->Date_), 'd-M-Y h:i A');
            if ((isset($resultToday->status) && $resultToday->status == 'success') && (isset($resultYesterday->status) && $resultYesterday->status == 'success')) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate' => $DataForSelectedDate, 'FromDate' => date("d M Y", strtotime($FromDate)), 'PrevFromDate' => date("d M Y", strtotime($ToDate)))));
            } else {

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

//Dipti:get generation data as per filter
    public function GetGenerationDataDatewise($StateCode, $RegionCode, $FromDate, $ToDate, $FromTime, $ToTime) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetGenerationDataDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&FromDate=" . $FromDate . "&ToDate=" . $ToDate . "&FromTime=" . $FromTime . "&ToTime=" . $ToTime,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

//Dipti : to get Renewable data from db 
//    public function getRenewableData() {
//
//        try {
//            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
//            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
//            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
//            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter
//
//
//            $resultToday = $this->GetRenewableDataDatewise($StateCode, $RegionCode, $SelectedDate1);
//
//            $resultYesterday = $this->GetRenewableDataDatewise($StateCode, $RegionCode, $SelectedDate2);
//
//            $this->session->unset_userdata('FilterData');
//            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode];
//            $this->session->set_userdata('FilterData', $FilterDate);
//
//            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
//                $response1 = $resultToday;
//                $response2 = $resultYesterday;
//
//                $this->output
//                        ->set_content_type('application/json')
//                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
//            } else {
//                $this->output
//                        ->set_content_type('application/json')
//                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
//            }
//        } catch (Exception $ex) {
//            $this->load->view('ErrorPage');
//        }
//    }
//
////Dipti:get Renewable data as per filter
//    public function GetRenewableDataDatewise($StateCode, $RegionCode, $SelectedDate) {
//
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => URL . "/GetRenewableGenerationDetails",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
//            CURLOPT_HTTPHEADER => array(
//                "authorization: Basic " . CREDENTIAL,
//                "cache-control: no-cache",
//                "content-type: application/x-www-form-urlencoded"
//            ),
//        ));
//        $buffer = curl_exec($curl);
//        $err = curl_error($curl);
//
//        curl_close($curl);
//
//        $result = json_decode($buffer);
//        return $result;
//    }
//KCJ 30 Oct 2018 use to change language with details
    public function ChangeLanguage() {
        try {
            $Language = $this->session->userdata("lang_det");
            $SetLanguage = "english";
            $this->session->unset_userdata('lang_det');

            if ($Language === "english") {
                $SetLanguage = "hindi";
            }
            $this->session->set_userdata('lang_det', $SetLanguage);
            echo 1;
        } catch (Exception $ex) {
            echo -1;
        }
    }

//KCJ 02 Nov 2018 use to open inter regional transfer details on mobile with difffrent route
    public function LoadDataForInterRegional() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $Lang = $this->LangaugeChange($lang, 'InterRegionalTransmission');
                $Date = date('Y-m-d');
                $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
                $RegionTransferData = $this->GetInterRegionalTransfer($Date);
                $IsDataAvailable = 0;
                if ($RegionTransferData->status == 0) {
                    $IsDataAvailable = 1;
                }
                $data = array('RegionTransferData' => $RegionTransferData, 'Date' => $Date, 'Lang' => $Lang, 'IsMenuShow' => 0, 'IsDataAvailable' => $IsDataAvailable, 'Yesterday' => $Date2, 'Today' => $Date);
                $HeaderData = array('Lang' => $Lang, 'IsMenuShow' => 0, 'IsInterRegional' => 1);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('Dashboard/dashboard', $data);
                $this->load->view('MasterLayout/MasterFooter', array($Lang, "IsOther" => 1));
         //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {

            $this->load->view('ErrorPage');
        }
    }

//Kaustubh 26-12-2018 : to get Solar data using web api 
    public function getSolarData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter


            $resultToday = $this->GetSolarDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetSolarDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, "CreatedDate" => $CreatedDate];

            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    public function GetSolarDatewise($StateCode, $RegionCode, $SelectedDate) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetSolarGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

    //Kaustubh 26-12-2018 : to get Solar data using web api 
    public function getWindData() {

        try {
            $StateCode = $this->security->xss_clean($this->input->post('StateCode', TRUE)); //use xss_clean & xss_filter
            $RegionCode = $this->security->xss_clean($this->input->post('RegionCode', TRUE)); //use xss_clean & xss_filter
            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter


            $resultToday = $this->GetWindDatewise($StateCode, $RegionCode, $SelectedDate1);

            $resultYesterday = $this->GetWindDatewise($StateCode, $RegionCode, $SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));
            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => $RegionCode, 'SelectedStateCode' => $StateCode, "CreatedDate" => $CreatedDate];

            $this->session->set_userdata('FilterData', $FilterDate);

            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    public function GetWindDatewise($StateCode, $RegionCode, $SelectedDate) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetWindGenerationDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . $StateCode . "&RegionCode=" . $RegionCode . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

    public function MobileAppLoad($Querystring_) {
        try {

            $this->session->set_userdata('TypeOfDevice', $Querystring_);
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $FilterData = $this->session->userdata("FilterData");
                $SelectedDate = $FilterData["SelectedDate1"];
                $Lang = $this->LangaugeChange($lang, 'InterRegionalTransmission');
                $Date = $SelectedDate;
                $RegionTransferData = $this->GetInterRegionalTransfer($Date);
                $IsDataAvailable = 0;
                if ($RegionTransferData->status == 0) {
                    $IsDataAvailable = 1;
                }
                $Date = date('Y-m-d', strtotime(date('Y-m-d')));
                $Date2 = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
                $data = array('RegionTransferData' => $RegionTransferData, 'Date' => $Date, 'Lang' => $Lang, 'IsDataAvailable' => $IsDataAvailable, 'Yesterday' => $Date2, 'Today' => $Date);
                $HeaderData = array('Lang' => $Lang, 'IsMenuShow' => 1, 'IsInterRegional' => 1);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('Dashboard/dashboard', $data);
                $this->load->view('MasterLayout/MasterFooter', array($Lang, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    public function GetGenerationDataByRegion() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetGenerationDataByRegion",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($buffer);
        return $result;
    }

    //Kaustubh 26-12-2018 : Get generation data for all india and region wise
    public function getGenerationDataForTable() {
        try {

            $result = $this->GetGenerationDataByRegion();
            if ((isset($result->status) && ($result->status == 'success' || $result->status == '3'))) {
                $response1 = $result;

                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    //Kaustubh 27-03-2020 : Get frequency data for all India by date 
    public function LoadFrequencyView() {
        try {
           //if ($this->session->userdata('UserData') != "") {
                $lang = $this->session->userdata("lang_det");
                $data = $this->LangaugeChange($lang, 'Frequency');
                $HeaderData = array('Lang' => $data, 'IsMenuShow' => 0, 'IsInterRegional' => 0);
                $this->load->view('MasterLayout/MasterHeaderResponsive', $HeaderData);
                $this->load->view('GenerationData/Frequency', $data);
                $this->load->view('MasterLayout/MasterFooter', array($data, "IsOther" => 1));
           //            } else {
//                redirect('/login');
//            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

    //Kaustubh 27-03-2020 : Get frequency data for all India by date 
    public function GetFrequencyDatewise($SelectedDate) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL . "/GetFrequencyDetails",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "StateCode=" . 0 . "&RegionCode=" . 0 . "&SelectedDate=" . $SelectedDate,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . CREDENTIAL,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $buffer = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $result = json_decode($buffer);

        return $result;
    }

    //Kaustubh 27-03-2020 : Get frequency data for all India by date 
    public function getFrequencyViewData() {

        try {

            $SelectedDate1 = $this->security->xss_clean($this->input->post('SelectedDate1', TRUE)); //use xss_clean & xss_filter
            $SelectedDate2 = $this->security->xss_clean($this->input->post('SelectedDate2', TRUE)); //use xss_clean & xss_filter


            $resultToday = $this->GetFrequencyDatewise($SelectedDate1);

            $resultYesterday = $this->GetFrequencyDatewise($SelectedDate2);

            $this->session->unset_userdata('FilterData');

            $CreatedDate = date('Y-m-d H:i:s', strtotime('+20 minutes'));

            $FilterDate = ['SelectedDate1' => $SelectedDate1, 'SelectedDate2' => $SelectedDate2, 'SelectedRegionCode' => 0, 'SelectedStateCode' => 0, "CreatedDate" => $CreatedDate];
            $this->session->set_userdata('FilterData', $FilterDate);
            if ((isset($resultToday->status) && ($resultToday->status == 'success' || $resultToday->status == '3')) && (isset($resultYesterday->status) && ($resultYesterday->status == 'success' || $resultYesterday->status == '3'))) {
                $response1 = $resultToday;
                $response2 = $resultYesterday;
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(array('result1' => $response1), array('result2' => $response2), 'SelectedDate1' => date("d M Y", strtotime($SelectedDate1)), 'SelectedDate2' => date("d M Y", strtotime($SelectedDate2)))));
            } else {
                $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array((array('0' => "-1")), (array('msg' => ERRORMSG)))));
            }
        } catch (Exception $ex) {
            $this->load->view('ErrorPage');
        }
    }

}
