<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'DashboardController/';
$route['default_controller'] = "Test";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



//View render routes - Generation App

$route['(?i)interregional-transfer']['get'] =  'DashboardController/LoadDataForInterRegional';
$route['(?i)generation-data']['get'] =  'DashboardController/LoadGenerationView';
$route['(?i)demand-met']['get'] =  'DashboardController/LoadDemandMetView';
$route['(?i)isgs-generation']['get'] =  'DashboardController/LoadISGSView';
$route['(?i)thermal-generation']['get'] =  'DashboardController/LoadThermalView';
$route['(?i)hydro-generation']['get'] =  'DashboardController/LoadHydroView';
$route['(?i)gas-generation']['get'] =  'DashboardController/LoadGasView';
$route['(?i)nuclear-generation']['get'] =  'DashboardController/LoadNuclearView';
$route['(?i)solar-generation']['get'] =  'DashboardController/LoadSolarView';
$route['(?i)wind-generation']['get'] =  'DashboardController/LoadWindView';
$route['(?i)faq']['get'] =  'DashboardController/LoadFAQView';
$route['(?i)frequency']['get'] =  'DashboardController/LoadFrequencyView';
$route['(?i)cxbtesting']['get'] =  'DashboardController/index1';
$route['(?i)app/(:any)']['get'] =  'DashboardController/MobileAppLoad/$1';

//View render routes - RTD

$route['(?i)national-summary']['get'] =  'RTDController/LoadNationalSummary';
$route['(?i)inter-regional-exchange']['get'] =  'RTDController/InterRegionalExchage';
$route['(?i)ATC-monitoring']['get'] =  'RTDController/ATCMonitoring';
$route['(?i)important-line-ER']['get'] =  'RTDController/ImportantLineOfER';


$route['(?i)national-rtd']['get'] =  'RTDController/NationalSummary';

$route['(?i)login']['get'] =  'LoginController/LoadLogin';




//Ajax request routes


$route['IndiaMapLoad/(:any)']['get'] =  'DashboardController/GetIndiaDetails/$1';
$route['FillDdlRegion']['post'] =  'DashboardController/FillDdlRegion';
$route['FillDdlState']['post'] =  'DashboardController/FillDdlState';
$route['getDemandMetData']['post'] =  'DashboardController/getDemandMetData';
$route['getGasData']['post'] =  'DashboardController/getGasData';
$route['getHydroData']['post'] =  'DashboardController/getHydroData';
$route['getISGSData']['post'] =  'DashboardController/getISGSData';
$route['getSolarData']['post'] =  'DashboardController/getSolarData';
$route['getWindData']['post'] =  'DashboardController/getWindData';
$route['getGenerationData']['post'] =  'DashboardController/getGenerationData';
$route['getThermalData']['post'] =  'DashboardController/getThermalData';
$route['getNuclearData']['post'] =  'DashboardController/getNuclearData';
$route['LanguageChange']['post'] =  'DashboardController/ChangeLanguage';
$route['SendErrorEmail']['post'] =  'SendMailController/SendErrorEmail';
$route['getGenerationDataForTable']['post'] =  'DashboardController/getGenerationDataForTable';
$route['getFrequencyData']['post'] =  'DashboardController/getFrequencyViewData';
$route['authenticate']['post'] =  'LoginController/ValidateActiveDirectoryUser';