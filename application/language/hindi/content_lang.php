<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$lang['Message']="Hello World! Hindi";

//****************************************
//NOTE THAT: if you add variable into this array then you have to add same at English/content_lang.php also
//****************************************
$lang['InterRegionalTransmission']="Inter Regional Transmission";
$lang['InterRegional']="Inter Regional Transmission";
$lang['Generation']="जनरेशन";
$lang['GenerationData']="जनरेशन डेटा";
$lang['DemandMet']="डिमांड मेट";
$lang['ISGS']="आयएसजीएस जनरेशन";
$lang['Thermal']="थर्मल जनरेशन";
$lang['Hydro']="हायड्रो जनरेशन";
$lang['Gas']="गॅस जनरेशन";
$lang['Nuclear']="Nuclear Generation";
$lang['Renewable']="Renewable Generation";
$lang['Frequency']="Frequency";
$lang['Solar']="Solar Generation";
$lang['Wind']="Wind Generation";
$lang['Home']="Home";

$lang['FAQ']="Frequently Asked Questions";
$lang['MW']="मेगावाट";
$lang['Generation']="जनरेशन";
$lang['App']="App";
$lang['Today']="आज";
$lang['Yesterday']="बिता कल";
$lang['Diff']="अंतर";

$lang['HeaderDate1']="डेटा के लिये";
$lang['HeaderDate2']="";
$lang['Translation_details']="Translate to English";
$lang['DataFor']="DATA FOR";
$lang['GenerationFor']="Generation For";


$lang['SubHeader']="Electricity Price & Availability Highlights";
$lang['Northern']="NORTHERN";
$lang['Southern']="SOUTHERN";
$lang['Western']="WESTERN";
$lang['Eastern']="EASTERN";
$lang['NorthEastern']="NORTH EASTERN";
$lang['Region']="REGION";
$lang['Bangladesh']="BANGLADESH";
$lang['Bhutan']="BHUTAN";
$lang['Nepal']="NEPAL";


$lang['ImpLineOfER']="Important Line Of ER";
$lang['ATCMonitoring']="ATC Monitoring";
$lang['InterRegionalExchange']="Inter Regional Exchange";
$lang['NationalSummary']="National Summary";
$lang['Dashboard']="Dashboard";

$lang['ElectricityPrice']="ELECTRICITY PRICE & AVAILABILITY HIGHLIGHTS";

//$lang['MobileFooterMenuImage']="english_icon.png";
$lang['MobileFooterMenuImage']="english_img";
$lang['Translation_details_New']="English";