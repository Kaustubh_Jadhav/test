<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$lang['Message']="Hello World english!";
//****************************************
//NOTE THAT: if you add variable into this array then you have to add same at Hindi/content_lang.php also
//****************************************
$lang['InterRegionalTransmission']="Inter Regional Transmission";
$lang['InterRegional']="Inter Regional";
$lang['Transmission']="Transmission";
$lang['GenerationData']="Generation Data";
$lang['Generation']="Generation";
$lang['DemandMet']="Demand Met";
$lang['ISGS']="ISGS Generation";
$lang['Thermal']="Thermal Generation";
$lang['Hydro']="Hydro Generation";
$lang['Gas']="Gas Generation";
$lang['Nuclear']="Nuclear Generation";
$lang['Renewable']="Renewable Generation";
$lang['Solar']="Solar Generation";
$lang['Wind']="Wind Generation";
$lang['FAQ']="Frequently Asked Questions";
$lang['Frequency']="Frequency";
$lang['MW']="MW";
$lang['Generation']="Generation";
$lang['App']="App";
$lang['Today']="Today";
$lang['Yesterday']="Yesterday";
$lang['Home']="Home";

$lang['Diff']="Difference";

$lang['HeaderDate1']=" DATA FOR ";
$lang['HeaderDate2']="FOR ";
$lang['Translation_details']="हिंदी में अनुवाद";
$lang['GenerationFor']="Generation For";



$lang['SubHeader']="Electricity Price & Availability Highlights";
$lang['Northern']="NORTHERN";
$lang['Southern']="SOUTHERN";
$lang['Western']="WESTERN";
$lang['Eastern']="EASTERN";
$lang['NorthEastern']="NORTH EASTERN";
$lang['Region']="REGION";
$lang['Bangladesh']="BANGLADESH";
$lang['Bhutan']="BHUTAN";
$lang['Nepal']="NEPAL";


$lang['ImpLineOfER']="Important Line Of ER";
$lang['ATCMonitoring']="ATC Monitoring";
$lang['InterRegionalExchange']="Inter Regional Exchange";
$lang['NationalSummary']="National Summary";
$lang['Dashboard']="Dashboard";

$lang['ElectricityPrice']="ELECTRICITY PRICE & AVAILABILITY HIGHLIGHTS";

//$lang['MobileFooterMenuImage']="hindi_icon.png";
$lang['MobileFooterMenuImage']="hindi_img";
$lang['Translation_details_New']="हिंदी";





