<html>
    <head>
        <title><?php echo $Lang["Title"]; ?> - Generation App</title>   

        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <link rel="icon" href="<?php echo base_url(); ?>/images/PosocoTitle.jpg" sizes="32x32" />

        <link href="<?php echo base_url(); ?>/css/responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/style_gen.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/map.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css">
        <link href="<?php echo base_url(); ?>/css/waitMe.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

        <script src="<?php echo base_url(); ?>/script/jquery-3.3.1.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>/script/Chart.Bar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/Chart.PieceLabel.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.min.js" type="text/javascript"></script>
        <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/Common.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.paginate.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.filter.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.sort.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>script/Chart.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/hammer.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/chartjs-plugin-zoom.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/bootstrap-toggle.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/jquery.slimscroll.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Anaheim" rel="stylesheet" type="text/css">

        <style>
            canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="Page_data">
                <div class="container-fluid">
                    <div class="col-md-12 col-xs-12 headerLogo mobile_sec header" id="myHeader">
                        <div class="Header_Logo">                    
                            <a href="<?php echo base_url() ?>"><img style="height: 100px;" src="<?php echo base_url(); ?>/images/ministry-ofpower.png" alt="Ministry Of Power" title="Ministry Of Power"/></a>
                        </div>        
                        <div class="col-md-12 col-xs-12 Header_padding">
                            <div class="col-md-12 col-xs-12 mobile_sec">
                                <div class="col-md-2 col-xs-2">
                                    <div class="Menu_Link">
                                        <i class="menu-i fa fa-bars link_section" onclick="LoadSlideMenu();"></i>  <!-- OpenMenu();-->
                                        <i class="back-i fa fa-chevron-left link_section" onclick="location.href = 'cxbTesting';"></i>                                    
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 min_padding">
                                    <div id="app_title" class="app_title mobile_sec">
                                        <p class="title_org no-margin-bottom"><?php echo $Lang["Generation"] ?>&nbsp;<?php echo $Lang["App"] ?></p>
                                    </div>
                                    <div class="app_title mobile_sec" id="page_title">
                                        <span class="page_org"></span><br />
                                        <span style="color: #fff;font-size: 12px;display: none;" id="SelectedDate_">Data For <span id="SelectedDate_1"></span></span>
                                    </div> 
                                </div>
                                <div class="col-md-2 link_section col-xs-2">
<!--                                    <a class="LangChangeLink mob_link" href="<?php echo base_url(); ?>national-summary">R</a>
                                    <a class="LangChangeLink desktop_link" href="<?php echo base_url(); ?>national-summary">RTD</a>
                                    <a class="LangChangeLink" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>-->
                                    <a class="LanguageTranslateText_en_rtd mob_link" href="<?php echo base_url(); ?>national-summary">R</a>
                                    <a class="LanguageTranslateText_en_rtd desktop_link" href="<?php echo base_url(); ?>national-summary">RTD</a>
                                    <a class="LanguageTranslateText_en_rtd hindi_tran" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
                                    <a class="LanguageTranslateText_en_rtd faq_link" href="<?php echo base_url(); ?>faq">FAQ</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 mobile_sec">  
                                <?php if ($IsInterRegional != 2) : ?> 
                                    <?php if ($IsInterRegional == 1) : ?> 
                                        <div class="col-md-12 sub_header_Inter">
                                            <input type="hidden" value="<?php echo $Lang["InterRegionalTransmission"] ?>" id="CommonPageTitle">
                                            <span class="Generation_Name sub_heading_main"> <?php echo $Lang["InterRegionalTransmission"] ?>  <?php echo $Lang["HeaderDate1"] ?></span>
                                            <input type="text" class="IntertxtDate form-control DateFilterText" id="txt_date" readonly style="display: none !important;">
                                            <i class="fa fa-calendar cal_icon_inter" aria-hidden="true" onclick="ToShowCalender();" style="display: none;">
                                            </i>
                                            <span class="Generation_Name sub_heading_main" id="SelectedDate_2"></span>

                                        </div>
                                    <?php else: ?>

                                        <?php $this->load->view('PartialView/DateSelection.php'); ?>  

                                    <?php endif; ?> 

                                <?php else: ?>
                                    <div class="col-md-12 sub_header_Inter" style="margin-bottom: 0px;">
                                        <div class='col-md-12'>
                                            <?php echo $Lang["FAQ"]; ?>
                                        </div>
                                    </div>

                                <?php endif; ?> 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>                 
                <input type="hidden" id="IsMenuShow" value="<?php echo $IsMenuShow ?>">
                <input type="hidden" id="IsDataPresent" value="1">
                <input type="hidden" id="SelectedDate1" value="<?php echo $this->session->userdata('FilterData')['SelectedDate1'] ?>">
                <input type="hidden" id="SelectedDate2" value="<?php echo $this->session->userdata('FilterData')['SelectedDate2'] ?>">
                <input type="hidden" id="SelectedRegion" value="<?php echo $this->session->userdata('FilterData')['SelectedRegionCode'] ?>">
                <input type="hidden" id="SelectedState" value="<?php echo $this->session->userdata('FilterData')['SelectedStateCode'] ?>">
                <script type="text/javascript">
                    //dipti 20 oct 18: common function to toggle chart to table / table to chart
                    function ChartTableToggle()
                    {
                        if (!$("#inner_chart_Line1").is(':visible'))
                        {
                            $("#inner_chart_Line1").show();
                            $("#div_data").hide();
                            $("#div_data1").hide();
                        } else
                        {
                            $("#inner_chart_Line1").hide();
                            $("#div_data").show();
                            $("#div_data1").show();
                        }
                    }

                    //Dipti 20 oct 18: common Function of calender controller
                    function ToShowCalender()
                    {
                        $("#txt_vpDate").focus();
                    }
                    function ToShowCalender()
                    {
                        $("#txt_vpDate2").focus();
                    }
                    //Dipti 20oct 18: not in used
                    function LandingPageView()
                    {
                        $("#map_desktopView").css('display', 'table');
                        $(".desktop_menu").css('display', 'none');
                        $(".mobile_menu").css('display', 'none');
                    }

                    function TranslatePage()
                    {
                        $.ajax({
                            url: "<?php echo base_url(); ?>LanguageChange",
                            datatype: "JSON",
                            data: {
                                'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>',
                            },
                            type: "POST",
                            success: function (data) {
                                if (data == "1")
                                {
                                    location.reload();
                                }
                            },
                        });
                    }

                    $(document).ready(function (e)
                    {

                        $("#DesktopMenu").addClass("Menu_scroll");
                        //$(".box_section").css("opacity", '0.7');
                        $(".box_section").mouseover(function (e) {
                            var IsPositionLast = $("#DesktopMenu").hasClass("Menu_scroll");
                            if (IsPositionLast == true)
                            {
                                $(this).animate({'top': '-50px'}, 100);
                            }
                        });

                        $(".box_section").mouseleave(function (e) {
                            $(".box_section").animate({'top': '0'}, 0);
                        });

                        $('#toggle-chart').change(function () {
                            var IsDataAvailable = $("#IsDataPresent").val();

                            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
                            if (IsDataAvailable != 0)
                            {
                                if (!$("#inner_chart_Line1").is(':visible'))
                                {

                                    $("#inner_chart_Line1").fadeIn(2000);
                                    $(".divTable").fadeOut(0);
                                    setInterval(function () {
                                        $(".waitMe").hide();
                                    }, 2000);
                                    $(".slimScrollDiv").hide();
                                    $("#div_data1").fadeOut(0);
                                    $("#Legend").show();
                                } else
                                {

                                    $("#inner_chart_Line1").fadeOut(0);
                                    $(".divTable").fadeIn(2000);
                                    setInterval(function () {
                                        $(".waitMe").hide();

                                    }, 2000);
                                    $(".slimScrollDiv").show();
                                    $("#div_data1").fadeIn(2000);
                                    $("#Legend").hide();
                                }
                            } else
                            {
                                $(".waitMe").hide();
                            }
                        });


                        var WindowWidth = $(window).width();
                        if (WindowWidth <= 991)
                        {
                            $("#app_title").hide();
                            $("#page_title").show();
                            var PageTitle = $("#CommonPageTitle").val();
                            $(".page_org").text(PageTitle);
                            $(".Generation_Name").hide();
                            $(".divTable").hide();
                            var IsMenushow = $("#IsMenuShow").val();
                            if (IsMenushow == 1)
                            {
                                OpenMenu();
                            }
                        } else
                        {
                            $("#app_title").show();
                            $("#page_title").hide();
                            $(".Generation_Name").show();
                            $(".divTable").show();
                        }

                        $(function () {
                            $('#toggle-chart').bootstrapToggle({
                                on: 'Table',
                                off: 'Chart'
                            });
                        });



                    });

                    $(window).resize(function () {

                        var WindowWidth = $(window).width();
                        if (WindowWidth <= 991)
                        {
                            $("#app_title").hide();
                            $("#page_title").show();
                            var PageTitle = $("#CommonPageTitle").val();
                            $(".page_org").text(PageTitle);
                            $(".Generation_Name").hide();
                            var IsDataShow = $("#inner_chart_Line1").css("display");
                            if (IsDataShow == "block")
                            {
                                $(".divTable").hide();
                                $("#div_data1").hide();
                            } else
                            {
                                $(".divTable").show();
                                $("#div_data1").show();
                            }
                            $("#Page_data").css("overflow-y", 'hidden');

                        } else
                        {

                            $("#app_title").show();
                            $("#page_title").hide();
                            $(".Generation_Name").show();
                            $(".divTable").show();
                            $("#div_data1").show();
                            $("#ShowMenuMobileView").hide();

                        }
                        //$("body").css('overflow','hidden');
                    });

                    $(window).on("scroll", function () {

                    });

                    $(document).mouseup(function (e)
                    {
                        var container = $("#Tooltip_NR");

                        // if the target of the click isn't the container nor a descendant of the container
                        if (!container.is(e.target) && container.has(e.target).length === 0)
                        {
                            container.hide();
                        }
                    });



                    function RedirectDashboard()
                    {
                        var Location_ = "<?php echo base_url(); ?>cxbTesting";
                        window.location.href = Location_;
                    }

                    function LoadMobile_DesktopView(IsMobile)
                    {
                        if (IsMobile == 1)
                        {
                            $(".Generation_Name").hide();
                            $("#Mobile_date").show()
                        } else
                        {
                            $(".Generation_Name").show();
                            $("#Mobile_date").hide();
                        }
                    }

                    function timeToSeconds(Date, time) {
                        return Date + " " + time;
                    }

                    function ShowNewMenu()
                    {
                        var WindowWidth = $(window).width();
                        if (WindowWidth >= 991)
                        {
                            var IsMenuVisible = 0;
                            var DivMargin = $("#DesktopMenu").css('bottom');
                            if (DivMargin != "0px") {
                                IsMenuVisible = 1;
                            }
                            if (IsMenuVisible == 1)
                            {
                                // $("#DesktopMenu").slideUp();
                                // $("#DesktopMenu").css('bottom','-60px');
                                $("#DesktopMenu").addClass("Menu_scroll");
                                // $(".box_section").css('opacity', "0.7");
                                $("#UpArrow").fadeIn();
                                $("#DownArrow").hide();
                                //$("#Page_data").css("visibility", "visible");
                                

                            } else
                            {
                                // $("#DesktopMenu").slideDown();
                                $("#UpArrow").hide();
                                $("#DownArrow").fadeIn();
                                //$(".box_section").css('opacity', "0.7");
                                $("#DesktopMenu").addClass("Menu_scroll");
                               // $("#Page_data").css("visibility", "hidden");
                            }

                        } else
                        {
                            var IsMenuVisible = 0;
                            if ($("#ShowMenuMobileView").css('display') === "block") {
                                IsMenuVisible = 1;
                            }
                            if (IsMenuVisible == 1)
                            {
                                $("#ShowMenuMobileView").slideUp();
                                $("#UpArrow").fadeIn();
                                $("#DownArrow").hide();
                                $("#Page_data").html("");
                                $("#Page_data").css("visibility", "hidden");

                            } else
                            {
                                $("#ShowMenuMobileView").slideDown();
                                $("#UpArrow").hide();
                                $("#DownArrow").fadeIn();
                                $("#Page_data").css("visibility", "visible");
                            }
                        }

                    }


                    function OpenMenu()
                    {
                        var IsVisible = $("#ShowMenuMobileView").css('display');
                        if (IsVisible == "block")
                        {
                            $("#ShowMenuMobileView").slideUp(1);
                            $("#Page_data").css("visibility","visible");

                        } else
                        {
                            $("#ShowMenuMobileView").slideDown(600);
                            $("#Page_data").css("visibility","hidden");
                            //$("#Page_data").css('display', 'none');
                        }
                        $('#SlideMenu').hide('slide', {direction: 'left'}, 700);
                        $("body").css('overflow', 'auto');
                        $("#IndiaMapDiv").css('display', 'none');
                        $("#desktop_data").css("visibility", "hidden");
                        if ($("#faq_view").length > 0) {
                            $("#faq_view").css("visibility", "hidden");
                        }
                        $(".inter-regional-value-box.last-hide").css("display", "none");
                    }

                    $(window).scroll(function () {
                        if ($(window).scrollTop() >= 300) {
                            $('nav').addClass('fixed-header');
                            $('nav div').addClass('visible-title');
                        } else {
                            $('nav').removeClass('fixed-header');
                            $('nav div').removeClass('visible-title');
                        }

                        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                            var IsMenuShow = '';
                            if ($(window).width() > 991)
                            {
                                IsMenuShow = $("#DesktopMenu").css("display");
                                if (IsMenuShow != 'block')
                                {
                                    ShowNewMenu();
                                }
                                //$("#DesktopMenu").css('position', 'absolute');
                                //$("#DesktopMenu").css('bottom', '0px');
                                $("#DesktopMenu").removeClass("Menu_scroll");
                                $(".box_section").css('opacity', "1.0");
                            }
                            //                        else
                            //                        {
                            //                            IsMenuShow = $("#ShowMenuMobileView").css('display');
                            //                        }


                        } else
                        {
                            IsMenuShow = $("#DesktopMenu").css("display");
                            if (IsMenuShow == "block")
                            {
                                $("#DesktopMenu").css('position', 'relative');
                                ShowNewMenu();
                            }
                            $(".box_section").css('opacity', "0.9");
                        }


                    });


                </script>
