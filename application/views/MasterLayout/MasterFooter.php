<div class="clearfix"></div>
<input type="hidden" id="IsOther" value="<?php echo $IsOther ?>">
<div class="menuOpen">
    <div class="col-md-12 mobile_sec nav_details MenuView" id="DesktopMenu">
        <div class="col-md-1 zero_width"></div> 
        <a href="<?php echo base_url() ?>cxbTesting"><div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divIndiaMap" style="border-top:none;">
                <p class="icon_menu"><i class="fa fa-globe"></i></p>
                <p><?php echo $Lang['Home']; ?></p>
            </div> </a>

        <a href="<?php echo base_url() ?>generation-data"><div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divGenerationData" style="border-top:none;">
                <p class="icon_menu"><i class="fa fa-bolt" aria-hidden="true"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["GenerationData"]); ?></p>
            </div></a>

        <a href="<?php echo base_url() ?>demand-met"><div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divDemandMet">
                <p class="icon_menu"><i class="fa fa-balance-scale"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["DemandMet"]); ?></p>
            </div> </a> 

        <a href="<?php echo base_url() ?>isgs-generation"><div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divISGS">
                <p class="icon_menu"><i class="fa fa-arrows-alt"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["ISGS"]); ?></p>
            </div> </a>

        <a href="<?php echo base_url() ?>thermal-generation"><div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divThermal">
                <p class="icon_menu"><i class="fa fa-file-medical-alt"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Thermal"]); ?></p>
            </div></a>  

        <a href="<?php echo base_url() ?>hydro-generation">
            <div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divHydro">
                <p class="icon_menu"><i class="fa fa-tint"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Hydro"]); ?></p>
            </div></a>

        <a href="<?php echo base_url() ?>gas-generation">
            <div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divGas">
                <p class="icon_menu"><i class="fa fa-fire"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Gas"]); ?></p>
            </div> </a>

        <a href="<?php echo base_url() ?>nuclear-generation">
            <div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divNuclear">
                <p class="icon_menu"><i class="fa fa-atom"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Nuclear"]); ?></p>
            </div></a>

        <a href="<?php echo base_url() ?>solar-generation">
            <div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divSolar">
                <p class="icon_menu"><i class="fa fa-sun"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Solar"]); ?></p>
            </div> </a>

        <a href="<?php echo base_url() ?>wind-generation">
            <div class="col-md-1 nav_borderBox_left_top_desk box_section" id="divWind">
                <p class="icon_menu"><i class="fa fa-asterisk"></i></p>
                <p><?php echo str_replace(' ', '<br />', $Lang["Wind"]); ?></p>
            </div></a>
        <div class="col-md-1 zero_width"></div> 
    </div>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
    <!--    <div class="col-md-12 mobile_sec nav_details MenuMobileView" id="ShowMenuMobileView" style="display:none">
            <div class="col-md-12 col-xs-12 mobile_sec border_BottomHeading1">
                <div class="col-md-1 col-xs-2" style="padding: 2%;">
                    <img style="height: 60px;" src="<?php echo base_url(); ?>/images/ministry-ofpower_Mobile.png" alt="Ministry Of Power" title="Ministry Of Power"/>
                </div>
                <div class="col-md-10 col-xs-8 min_padding">
                    <div id="app_title" class="app_title col-md-11">
                        <p class="title_org1" style="margin-bottom: 0px;">Generation&nbsp;App</p>
                    </div>
                </div>
                <div class="link_section col-md-1 col-xs-2" style="padding: 26px 0px !important;">
                    <a class="LangChangeLink" href="<?php echo base_url(); ?>national-summary">R</a>&nbsp;
                    <a class="LangChangeLink" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
                </div>
            </div>
    
            <div class="mobile_sec col-md-12 col-xs-12">
                <div id="divIndiaMap_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>interregional-transfer'">
                    <p class="icon_menu"><i class="fa fa-globe"></i></i></p>
                     <p style="margin-bottom: 0px;"><?php echo $Lang["InterRegional"]; ?><br /><?php echo $Lang["Transmission"]; ?></p>
                </div> 
    
                <div id="divGenerationData_mobile" class="col-md-6 col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>generation-data';">
                    <p class="icon_menu"><i class="fa fa-bolt" aria-hidden="true"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["GenerationData"]); ?></p>
                </div> 
            </div>
            <div class="clearfix"></div>
            <div class="mobile_sec col-md-12 col-xs-12"> 
    
                <div id="divDemandMet_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>demand-met';">
                    <p class="icon_menu"><i class="fa fa-balance-scale"></i></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["DemandMet"]); ?></p> 
                </div> 
    
                <div id="divISGS_mobile" class="col-md-6 col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>isgs-generation';">
                    <p class="icon_menu"><i class="fa fa-arrows-alt"></i></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["ISGS"]); ?></p>
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="mobile_sec col-md-12 col-xs-12"> 
    
                <div id="divThermal_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>thermal-generation';">
                    <p class="icon_menu"><i class="fa fa-file-medical-alt"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Thermal"]); ?></p>
    
                </div>  
    
                <div id="divHydro_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>hydro-generation';">
                    <p class="icon_menu"><i class="fa fa-tint"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Hydro"]); ?></p>
    
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="mobile_sec col-md-12 col-xs-12"> 
    
                <div id="divGas_mobile" class="col-md-6  col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>gas-generation';">
                    <p class="icon_menu"><i class="fa fa-fire"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Gas"]); ?></p>
    
                </div>  
    
                <div id="divNuclear_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>nuclear-generation';">
                    <p class="icon_menu"><i class="fa fa-atom"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Nuclear"]); ?></p>
    
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="mobile_sec col-md-12 col-xs-12"> 
    
                <div id="divRenewable_mobile" class="col-md-6  col-xs-6 nav_borderBox_left_top_Bottom Menu_section" onclick="window.location.href = '<?php echo base_url() ?>solar-generation';">
                    <p class="icon_menu"><i class="fa fa-sun"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Solar"]); ?></p>
    
                </div>  
    
                <div id="divFAQ_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top_Bottom Menu_section" onclick="window.location.href = '<?php echo base_url() ?>wind-generation';">
                    <p class="icon_menu"><i class="fa fa-asterisk"></i></p>
                    <p style="margin-bottom: 0px;"><?php echo str_replace(' ', '<br />', $Lang["Wind"]); ?></p>
                </div>  
            </div>
            <div class="clearfix"></div>
    
        </div>-->

    <div class="mobile_sec col-md-12 nav_details MenuMobileView" id="ShowMenuMobileView" style="display:none">
        <div class="col-md-12 col-xs-12">
            <div>
                <img class="padding-top-25" style="height: 80px;" src="<?php echo base_url(); ?>/images/ministry-ofpower_Mobile.png" alt="Ministry Of Power" title="Ministry Of Power"/>
            </div>
            <div>
                <div id="app_title" class="app_title ">
                    <p class="title_org1 text-white" style="margin-bottom: 0px;">Generation&nbsp;App</p>
                </div>
            </div>
            <!--            <div class="link_section col-md-1 col-xs-2" style="padding: 26px 0px !important;">
                            <a class="LangChangeLink" href="<?php echo base_url(); ?>national-summary">R</a>&nbsp;
                            <a class="LangChangeLink" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
                        </div>-->
        </div>
        <div class="clearfix"></div>
        <div class="margin-bottom-100">
            <div class="main-section-shadow-box while-box">
                <div class="col-md-12 col-xs-12">
                    <div id="divIndiaMap_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>interregional-transfer'">
                        <p class="icon_menu"><i class="fa fa-globe fa-lg"></i></i></p>
                        <p class="tiles-text"><?php echo $Lang["Home"]; ?></p>
                    </div> 

                    <div id="divGenerationData_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>generation-data';">
                        <p class="icon_menu"><i class="fa fa-bolt fa-lg" aria-hidden="true"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["GenerationData"]); ?></p>
                    </div> 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 

                    <div id="divDemandMet_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>demand-met';">
                        <p class="icon_menu"><i class="fa fa-balance-scale fa-lg"></i></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["DemandMet"]); ?></p> 
                    </div> 

                    <div id="divISGS_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>isgs-generation';">
                        <p class="icon_menu"><i class="fa fa-arrows-alt fa-lg"></i></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["ISGS"]); ?></p>
                    </div>  
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 

                    <div id="divThermal_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>thermal-generation';">
                        <p class="icon_menu"><i class="fa fa-file-medical-alt fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Thermal"]); ?></p>

                    </div>  

                    <div id="divHydro_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>hydro-generation';">
                        <p class="icon_menu"><i class="fa fa-tint fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Hydro"]); ?></p>

                    </div>  
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 

                    <div id="divGas_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>gas-generation';">
                        <p class="icon_menu"><i class="fa fa-fire fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Gas"]); ?></p>

                    </div>  

                    <div id="divNuclear_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>nuclear-generation';">
                        <p class="icon_menu"><i class="fa fa-atom fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Nuclear"]); ?></p>

                    </div>  
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 

                    <div id="divRenewable_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>solar-generation';">
                        <p class="icon_menu"><i class="fa fa-sun fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Solar"]); ?></p>

                    </div>  

                    <div id="divFAQ_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>wind-generation';">
                        <p class="icon_menu"><i class="fa fa-asterisk fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Wind"]); ?></p>
                    </div>  
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 
                    <div id="divNationalSummary_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>national-summary';">
                        <p class="icon_menu"><i class="fa fa-copy fa-lg"></i></p>
                        <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["NationalSummary"]); ?></p>
                    </div>  
                    <div id="divInterRegionalExchange_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>inter-regional-exchange';">
                        <p class="icon_menu"><i class="fa fa-exchange-alt fa-lg"></i></p>
                        <p class="tiles-text"><?php echo $Lang["InterRegionalExchange"]; ?></p>
                    </div> 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12"> 
                    <div id="divImportantLineER_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>important-line-ER';">
                        <p class="icon_menu"><i class="fa fa-equals fa-lg"></i></p>
                        <p class="tiles-text"><?php echo $Lang["ImpLineOfER"]; ?></p>
                    </div>  
                    <div class="col-md-6  col-xs-6 Menu_section">

                    </div> 
                </div>
                <div class="clearfix"></div>
            </div>  
        </div>
    </div>
    <div class="clearfix"></div>




</div>

<!--*****************Mobile View************************-->
<div class="col-md-12 no-padding-horizontal">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div id="myModal" class="modal fade " role="dialog" style="z-index: 1000000">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #000;background-repeat: no-repeat;color: #fff;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Filter Selection</h4>
                    </div>
                    <div>
                        <div class="col-md-12" style="padding-top: 15px;">
                            <div class="col-md-1"></div>
                            <div class="alert alert-danger" id="DateErrorSection" style="display: none">
                                <span id="DateError"></span>
                                <div class="col-md-1"></div></div>
                        </div>
                        <div class="option_details col-md-12" id="option_section" style="text-align: center;padding-top: 20px;">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 mobile_sec" style="text-align: center;">
                                        <div class="col-xs-5 mobile_sec label_right" style="color: #000;padding-right: 5px !important;">Date1:</div>
                                        <div class="col-xs-7 mobile_sec">
                                            <input class="form-control form-group txtDate" id="txt_vpDate2" type="text" style="float:left;" readonly>
                                            <i class="fa fa-calendar cal_icon" aria-hidden="true" onclick="ToShowCalender2();"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group mobile_sec" style="text-align: center;">
                                        <div class="col-xs-5 mobile_sec label_right" style="color: #000;;padding-right: 5px !important;">Date2:</div>
                                        <div class="col-xs-7 mobile_sec">
                                            <input class="form-control txtDate" id="txt_vpDate" type="text" style="float:left;" readonly>
                                            <i class="fa fa-calendar cal_icon" aria-hidden="true" onclick="ToShowCalender1();"></i>
                                        </div>
                                    </div> 
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="col-md-6 mobile_sec" style="text-align: center;">
                                        <div class="col-xs-5 mobile_sec" style="color: #000;">Region / State:</div>
                                        <div class="col-xs-7 mobile_sec">
                                            <?php if ($IsOther == 1) : ?>  
                                                <select class="form-control form-group dd_GenControl" id="ddlRegion1" >
                                                    <optgroup label="--Select Region / State--">
                                                        <option value="0" data-value="India">--All India--</option>
                                                    <optgroup label="North Region">
                                                        <option value="NR" data-value="Region">--All NR States--</option>
                                                        <option value="CHG" data-value="State">Chandigarh</option>
                                                        <option value="DL" data-value="State">Delhi</option>
                                                        <option value="HRN" data-value="State">Haryana</option>
                                                        <option value="HP" data-value="State">Himachal Pradesh</option>
                                                        <option value="JAK" data-value="State">Jammu and Kashmir</option>
                                                        <option value="PNB" data-value="State">Punjab</option>
                                                        <option value="RJ" data-value="State">Rajasthan</option>
                                                        <option value="UP" data-value="State">Uttar Pradesh</option>
                                                    </optgroup>
                                                    <optgroup label="West Region">
                                                        <option value="WR" data-value="Region">--All WR States--</option>
                                                        <option value="CTG" data-value="State">Chhattisgarh</option>
                                                        <option value="DNH" data-value="State">Dadra and Nagar Haveli</option>
                                                        <option value="DND" data-value="State">Daman and Diu</option>
                                                        <option value="GOA" data-value="State">Goa</option>
                                                        <option value="GJT" data-value="State">Gujarat</option>
                                                        <option value="MPD" data-value="State">Madhya Pradesh</option>
                                                        <option value="MHA" data-value="State">Maharashtra</option>
                                                    </optgroup>
                                                    <optgroup label="East Region">
                                                        <option value="ER" data-value="Region">--All ER States--</option>
                                                        <option value="BHR" data-value="State">Bihar</option>
                                                        <option value="JHK" data-value="State">Jharkhand</option>
                                                        <option value="ODI" data-value="State">Odisha</option>
                                                        <option value="SKM" data-value="State">Sikkim</option>
                                                        <option value="BGL" data-value="State">West Bengal</option>
                                                    </optgroup>
                                                    <optgroup label="North East Region">
                                                        <option value="NER" data-value="Region">--All NER States--</option>
                                                        <option value="ACP" data-value="State">Arunachal Pradesh</option>
                                                        <option value="ASM" data-value="State">Assam</option>
                                                        <option value="MIP" data-value="State">Manipur</option>
                                                        <option value="MGA" data-value="State">Meghalaya</option>
                                                        <option value="MZM" data-value="State">Mizoram</option>
                                                        <option value="NGD" data-value="State">Nagaland</option>
                                                        <option value="TPA" data-value="State">Tripura</option>
                                                    </optgroup>
                                                    <optgroup label="South Region">
                                                        <option value="SR" data-value="Region">--All SR States--</option>
                                                        <option value="AP" data-value="State">Andhra Pradesh</option>
                                                        <option value="KRT" data-value="State">Karnataka</option>
                                                        <option value="KRL" data-value="State">Kerala</option>
                                                        <option value="PU" data-value="State">Puducherry</option>
                                                        <option value="TND" data-value="State">Tamil Nadu</option>
                                                        <option value="TLG" data-value="State">Telangana</option>
                                                    </optgroup>
                                                    </optgroup>
                                                </select>  
                                            <?php endif; ?>
                                            <?php if ($IsOther == 3) : ?>     <!-- Wind Dropdown-->
                                                <select class="form-control form-group dd_GenControl" id="ddlRegion1" >
                                                    <option value="0" data-value="India">--All India--</option> 
                                                    <optgroup label="North Region">
                                                        <option value="NR" data-value="Region">--All NR States--</option>
                                                        <option value="RJ" data-value="State">Rajasthan</option>
                                                    </optgroup>
                                                    <optgroup label="West Region">
                                                        <option value="WR" data-value="Region">--All WR States--</option>
                                                        <option value="GJT" data-value="State">Gujarat</option>	
                                                        <option value="MHA" data-value="State">Maharashtra</option>
                                                        <option value="MPD" data-value="State">Madhya Pradesh</option>
                                                    </optgroup>
                                                    <optgroup label="South Region">
                                                        <option value="SR" data-value="Region">--All SR States--</option>
                                                        <option value="AP" data-value="State">Andhra Pradesh</option>
                                                        <option value="KRT" data-value="State">Karnataka</option>  
                                                        <option value="TND" data-value="State">Tamil Nadu</option>
                                                    </optgroup>
                                                </select>  
                                            <?php endif; ?>
                                            <?php if ($IsOther == 2) : ?>     <!-- Solar Dropdown-->
                                                <select class="form-control form-group dd_GenControl" id="ddlRegion1" >
                                                    <option value="0" data-value="India">--All India--</option> 
                                                    <optgroup label="North Region">
                                                        <option value="NR" data-value="Region">--All NR States--</option>
                                                        <option value="PNB" data-value="State">Punjab</option>
                                                        <option value="RJ" data-value="State">Rajasthan</option>
                                                        <option value="UP" data-value="State">Uttar Pradesh</option>
                                                    </optgroup>
                                                    <optgroup label="West Region">
                                                        <option value="WR" data-value="Region">--All WR States--</option>
                                                        <option value="GJT" data-value="State">Gujarat</option> 
                                                        <option value="MHA" data-value="State">Maharashtra</option>
                                                        <option value="MPD" data-value="State">Madhya Pradesh</option>
                                                        <option value="CTG" data-value="State">Chhattisgarh</option>
                                                    </optgroup>
                                                    <optgroup label="South Region">
                                                        <option value="SR" data-value="Region">--All SR States--</option>
                                                        <option value="AP" data-value="State">Andhra Pradesh</option>
                                                        <option value="KRT" data-value="State">Karnataka</option> 
                                                        <option value="TLG" data-value="State">Telangana</option>
                                                        <option value="TND" data-value="State">Tamil Nadu</option>
                                                    </optgroup>
                                                </select> 
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn_apply" class="btn btn-default" style="color: #fff; background-color: #9e7b0d;" data-dismiss="modal">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>


        <div class="MobileMenu" id="SlideMenu" style="display: none;">
            <?php $this->load->view('PartialView/Menu.php'); ?>  
        </div>

        <script type="text/javascript">

            $(document).ready(function (e) {
                var PageTitle = $("#CommonPageTitle").val();
                if (PageTitle === 'Frequency')
                {
                    $("#ddlRegion1").attr("disabled", true);
                    $("#ddlRegion1").css("opacity", "0.5");
                } else
                {
                    $("#ddlRegion1").attr("disabled", false);
                    $("#ddlRegion1").css("opacity", "1");
                }

            });


            function LoadSlideMenu()
            {
                $('#SlideMenu').show('slide', {direction: 'left'}, 200);
                $("body").css('overflow', 'hidden');
                $("footer").show('slide', {direction: 'bottom'}, 0);
            }

            $(document).mouseup(function (e)
            {
                var container = $("#SlideMenu");
                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0)
                {
                    container.hide('slide', {direction: 'left'}, 700);
                    $("#Page_data").css('position', 'static');
                    $("body").css('overflow', 'auto');

                }



            });

            $(document).ready(function (e)
            {
                setInterval(function () {
                    getCommonGenerationData();
                }, 90000);
            });


        </script>    
        <script type="text/javascript">
            $(window).on("scroll", function () {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    $(".mobile-footer-menu").css("padding-bottom", "10px");
                } else {
                    $(".mobile-footer-menu").css("padding-bottom", "60px");
                }
            });
        </script>



        <!--<div style="height:50px !important;background-color: red;position: fixed;bottom: 0px;">
            <p>Hello I am new footer</p>
        </div>-->
        <!-- Go to www.addthis.com/dashboard to customize your tools --> 
        <!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c947e1aaf6174af"></script>
        <script type="text/javascript">
                $('.addthis_button_compact').live('click', function() {
                var addthis_config =
                {
                    data_track_addressbar: true,
                    services_compact: "email, facebook, twitter, google_plusone_share, gmail, pinterest"
                }
                addthis.button('.addthis_button_compact', [addthis_config], [{}]);
                }); 
            </script>-->

    </div>
    <?php
    $this->load->view('PartialView/footer_mobileview');
    ?>
</body>
</html>



