<html>
    <head>
        <title>Login - Generation App</title>   

        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <link rel="icon" href="<?php echo base_url(); ?>/images/PosocoTitle.jpg" sizes="32x32" />

        <link href="<?php echo base_url(); ?>/css/responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/style_gen.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/map.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css">
        <link href="<?php echo base_url(); ?>/css/waitMe.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

        <script src="<?php echo base_url(); ?>/script/jquery-3.3.1.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/Common.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/bootstrap-toggle.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Anaheim" rel="stylesheet" type="text/css">

        <style>
            canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
            }
            .btn_Login
            {
                color: #fff; 
                background-color: #000;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="Page_data" style="position: static;">
                <div class="container-fluid">
                    <div class="col-md-12 col-xs-12 headerLogo mobile_sec header" id="myHeader">
                        <div class="col-md-12 col-xs-12 Header_padding">
                            <div class="col-md-12 col-xs-12 mobile_sec">
                                <div id="app_title" class="app_title mobile_sec">
                                     <a href="<?php echo base_url() ?>" class="title_org"><img style="height: 100px;" src="<?php echo base_url(); ?>/images/ministry-ofpower.png" alt="Ministry Of Power" title="Ministry Of Power"/></a>
                                    <p class="title_org no-margin-bottom">Generation App</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 mobile_sec">  
                                <div class="col-md-12 sub_header_Inter">
                                    <input type="text" class="IntertxtDate form-control DateFilterText" id="txt_date" readonly style="display: none !important;">
                                    <i class="fa fa-calendar cal_icon_inter" aria-hidden="true" onclick="ToShowCalender();" style="display: none;">
                                    </i>
                                    <span class="Generation_Name sub_heading_main" id="SelectedDate_"></span>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div> 
                <div class="clearfix"></div>            