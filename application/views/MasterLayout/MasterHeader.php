        <script type="text/javascript">

            $(window).resize(function () {
                if ($(window).width() < 980)
                {
                    var WindowSize = $(window).width();
                    var DiffBetweenRes = 980 - parseInt(WindowSize);
                    var AbsoluteValue = (DiffBetweenRes / 200) * 21;
                    var FinalResult = 100 - AbsoluteValue;

                    $("#india_map").animate({'zoom': FinalResult + '%'}, {speed: "fast"});
                } else
                {
                    $("#india_map").animate({'zoom': 100 + '%'}, {speed: "fast"});
                }
            });

            $(document).ready(function (e)
            {
                $("#int_Reg_tran").click(function (e) {
                    $("#Mobile_nav_det").css('display', 'none');
                    $("#innerDetails_desktop").show();
                    var WindowSize = $(window).width();
                    var DiffBetweenRes = 980 - parseInt(WindowSize);
                    var AbsoluteValue = (DiffBetweenRes / 200) * 21;
                    var FinalResult = 100 - AbsoluteValue;
                    var Height = (980 + parseInt(WindowSize)) / 5;
                    $("#IsIndiaMapShow").val(1);
                    $("#india_map").animate({'zoom': FinalResult + '%'}, {speed: "fast"});
                    $(".slider-vertical").animate({'height': Height + 'px'}, false);
                });

            });
        </script>
</html>

