<div class="clearfix"></div>
<div class="menuOpen">
    <div class="col-md-12 mobile_sec nav_details MenuView" id="DesktopMenu">
        <div class="col-md-4"></div>
        <a href="<?php echo base_url() ?>cxbTesting"><div class="col-md-2 nav_borderBox_left_top_desk box_section" id="divGen" style="border-top:none;">
                <p class="icon_menu"><i class="fa fa-globe"></i></p>
                <p>Home</p>
            </div> </a>
        <a href="<?php echo base_url() ?>national-summary"><div class="col-md-2 nav_borderBox_left_top_desk box_section" id="divNationalSum" style="border-top:none;">
                <p class="icon_menu"><i class="fa fa-copy"></i></p>
                <p>National Summary/ RTD</p>
            </div> </a>

        <a href="<?php echo base_url() ?>inter-regional-exchange"><div class="col-md-2 nav_borderBox_left_top_desk box_section" id="divInterRegExc" style="border-top:none;">
                <p class="icon_menu"><i class="fas fa-exchange-alt"></i></p>
                <p>Inter Regional Exchange</p>
            </div></a>

<!--        <a href="<?php echo base_url() ?>ATC-monitoring"><div class="col-md-2 nav_borderBox_left_top_desk box_section" id="ATCMonitor">
                <p class="icon_menu"><i class="fa fa-eye"></i></p>
                <p>ATC Monitoring</p>
            </div> </a> -->

        <a href="<?php echo base_url() ?>important-line-ER"><div class="col-md-2 nav_borderBox_left_top_desk box_section" id="ImpLineOfER">
                <p class="icon_menu"><i class="fa fa-equals"></i></p>
                <p>Important Lines of ER</p>
            </div> </a>
        <div class="col-md-4"></div>
    </div>



    <div class="clearfix"></div>
</div>
</div>

<div class="MobileMenu" id="SlideMenu" style="display: none;">
    <?php $this->load->view('PartialView/Menu.php'); ?>  
</div>

<!--<div class="col-md-12 mobile_sec nav_details MenuMobileView" id="ShowMenuMobileView" style="display:none">


    <div class="col-md-12 col-xs-12 mobile_sec border_BottomHeading1">
        <div class="col-md-1 col-xs-2" style="padding: 2%;">
            <img style="height: 60px;" src="<?php echo base_url(); ?>/images/ministry-ofpower_Mobile.png" alt="Ministry Of Power" title="Ministry Of Power"/>
        </div>
        <div class="col-md-10 col-xs-8 min_padding">
            <div id="app_title" class="app_title col-md-11">
                <p class="title_org1" style="margin-bottom: 0px;">Generation&nbsp;App</p>
            </div>
        </div>
        <div class="link_section col-md-1 col-xs-2" style="padding: 26px 0px !important;">
            <a class="LangChangeLink" href="<?php echo base_url(); ?>national-summary">R</a>&nbsp;
            <a class="LangChangeLink" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
        </div>
    </div>

    <div class="mobile_sec col-md-12 col-xs-12">
        <div id="divIndiaMap_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>interregional-transfer'">
            <p class="icon_menu"><i class="fa fa-globe"></i></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo $Lang["InterRegional"]; ?><br /><?php echo $Lang["Transmission"]; ?></p>
        </div> 

        <div id="divGenerationData_mobile" class="col-md-6 col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>generation-data';">
            <p class="icon_menu"><i class="fa fa-bolt" aria-hidden="true"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["GenerationData"]); ?></p>
        </div> 
    </div>
    <div class="clearfix"></div>
    <div class="mobile_sec col-md-12 col-xs-12"> 

        <div id="divDemandMet_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>demand-met';">
            <p class="icon_menu"><i class="fa fa-balance-scale"></i></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["DemandMet"]); ?></p> 
        </div> 

        <div id="divISGS_mobile" class="col-md-6 col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>isgs-generation';">
            <p class="icon_menu"><i class="fa fa-arrows-alt"></i></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["ISGS"]); ?></p>
        </div>  
    </div>
    <div class="clearfix"></div>
    <div class="mobile_sec col-md-12 col-xs-12"> 

        <div id="divThermal_mobile" class="col-md-6 col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>thermal-generation';">
            <p class="icon_menu"><i class="fa fa-file-medical-alt"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Thermal"]); ?></p>

        </div>  

        <div id="divHydro_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>hydro-generation';">
            <p class="icon_menu"><i class="fa fa-tint"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Hydro"]); ?></p>

        </div>  
    </div>
    <div class="clearfix"></div>
    <div class="mobile_sec col-md-12 col-xs-12"> 

        <div id="divGas_mobile" class="col-md-6  col-xs-6 nav_borderBox_left_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>gas-generation';">
            <p class="icon_menu"><i class="fa fa-fire"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Gas"]); ?></p>

        </div>  

        <div id="divNuclear_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top Menu_section" onclick="window.location.href = '<?php echo base_url() ?>nuclear-generation';">
            <p class="icon_menu"><i class="fa fa-atom"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Nuclear"]); ?></p>

        </div>  
    </div>
    <div class="clearfix"></div>
    <div class="mobile_sec col-md-12 col-xs-12"> 

        <div id="divRenewable_mobile" class="col-md-6  col-xs-6 nav_borderBox_left_top_Bottom Menu_section" onclick="window.location.href = '<?php echo base_url() ?>solar-generation';">
            <p class="icon_menu"><i class="fa fa-sun"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Solar"]); ?></p>

        </div>  

        <div id="divFAQ_mobile" class="col-md-6  col-xs-6 nav_borderBox_right_top_Bottom Menu_section" onclick="window.location.href = '<?php echo base_url() ?>wind-generation';">
            <p class="icon_menu"><i class="fa fa-asterisk"></i></p>
            <p style="margin-bottom: 0px;font-size: 13px;"><?php echo str_replace(' ', '<br />', $Lang["Wind"]); ?></p>
        </div>  
    </div>
    <div class="clearfix"></div>

</div>-->



<div class="col-md-12 mobile_sec nav_details MenuMobileView" id="ShowMenuMobileView" style="display:none">
    <div class="col-md-12 col-xs-12">
        <div>
            <img class="padding-top-25" style="height: 80px;" src="<?php echo base_url(); ?>/images/ministry-ofpower_Mobile.png" alt="Ministry Of Power" title="Ministry Of Power"/>
        </div>
        <div>
            <div id="app_title" class="app_title ">
                <p class="title_org1 text-white" style="margin-bottom: 0px;">Generation&nbsp;App</p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="margin-bottom-100">
        <div class="main-section-shadow-box while-box">
            <div class="col-md-12 col-xs-12">
                <div id="divIndiaMap_mobile" class="col-md-6 col-xs-6  Menu_section" onclick="window.location.href = '<?php echo base_url() ?>interregional-transfer'">
                    <p class="icon_menu"><i class="fa fa-globe fa-lg"></i></i></p>
                    <p class="tiles-text"><?php echo $Lang["Home"]; ?></p>
                </div> 

                <div id="divGenerationData_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>generation-data';">
                    <p class="icon_menu"><i class="fa fa-bolt fa-lg" aria-hidden="true"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["GenerationData"]); ?></p>
                </div> 
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12">
                <div id="divDemandMet_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>demand-met';">
                    <p class="icon_menu"><i class="fa fa-balance-scale fa-lg"></i></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["DemandMet"]); ?></p> 
                </div> 
                <div id="divISGS_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>isgs-generation';">
                    <p class="icon_menu"><i class="fa fa-arrows-alt fa-lg"></i></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["ISGS"]); ?></p>
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12">
                <div id="divThermal_mobile" class="col-md-6 col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>thermal-generation';">
                    <p class="icon_menu"><i class="fa fa-file-medical-alt fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Thermal"]); ?></p>
                </div>  
                <div id="divHydro_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>hydro-generation';">
                    <p class="icon_menu"><i class="fa fa-tint"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Hydro"]); ?></p>
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12"> 
                <div id="divGas_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>gas-generation';">
                    <p class="icon_menu"><i class="fa fa-fire fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Gas"]); ?></p>
                </div>  
                <div id="divNuclear_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>nuclear-generation';">
                    <p class="icon_menu"><i class="fa fa-atom fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Nuclear"]); ?></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12">
                <div id="divRenewable_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>solar-generation';">
                    <p class="icon_menu"><i class="fa fa-sun fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Solar"]); ?></p>
                </div>  
                <div id="divFAQ_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>wind-generation';">
                    <p class="icon_menu"><i class="fa fa-asterisk fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["Wind"]); ?></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12"> 
                <div id="divNationalSummary_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>national-summary';">
                    <p class="icon_menu"><i class="fa fa-copy fa-lg"></i></p>
                    <p class="tiles-text"><?php echo str_replace(' ', '<br />', $Lang["NationalSummary"]); ?></p>
                </div>  
                <div id="divInterRegionalExchange_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>inter-regional-exchange';">
                    <p class="icon_menu"><i class="fa fa-exchange-alt fa-lg"></i></p>
                    <p class="tiles-text"><?php echo $Lang["InterRegionalExchange"]; ?></p>
                </div> 
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12"> 
                <div id="divImportantLineER_mobile" class="col-md-6  col-xs-6 Menu_section" onclick="window.location.href = '<?php echo base_url() ?>important-line-ER';">
                    <p class="icon_menu"><i class="fa fa-equals fa-lg"></i></p>
                    <p class="tiles-text"><?php echo $Lang["ImpLineOfER"]; ?></p>
                </div>  
                <div class="col-md-6  col-xs-6 Menu_section">

                </div> 
            </div>
            <div class="clearfix"></div>
        </div>
    </div>        
</div>
<div class="clearfix"></div>
<?php
$this->load->view('PartialView/footer_mobileview');
?>

</body>
</html>


<script type="text/javascript">

    $(document).ready(function (e)
    {
        $("#DesktopMenu").addClass("Menu_scroll");
        $(".box_section").css("opacity", '0.7');
        $(".box_section").mouseover(function (e) {
            var IsPositionLast = $("#DesktopMenu").hasClass("Menu_scroll");
            if (IsPositionLast == true)
            {
                $(this).animate({'top': '-50px'}, 100);
            }
        });

        $(".box_section").mouseleave(function (e) {
            $(".box_section").animate({'top': '0'}, 0);
        });

    });




    function ShowNewMenu()
    {
        var WindowWidth = $(window).width();
        if (WindowWidth >= 991)
        {
            var IsMenuVisible = 0;
            var DivMargin = $("#DesktopMenu").css('bottom');
            if (DivMargin != "0px") {
                IsMenuVisible = 1;
            }
            if (IsMenuVisible == 1)
            {
                // $("#DesktopMenu").slideUp();
                // $("#DesktopMenu").css('bottom','-60px');
                $("#DesktopMenu").addClass("Menu_scroll");
                // $(".box_section").css('opacity', "0.7");
                $("#UpArrow").fadeIn();
                $("#DownArrow").hide();

            } else
            {
                // $("#DesktopMenu").slideDown();
                $("#UpArrow").hide();
                $("#DownArrow").fadeIn();
                //$(".box_section").css('opacity', "0.7");
                $("#DesktopMenu").addClass("Menu_scroll");
            }

        } else
        {
            var IsMenuVisible = 0;
            if ($("#ShowMenuMobileView").css('display') === "block") {
                IsMenuVisible = 1;
            }
            if (IsMenuVisible == 1)
            {
                $("#ShowMenuMobileView").slideUp();
                $("#UpArrow").fadeIn();
                $("#DownArrow").hide();

            } else
            {
                $("#ShowMenuMobileView").slideDown();
                $("#UpArrow").hide();
                $("#DownArrow").fadeIn();
            }
        }

    }


    function LoadSlideMenu()
    {
        $('#SlideMenu').show('slide', {direction: 'left'}, 200);
        $("body").css('overflow', 'hidden');

    }

    $(document).mouseup(function (e)
    {
        var container = $("#SlideMenu");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide('slide', {direction: 'left'}, 700);
            $("body").css('overflow', 'auto');
        }
    });


//    function OpenMenu()
//    {
//        var IsVisible = $("#ShowMenuMobileView").css('display');
//        if (IsVisible == "block")
//        {
//            $("#ShowMenuMobileView").slideUp(1000);
//
//        } else
//        {
//            $("#ShowMenuMobileView").slideDown(1000);
//            //$("#Page_data").css('display', 'none');
//        }
//        $('#SlideMenu').hide('slide', {direction: 'left'}, 700);
//        //$("body").css('overflow','auto');
//
//    }


    function OpenMenu()
    {
        var IsVisible = $("#ShowMenuMobileView").css('display');
        if (IsVisible == "block")
        {
            $("#ShowMenuMobileView").slideUp(1000);

        } else
        {
            $("#ShowMenuMobileView").slideDown(600);
            //$("#Page_data").css('display', 'none');
        }
        $('#SlideMenu').hide('slide', {direction: 'left'}, 700);
        $("body").css('overflow', 'auto');
        if ($("#nationalsummary_view").length > 0) {
            $("#nationalsummary_view").css("visibility", "hidden");
        }
        if ($("#interregionalexchange_view").length > 0) {
            $("#interregionalexchange_view").css("visibility", "hidden");
        }
        if ($("#interregionalexchange_view").length > 0) {
            $("#interregionalexchange_view").css("visibility", "hidden");
        }
        if ($("#importantlinesofer_view").length > 0) {
            $("#importantlinesofer_view").css("visibility", "hidden");
        }
        $("#IndiaMapDiv").css('display', 'none');
    }


</script>    
<style type="text/css">
    .MobileMenuSection {
        border-right: 1px solid #e8d496;
        /*        padding: 0px;
                height: 80%;*/
        overflow-y: auto;
    }
    .MobileMenu {
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        width: 70%;
        /*height: 99%;*/
        z-index: 200;
    }
</style>
<script type="text/javascript">
    $(window).on("scroll", function () {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            $(".mobile-footer-menu").css("padding-bottom", "10px");
            //console.log('12');        
        } else {
            $(".mobile-footer-menu").css("padding-bottom", "60px");
            //console.log('11');       
        }
    });

//    $(window).scroll(function () {
//        if (($(window).innerHeight() + $(window).scrollTop()) >= $("body").height()) {
//            console.log('12');
//        }
//        else{
//            console.log('11');
//        }
//    });
</script>