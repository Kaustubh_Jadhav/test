<html>
    <head>

        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <title><?php echo $Lang["Title"]; ?> - Generation App</title>   
        <link rel="icon" href="<?php echo base_url(); ?>/images/PosocoTitle.jpg" sizes="32x32" />

        <link href="<?php echo base_url(); ?>/css/responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/map.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css">
        <link href="<?php echo base_url(); ?>/css/waitMe.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/footable.metro.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

        <script src="<?php echo base_url(); ?>/script/jquery-3.3.1.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>/script/Chart.Bar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/Chart.PieceLabel.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/waitMe.min.js" type="text/javascript"></script>
        <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/Common.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.paginate.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.filter.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/footable.sort.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>script/Chart.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/Chart.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/hammer.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>script/chartjs-plugin-zoom.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>/script/bootstrap-toggle.min.js"></script>
        <script src="<?php echo base_url(); ?>/script/jquery.slimscroll.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Anaheim" rel="stylesheet" type="text/css">
        <style>
            canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="Page_data">                
<!--                    <div class="col-md-12 col-xs-12 headerLogo mobile_sec header" id="myHeader">
                        <div class="col-md-1 col-xs-3 Header_Logo">                    
                            <a href="<?php echo base_url() ?>"><img style="height: 100px;" src="<?php echo base_url(); ?>/images/ministry-ofpower.png" alt="Ministry Of Power" title="Ministry Of Power"/></a>
                        </div>        
                        <div class="col-md-11 col-xs-12 Header_padding">
                            <div class="col-md-12 col-xs-12 mobile_sec border_BottomHeading">
                                <div class="col-xs-2 Menu_Link">
                                    <i class="fa fa-bars link_section" onclick="LoadSlideMenu();"></i>
                                </div>
                                <div class="col-md-11 col-xs-8 min_padding">
                                    <div id="app_title" class="app_title mobile_sec col-md-11">
                                        <span class="title_org" style="padding-bottom: 10px;"><?php echo $Lang["Generation"] ?>&nbsp;<?php echo $Lang["App"] ?></span>
                                    <p></p>
                                    </div>
                                    <div class="app_title mobile_sec col-md-11" id="page_title">
                                        <span class="page_org"></span>
                                    </div> 
                                </div>
                                <div class="col-md-1 col-xs-2 link_section">
                                    <a class="LangChangeLink mob_link" href="<?php echo base_url(); ?>cxbTesting">G</a>
                                    <a class="LangChangeLink desktop_link" href="<?php echo base_url(); ?>cxbTesting">GEN</a>
                                    <a class="LangChangeLink" onclick="TranslatePage()">हिं</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 mobile_sec"> 
                                <div class="col-md-10 sub_header_Inter">
                                    <span class="Generation_Name sub_heading_main" id="MainDetails">gg</span>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>-->
                <div class="container-fluid">
                    <div class="col-md-12 col-xs-12 headerLogo mobile_sec header" id="myHeader">
                        <div class="Header_Logo">                    
                            <a href="<?php echo base_url() ?>"><img style="height: 100px;" src="<?php echo base_url(); ?>/images/ministry-ofpower.png" alt="Ministry Of Power" title="Ministry Of Power"/></a>
                        </div>        
                        <div class="col-md-12 col-xs-12 Header_padding">
                            <div class="col-md-12 col-xs-12 mobile_sec border_BottomHeading">
                                <div class="col-md-2 col-xs-2">
                                    <div class="Menu_Link">
                                    <i class="menu-i fa fa-bars link_section" onclick="LoadSlideMenu();"></i>
                                    <i class="back-i fa fa-chevron-left link_section" onclick="location.href = 'cxbTesting';"></i>                                    
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 min_padding">
                                    <div id="app_title" class="app_title mobile_sec">
                                        <span class="title_org" style="padding-bottom: 10px;"><?php echo $Lang["Generation"] ?>&nbsp;<?php echo $Lang["App"] ?></span>
                                    <p></p>
                                    </div>
                                    <div class="app_title mobile_sec" id="page_title">
                                        <span class="page_org"></span>
                                    </div> 
                                </div>
                                <div class="col-md-2 link_section col-xs-2">
                                    <!--<a class="LangChangeLink mob_link" href="<?php echo base_url(); ?>cxbTesting">G</a>-->
                                    <!--<a class="LangChangeLink desktop_link" href="<?php echo base_url(); ?>cxbTesting">GEN</a>-->
                                    <!--<a class="LangChangeLink" onclick="TranslatePage()">हिं</a>-->
                                    
                                    <a class="LanguageTranslateText_en_rtd mob_link" href="<?php echo base_url(); ?>cxbTesting">G</a>
                                    <a class="LanguageTranslateText_en_rtd desktop_link" href="<?php echo base_url(); ?>cxbTesting">GEN</a>
                                    <a class="LanguageTranslateText_en_rtd hindi_tran" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
                                    <a class="LanguageTranslateText_en_rtd faq_link" href="<?php echo base_url(); ?>faq">FAQ</a>
             
                                  
                                
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-xs-12 mobile_sec"> 
                                <div class="col-md-12 col-xs-12 sub_header_Inter">
                                    <span class="Generation_Name sub_heading_main" id="MainDetails"></span>&nbsp;<span id="Date_RTD"></span></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <script type="text/javascript">
                    
                    $(document).ready(function (e){
                       var Date_RTD = "<?php echo $_SESSION["DataForDateRTD"]; ?>";
                        $("#Date_RTD").text(Date_RTD);
                        
                        var WindowWidth = $(window).width();
                        //alert(WindowWidth);
                        if (WindowWidth <= 991)
                        {
                            //alert('1');
                            $("#app_title").hide();
                            $("#page_title").show();
                            var PageTitle = $("#CommonPageTitle").val();
                            $(".page_org").text(PageTitle);
                           // $(".Generation_Name").hide();
                            $("#DesktopTitle").hide();
                           
                            $(".divTable").hide();
                            var IsMenushow = $("#IsMenuShow").val();
                            if (IsMenushow == 1)
                            {
                                OpenMenu();
                            }
                            //alert('2');

                        } else
                        {
                            $("#app_title").show();
                            $("#page_title").hide();
                           // $(".Generation_Name").show();
                            $("#DesktopTitle").show();
                            $(".divTable").show();
                        }
                    });
                    
                    $(window).resize(function () {

                        var WindowWidth = $(window).width();
                        if (WindowWidth <= 991)
                        {
                            $("#app_title").hide();
                            $("#page_title").show();
                            $("#DesktopTitle").hide();
                            var PageTitle = $("#CommonPageTitle").val();
                            $(".page_org").text(PageTitle);
                            //$(".Generation_Name").hide();
                            var IsDataShow = $("#inner_chart_Line1").css("display");
                            if (IsDataShow == "block")
                            {
                                $(".divTable").hide();
                                $("#div_data1").hide();
                            } else
                            {
                                $(".divTable").show();
                                $("#div_data1").show();
                            }
                            $("#Page_data").css("overflow-y", 'hidden');


                        } else
                        {

                            $("#app_title").show();
                            $("#page_title").hide();
                            $("#DesktopTitle").show();
                            //$(".Generation_Name").show();
                            $(".divTable").show();
                            $("#div_data1").show();
                            $("#ShowMenuMobileView").hide();
                        }

                    });
                    
                    
                      $(document).ready(function (e)
                    {

                        $("#DesktopMenu").addClass("Menu_scroll");
                        //$(".box_section").css("opacity", '0.7');
                        $(".box_section").mouseover(function (e) {
                            var IsPositionLast = $("#DesktopMenu").hasClass("Menu_scroll");
                            if (IsPositionLast == true)
                            {
                                $(this).animate({'top': '-50px'}, 100);
                            }
                        });

                        $(".box_section").mouseleave(function (e) {
                            $(".box_section").animate({'top': '0'}, 0);
                        });

                        $('#toggle-chart').change(function () {
                            var IsDataAvailable = $("#IsDataPresent").val();

                            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
                            if (IsDataAvailable != 0)
                            {
                                if (!$("#inner_chart_Line1").is(':visible'))
                                {

                                    $("#inner_chart_Line1").fadeIn(2000);
                                    $(".divTable").fadeOut(0);
                                    setInterval(function () {
                                        $(".waitMe").hide();
                                    }, 2000);
                                    $(".slimScrollDiv").hide();
                                    $("#div_data1").fadeOut(0);
                                    $("#Legend").show();
                                } else
                                {

                                    $("#inner_chart_Line1").fadeOut(0);
                                    $(".divTable").fadeIn(2000);
                                    setInterval(function () {
                                        $(".waitMe").hide();

                                    }, 2000);
                                    $(".slimScrollDiv").show();
                                    $("#div_data1").fadeIn(2000);
                                    $("#Legend").hide();
                                }
                            } else
                            {
                                $(".waitMe").hide();
                            }
                        });



                        $(function () {
                            $('#toggle-chart').bootstrapToggle({
                                on: 'Table',
                                off: 'Chart'
                            });
                        });



                    });
                    
                    $(window).resize(function () {

                        var WindowWidth = $(window).width();
                        if (WindowWidth <= 991)
                        {
                            $("#app_title").hide();
                        } else
                        {
                            $("#app_title").show();
                        }

                    });                         
                    
                    
                </script>

