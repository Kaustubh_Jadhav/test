<div class="container-fluid main-section-shadow-box" id="desktop_data" style="height: 100%;">
    <div class="col-md-12 col-xs-12 padding_zero" id="main_dashboard">
        <div class="col-md-4"></div>
        <div class="col-xs-12 col-md-4 login-container">
            <section class="login" id="login">
                <h2 style="text-align: center;">Login</h2>
                <form class="login-form" action="authenticate" method="POST">
                    <div class="col-md-12 alert alert-danger form-group" role="alert" id="ErrorMesssageDiv" style="display: none;">
                        <span id="ErrorMessage" style="color: #fa3031"></span>
                    </div>  
                    <input type="text" class="login-input" placeholder="Username" required autofocus name="username" id="username"/>
                    <input type="password" class="login-input" placeholder="Password" required name="password" id="password"/>
                    <div class="submit-container">
                        <button type="button" class="btn_Login btn" onclick = "validate();">SIGN IN</button>
                    </div>
                </form>
            </section>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<script type="text/javascript">
    function validate()
    {
        var Username = $("#username").val();
        var Password = $("#password").val();
        if (Username != "")
        {
            if (Password != "")
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>authenticate",
                    type: "post",
                    data: {'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>', 'username': Username, 'password': Password},
                    success: function (response) {
                        if (response == "")
                        {
                            location.href = 'cxbTesting';

                        } else
                        {
                            $("#ErrorMesssageDiv").show();
                            $("#ErrorMessage").text(response);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#ErrorMesssageDiv").show();
                        $("#ErrorMessage").text(errorThrown);
                    }
                });
            } else
            {
                $("#password").focus();
                $("#ErrorMesssageDiv").show();
                $("#ErrorMessage").text("Please provide Password");
            }
        } else
        {
            $("#username").focus();
            $("#ErrorMesssageDiv").show();
            $("#ErrorMessage").text("Please provide username");
        }
    }

</script>

<style type="text/css">
    body {
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    /* Animation Keyframes */
    @keyframes scale_header {
        0%   {max-height: 0px; margin-bottom: 0px; opacity: 0;}
        100% {max-height: 117px; margin-bottom: 25px; opacity: 1;}
    }

    @keyframes input_opacity {
        0%   {transform: translateY(-10px); opacity: 0}
        100% {transform: translateY(0px); opacity: 1}
    }

    @keyframes text_opacity {
        0% {color: transparent;}
    }

    @keyframes error_before {
        0%   {height: 5px; background: rgba(0, 0, 0, 0.156); color: transparent;}
        10%  {height: 117px; background: #FFFFFF; color: #C62828}
        90%  {height: 117px; background: #FFFFFF; color: #C62828}
        100% {height: 5px; background: rgba(0, 0, 0, 0.156); color: transparent;}
    }


    /* Login Form */
    .login-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        position: relative;
        height: auto;
        padding: 5px;
        box-sizing: border-box;
    }

    .login-container img {
        width: 200px;
        margin: 0 0 20px 0;
    }

    .login-container p {
        align-self: flex-start;      
        font-size: 0.8rem;
        color: rgba(0, 0, 0, 0.5);
    }

    .login-container p a {
        color: rgba(0, 0, 0, 0.4);
    }

    .login {
        position: relative;
        width: 100%;
        padding: 10px;
        margin: 0 0 10px 0;
        box-sizing: border-box;
        border-radius: 3px;
        background: #fff;
        overflow: hidden;
        animation: input_opacity 0.2s cubic-bezier(.55, 0, .1, 1);
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 1px 5px 0 rgba(0, 0, 0, 0.12),
            0 3px 1px -2px rgba(0, 0, 0, 0.2);
    }

    .login > header {
        position: relative;
        width: 100%;
        padding: 10px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        background: #31b0a4;
        font-size: 1.3rem;
        color: #FAFAFA;
        animation: scale_header 0.6s cubic-bezier(.55, 0, .1, 1), text_opacity 1s cubic-bezier(.55, 0, .1, 1);
        box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.14),
            0px 1px 5px 0px rgba(0, 0, 0, 0.12),
            0px 3px 1px -2px rgba(0, 0, 0, 0.2);
    }

    .login > header:before {
        content: '';
        display: flex;
        justify-content: center;
        align-items: center;
        position: absolute;
        width: 100%;
        height: 5px;
        padding: 10px;
        margin: -10px 0 0 -10px;
        box-sizing: border-box;
        background: rgba(0, 0, 0, 0.156);      
        font-size: 0.9rem;
        color: transparent;
        z-index: 5;
    }

    .login.error_1 > header:before,
    .login.error_2 > header:before {
        animation: error_before 3s cubic-bezier(.55, 0, .1, 1);
    }

    .login.error_1 > header:before {
        content: 'Invalid username or password!';
    }

    .login.error_2 > header:before {
        content: 'Invalid or expired Token!';
    }

    .login > header h2 {
        margin: 50px 0 10px 0;
    }

    .login > header h4 {
        font-size: 0.7em;
        animation: text_opacity 1.5s cubic-bezier(.55, 0, .1, 1);
        color: rgba(255, 255, 255, 0.4);
    }

    /* Form */
    .login-form {
        padding: 15px;
        box-sizing: border-box;
    }


    /* Inputs */
    .login-input {
        position: relative;
        width: 100%;
        padding: 10px 5px;
        margin: 0 0 25px 0;
        border: none;
        border-bottom: 2px solid rgba(0, 0, 0, 0.2);
        box-sizing: border-box;
        background: transparent;
        font-size: 1rem;       
        font-weight: 500;
        opacity: 1;
        animation: input_opacity 0.8s cubic-bezier(.55, 0, .1, 1);
        transition: border-bottom 0.2s cubic-bezier(.55, 0, .1, 1);
    }

    .login-input:focus {
        outline: none;
        border-bottom: 2px solid #000;
    }


    /* Submit Button */
    .submit-container {
        text-align: center;
        flex-direction: row;
        justify-content: flex-end;
        position: relative;
        padding: 10px;
        margin: 35px -25px -25px -25px;
    }

    .login-button {
        padding: 10px;
        border: none;
        border-radius: 3px;
        background: transparent;
        font-size: 0.9rem;
        font-weight: 500;
        color: #000;
        cursor: pointer;
        opacity: 1;
        animation: input_opacity 0.8s cubic-bezier(.55, 0, .1, 1);
        transition: background 0.2s ease-in-out;
    }

    .login-button.raised {
        padding: 5px 10px;
        color: #FAFAFA;
        background: #000;
        box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.137255),
            0px 1px 5px 0px rgba(0, 0, 0, 0.117647),
            0px 3px 1px -2px rgba(0, 0, 0, 0.2);
    }

    .login-button:hover {
        background: rgba(0, 0, 0, 0.05);
    }

    .login-button.raised:hover {
        background: #FDAB43;
    }

    .btn_Login:hover
    {
        color: #fff;
        background-color: #000;
    }
    .btn_Login:active
    {
        color: #fff;
        background-color: #000;
    }
    .btn_Login:focus
    {
        color: #fff;
        background-color: #000;
    }

</style>