<div>
    <div class="gendata">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" value="<?php echo $Frequency ?>" id="CommonPageTitle">      
        <!--        <div class="divToggel" style="text-align:right;">
                    <div class="col-xs-11">
                        <input class="divToggel" data-style="ios" id="toggle-chart" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-table' aria-hidden='true'></i>" data-off="<i class='fa fa-line-chart'></i>">
                    </div>
                    <div class="col-xs-1"></div>
                </div>-->
        <div class="clearfix"></div>
        <?php $this->load->view('PartialView/ErrorViewShow.php'); ?> 
        <div class="Datatable">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-8 col-xs-12 legend_section" id="Legend"></div>
                <div class="col-md-4"></div>
            </div>
            <div class="col-md-12 add-overflow-x">
                <div class="col-md-8 divChart" id="inner_chart_Line1">
                    <div id='graph'></div>
                </div>
                <div class="col-md-4 divTable no-padding-horizontal">
                    <div class=" div_datatable" id="div_data">
                        <table id="Frequency_datatable" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th><span>Time</span></th>
                                    <th data-type="numeric"><span id="lblYesterdayDate">  <?php echo $Yesterday; ?></span></th>
                                    <th data-type="numeric"><span id="lblTodayDate">   <?php echo $Today; ?></span></th>
                                    <th data-type="numeric"><span> <?php echo $Diff; ?></span></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <script src="<?php echo base_url(); ?>script/dygraph.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/dygraph.min.css" />
    <script type="text/javascript">

        function getCommonGenerationData()
        {
            var date1 = $("#txt_vpDate").val();
            var date2 = $("#txt_vpDate2").val();
            var StateCode = 0;
            var RegionCode = 0;
            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
            var d = date1.toString().split(" ");
            var dd = d[0];
            var months = {
                'Jan': '01',
                'Feb': '02',
                'Mar': '03',
                'Apr': '04',
                'May': '05',
                'Jun': '06',
                'Jul': '07',
                'Aug': '08',
                'Sep': '09',
                'Oct': '10',
                'Nov': '11',
                'Dec': '12'
            }

            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;

            var Date1 = yy + "-" + mm + "-" + dd;

            var d = date2.toString().split(" ");
            var dd = d[0];
            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;

            var Date2 = yy + "-" + mm + "-" + dd;

            if ((typeof StateCode != 'undefined') && (typeof RegionCode != 'undefined'))
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>getFrequencyData",
                    datatype: "JSON",
                    data: {
                        'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>',
                        "SelectedDate1": Date1,
                        "SelectedDate2": Date2
                    },
                    type: "POST",
                    success: function (data) {

                        if (data["0"] == "-1")
                        {

                            $("#div_data").hide();
                            $("#div_error").show();
                            $('#inner_chart_Line1').html("");
                            $('#inner_chart_Line1').html("<div id='dataStuctureChart'></div>");
                            $("#DisplayError").show();
                            $("#ErrorMessage").text(data["1"]["msg"]);

                        } else
                        {
                            $("#div_error").hide();
                            var Today = data["SelectedDate1"];
                            var Yesterday = data["SelectedDate2"];
                            $("#lblTodayDate").text(Today);
                            $("#lblYesterdayDate").text(Yesterday);
                            $("#lblDatedData").text(Today);
                            var FrequencyTodayData = data["0"]["result1"]["Result"]["0"];
                            var FrequencyYesterdayData = data["1"]["result2"]["Result"]["0"];
                            var newRowContent = "";
                            var TimeslotTodayList = [];
                            var TimeslotYesterdayList = [];
                            var TimeslotFinalList = [];
                            var Todaydatalist = [];
                            var Yesterdaydatalist = [];
                            var IsDetailsAvailable = 0;

                            if (data["0"]["result1"]["Count_"] > 0) {
                                $.each(FrequencyTodayData, function (key, value) {
                                    TimeslotTodayList.push(value.TimeSlot);
                                    TimeslotFinalList.push(value.TimeSlot);
                                    Todaydatalist.push(value.AllIndiaFreq);

                                });
                            }
                            //create array for yesterday data
                            if (data["1"]["result2"]["Count_"] > 0)
                            {
                                $.each(FrequencyYesterdayData, function (key, value) {
                                    TimeslotYesterdayList.push(value.TimeSlot);
                                    Yesterdaydatalist.push(value.AllIndiaFreq);
                                });
                            }
                            //merge timeslot & remove duplicate
                            $.unique(($.merge(TimeslotFinalList, TimeslotYesterdayList)).sort());
                            TimeslotTodayList.sort();//sorting
                            TimeslotYesterdayList.sort();//sorting
                            var i1 = 0;//index of today
                            var i2 = 0;//index of yesterday
                            var TodaydatalistNew = [];//Create new array for chart
                            var YesterdaydatalistNew = [];//Create new array for chart
                            var str = [];
                            var str1 = "Date," + Yesterday + "," + Today + "\n";
                            //bind gridview/ table 
                            for (var i = 0; i < TimeslotFinalList.length; i++)
                            {

                                if ((Yesterdaydatalist[i2] > 0 || Todaydatalist[i1] > 0) && IsDetailsAvailable != 1)
                                {
                                    IsDetailsAvailable = 1;
                                }
                                if ((TimeslotTodayList[i1] == TimeslotFinalList[i]) && (TimeslotYesterdayList[i2] != TimeslotFinalList[i]))
                                {
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>-</td><td style='display: table-cell;'>" + Todaydatalist[i1] + "</td><td style='display: table-cell;'>-</td></tr>";
                                    TodaydatalistNew.push(Todaydatalist[i1]);
                                    YesterdaydatalistNew.push(null)
                                    i1++;
                                } else if ((TimeslotTodayList[i1] != TimeslotFinalList[i]) && (TimeslotYesterdayList[i2] == TimeslotFinalList[i]))
                                {
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>" + Yesterdaydatalist[i2] + "</td><td style='display: table-cell;'>-</td><td style='display: table-cell;'> - </td></tr>";
                                    YesterdaydatalistNew.push(Yesterdaydatalist[i2]);
                                    TodaydatalistNew.push(null);
                                    i2++;
                                } else if ((TimeslotTodayList[i1] == TimeslotYesterdayList[i2]) && (TimeslotYesterdayList[i2] == TimeslotFinalList[i]))
                                {
                                    //to caluculate difference
                                    var GenDataDiff = 0;
                                    if (Yesterdaydatalist[i2] == 0 || Todaydatalist[i1] == 0)
                                    {
                                        GenDataDiff = "-"
                                    } else
                                    {
                                        if (Yesterdaydatalist[i2] > Todaydatalist[i1] && Yesterdaydatalist[i2] > 0)
                                        {
                                            GenDataDiff = Yesterdaydatalist[i2] - Todaydatalist[i1];
                                            GenDataDiff = ((GenDataDiff / Yesterdaydatalist[i2]) * 100) * -1;
                                        } else if (Todaydatalist[i1] > 0)
                                        {
                                            GenDataDiff = Todaydatalist[i1] - Yesterdaydatalist[i2];
                                            GenDataDiff = (GenDataDiff / Todaydatalist[i1]) * 100;
                                        }
                                        GenDataDiff = (Math.round(GenDataDiff * 100) / 100) + "%";
                                    }
                                    //append data to table
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>" + Yesterdaydatalist[i2] + "</td><td style='display: table-cell;'> " + Todaydatalist[i1] + "</td><td style='display: table-cell;'> " + GenDataDiff + " </td></tr>";
                                    TodaydatalistNew.push(Todaydatalist[i1]);
                                    YesterdaydatalistNew.push(Yesterdaydatalist[i2]);
                                    i1++;
                                    i2++;

                                }
                                str.push([TimeslotFinalList[i], [TodaydatalistNew[i], YesterdaydatalistNew[i]]]);
                                var stringTime = timeToSeconds(Date1, TimeslotFinalList[i]);

                                if (TodaydatalistNew[i] == null)
                                {
                                    str1 += stringTime + ',' + YesterdaydatalistNew[i] + "," + '\n';

                                } else if (YesterdaydatalistNew[i] == null)
                                {
                                    str1 += stringTime + ',' + "," + TodaydatalistNew[i] + '\n';
                                } else
                                {
                                    str1 += stringTime + ',' + YesterdaydatalistNew[i] + "," + TodaydatalistNew[i] + '\n';
                                }

                            }

                            function legendFormatter(data) {
                                if (typeof data.xHTML != 'undefined')
                                {
                                    var SplitTime = data.xHTML.split(' ');
                                    var html = "";
                                    data.series.forEach(function (series) {
                                        if (!series.isVisible)
                                            return;
                                        if (typeof series.yHTML != 'undefined')
                                        {
                                            var labeledData = series.labelHTML + ': ' + series.yHTML + " MW";
                                            if (series.isHighlighted) {
                                                labeledData = '<b>' + labeledData + '</b>';
                                            }
                                            html += '<br>' + series.dashHTML + ' ' + labeledData;
                                        }
                                    });
                                    html += " (@Time" + ': ' + SplitTime[1] + ")";
                                    var html1 = html.replace(/\<br>/g, "  ");
                                    $("#Legend").html(html1);
                                } else
                                {
                                    var Fromdate = $("#txt_vpDate").val();
                                    $("#Legend").html("<div class='dygraph-legend-line' style='border-bottom-color: #ffc1b4;'></div> " + Yesterday + "  <div class='dygraph-legend-line' style='border-bottom-color: #64eade;'></div> " + Fromdate + "</div>");
                                }
                            }

                            if (IsDetailsAvailable == 0)
                            {
                                $("#inner_chart_Line1").hide();
                                $(".slimScrollDiv").hide();
                                $("#graph").hide();
                                $("#div_data").hide();
                                $("#DisplayError").show();
                                var Message = '<?php echo NoRecords ?>';
                                $("#ErrorMessage").text(Message);
                                $(".Datatable").css('height', '50%');
                                $("#Legend").hide();
                            } else
                            {
                                $("#Legend").show();
                                var IsChartShow = $('#toggle-chart').prop("checked");
                                //table-false
                                //chart -true
                                if (IsChartShow == true)
                                {
                                    $(".divTable").show();
                                    $("#inner_chart_Line1").hide();


                                } else
                                {
                                    $(".divTable").hide();
                                    $("#inner_chart_Line1").show();


                                }
                                $("#graph").show();
                                $(".slimScrollDiv").show();
                                $("#DisplayError").hide();
                                $(".Datatable").css('height', 'auto');
                            }


                            $("#Frequency_datatable tbody").html(newRowContent);// gridview binding
                            new Dygraph(
                                    document.getElementById("graph"),
                                    str1,
                                    {
                                        customBars: false,
                                        rollPeriod: 0.1,
                                        title: '',
                                        ylabel: 'Frequency (Hz)',
                                        xlabel: 'Time',
                                        legend: 'hide',
                                        showRangeSelector: true,                                       
                                        colors: ['#ffc1b4', '#64eade'],
                                        rangeSelectorPlotFillColor: '#f9dff2',
                                        rangeSelectorPlotStrokeColor: '#f3afe0',
                                        connectSeparatedPoints: true,
                                        labelsSeparateLines: false,
                                        width: 750,
                                        height: 360,
                                        legendFormatter: legendFormatter,
                                        underlayCallback: function (ctx, area) {
                                            var mm = calcMinMax(this),
                                                    ymin = mm[0],
                                                    ymax = mm[1],
                                                    canvasYmin = this.toDomYCoord(ymin),
                                                    canvasYmax = this.toDomYCoord(ymax);
                                            ctx.strokeStyle = '#ffb635';
                                            ctx.lineWidth=0.75;
                                            ctx.beginPath();
                                            ctx.moveTo(area.x, canvasYmin);
                                            ctx.lineTo(area.x + area.w, canvasYmin);
                                            ctx.moveTo(area.x, canvasYmax);
                                            ctx.lineTo(area.x + area.w, canvasYmax);
                                            ctx.closePath();
                                            ctx.stroke();
                                        }
                                    }
                            );
                        }
                        $(".waitMe").hide();
                    },
                    error: function (data)
                    {
                        $("#DisplayError").show();
                        var Message = '<?php echo ERROR_EXPECTION ?>';
                        $("#ErrorMessage").text(Message);
                        $(".waitMe").hide();
                    },
                });
            }
        }

        $(document).ready(function (e)
        {
            $('.footable').footable();//for apply Footable
            // $("table").stickyTableHeaders({scrollableArea: $('#div_data')});
            var Width = $(window).width();
            var Height_ = "100%"
            if (Width >= 768)
            {
                Height_ = "75%";
            }
            $('#div_data').slimScroll({
                size: '5px',
                width: '100%',
                height: Height_,
                color: '#4181a0',
                allowPageScroll: true,
                alwaysVisible: true,
                distance: '0px',
                color: "#000"
            });


        }
        );


        function calcMinMax(g) {
            var ymin = g.getValue(0, 1), ymax = g.getValue(0, 1), v;
            for (var i = 0; i < g.numRows(); i++) {
                for (var j = 1; j < g.numColumns(); j++) {
                    y = g.getValue(i, j);
                    if (y < ymin) {
                        ymin = y;
                    } else if (y > ymax) {
                        ymax = y;
                    }
                }
            }
            return [49.9, 50.05];
        }
    </script>
    <style>
        .zc-data-table {
            display: none !important;
        }
        .Initial_td
        {
            background-color: #e4fffc !important;
            background-image: none !important;
        }
    </style>