<div>
    <script src="<?php echo base_url(); ?>/script/moment.min.js" type="text/javascript"></script>
    <div class="gendata">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" id="CommonPageTitle" value=" <?php echo $GenerationData; ?>">
        <!--        <div class="divToggel" style="text-align:right;">
                    <div class="col-xs-11">
                        <input class="divToggel" data-style="ios" id="toggle-chart" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-table' aria-hidden='true'></i>" data-off="<i class='fa fa-line-chart'></i>">
                    </div>
                    <div class="col-xs-1"></div>
                </div>-->
        <div class="clearfix"></div>
        <?php $this->load->view('PartialView/ErrorViewShow.php'); ?> 
        <div class="col-md-12 Datatable add-overflow-x" style="padding-bottom: 20px;">
            <div class="col-md-8 divChart" id="inner_chart_Line1" >
                <canvas id="dataStuctureChart"></canvas>
            </div>
            <div class="col-md-4 divTable no-padding-horizontal">
                <p style="text-align: center;">Data as on <span id="Date_Generation"></span></p>
                <div class="div_datatable" id="div_data">
                    <table id="generation_datatable" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                        <thead>
                            <tr>
                                <th></th>
                                <th data-type="numeric"><span id="lblYesterdayDate">  <?php echo $Yesterday; ?></span></th>
                                <th data-type="numeric"><span id="lblTodayDate">   <?php echo $Today; ?></span></th>
                                <th data-type="numeric">&#x21c5;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-balance-scale"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>demand-met"><?php echo $Lang["DemandMet"]; ?></a></td>
                                <td><span id="lblDemandMetYesterday"></span></td>
                                <td><span id="lblDemandMetToday"></span></td>
                                <td><span id="lblDemandMetDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-arrows-alt"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>isgs-generation"><?php echo $Lang["ISGS"]; ?></a></td>
                                <td><span id="lblISGSYesterday"></span></td>
                                <td><span id="lblISGSToday"></span></td>
                                <td><span id="lblISGSDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-file-medical-alt"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>thermal-generation"><?php echo $Lang["Thermal"]; ?> </a></td>
                                <td><span id="lblThermalYesterday"></span></td>
                                <td><span id="lblThermalToday"></span></td>
                                <td><span id="lblThermalDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-tint"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>hydro-generation"><?php echo $Lang["Hydro"]; ?> </a></td>
                                <td><span id="lblHydroYesterday"></span></td>
                                <td><span id="lblHydroToday"></span></td>
                                <td><span id="lblHydroDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-fire"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>gas-generation"><?php echo $Lang["Gas"]; ?></a></td>
                                <td><span id="lblGasYesterday"></span></td>
                                <td><span id="lblGasToday"></span></td>
                                <td><span id="lblGasDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-atom"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>nuclear-generation"><?php echo $Lang["Nuclear"]; ?> </a></td>
                                <td><span id="lblNuclearYesterday"></span></td>
                                <td><span id="lblNuclearToday"></span></td>
                                <td><span id="lblNuclearDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-sun"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>solar-generation"><?php echo $Lang["Solar"]; ?> </a></td>
                                <td><span id="lblSolarYesterday"></span></td>
                                <td><span id="lblSolarToday"></span></td>
                                <td><span id="lblSolarDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-asterisk"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>wind-generation"><?php echo $Lang["Wind"]; ?> </a></td>
                                <td><span id="lblWindYesterday"></span></td>
                                <td><span id="lblWindToday"></span></td>
                                <td><span id="lblWindDiff"></span></td>
                            </tr>
                            <tr>
                                <td class="Initial_td" style="text-align: left !important;"><i class="fa fa-bolt"></i>&nbsp;<a style="color:#000;font-size:13px;" href="<?php echo base_url() ?>frequency"><?php echo $Lang["$Frequency"]; ?> (Hz)</a></td>
                                <td><span id="lblFrequencyYesterday"></span></td>
                                <td><span id="lblFrequencyToday"></span></td>
                                <td><span id="lblFrequencyDiff"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>    
            </div>    
        </div>
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.footable').footable();
        });

        function getCommonGenerationData()
        {
            var SelectedDataValue = $("#ddlRegion1").find(':selected').data('value');
            var StateCode = 0;
            var RegionCode = 0;
            if (SelectedDataValue == "State")
            {
                StateCode = $("#ddlRegion1 option:selected").val();
            } else if (SelectedDataValue == "Region")
            {
                RegionCode = $("#ddlRegion1 option:selected").val();
            }
//            var StateCode = $("#ddlState option:selected").val();
//            if (typeof StateCode == 'undefined')
//            {
//                StateCode = $("#SelectedState").val();
//            }
//            var RegionCode = $("#ddlRegion option:selected").val();
//            if (typeof RegionCode == 'undefined')
//            {
//                RegionCode = $("#SelectedRegion").val();
//            }
            var date1 = $("#txt_vpDate").val();
            var date2 = $("#txt_vpDate2").val();


            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
            var d = date1.toString().split(" ");
            var dd = d[0];
            var months = {
                'Jan': '01',
                'Feb': '02',
                'Mar': '03',
                'Apr': '04',
                'May': '05',
                'Jun': '06',
                'Jul': '07',
                'Aug': '08',
                'Sep': '09',
                'Oct': '10',
                'Nov': '11',
                'Dec': '12'
            }
            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;
            var FromDate = yy + "-" + mm + "-" + dd;


            var d = date2.toString().split(" ");
            var dd = d[0];
            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;
            var ToDate = yy + "-" + mm + "-" + dd;
            var FromTime = null;
            var ToTime = null;

            if ((typeof StateCode != 'undefined') && (typeof RegionCode != 'undefined'))
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>getGenerationData",
                    datatype: "JSON",
                    data: {
                        'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>',
                        "RegionCode": RegionCode,
                        "StateCode": StateCode,
                        "FromDate": FromDate,
                        "ToDate": ToDate,
                        "FromTime": FromTime,
                        "ToTime": ToTime
                    },
                    type: "POST",
                    success: function (data) {

                        if (data["0"] == "-1")
                        {
                            $("#div_data").hide();
                            $("#generation_datatable").hide();
                            $("#DisplayError").show();
                            $("#ErrorMessage").text(data["1"]["msg"]);
                            $('#inner_chart_Line1').html("");
                            $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');

                        } else
                        {

                            var genTodayData = data["0"]["result1"]["Result"]["0"]["0"];
                            var genYesterdayData = data["1"]["result2"]["Result"]["0"]["0"];

                            var TodayDate = data["FromDate"];
                            var YesterdayDate = data["PrevFromDate"];
                            var TodaysData = data["0"]["result1"]["Result"]["0"]["0"];
                            var YesterdaysData = data["1"]["result2"]["Result"]["0"]["0"];

                            $("#lblDatedData").text(TodayDate);
                            $("#lblTodayDate").text(TodayDate);
                            $("#lblYesterdayDate").text(YesterdayDate);

                            $("#lblDemandMetToday").text(genTodayData["DemandMet"]);
                            $("#lblISGSToday").text(genTodayData["ISGS"]);
                            $("#lblThermalToday").text(genTodayData["Thermal"]);
                            $("#lblHydroToday").text(genTodayData["Hydro"]);
                            $("#lblGasToday").text(genTodayData["Gas"]);
                            $("#lblNuclearToday").text(genTodayData["Nuclear"]);
                            $("#lblSolarToday").text(genTodayData["Solar"]);
                            $("#lblWindToday").text(genTodayData["Wind"]);
                            $("#lblFrequencyToday").text(genTodayData["Frequency"]);
                            
                            $("#Date_Generation").text(data["SelectedDate"]);
                            
                            $("#lblDemandMetYesterday").text(genYesterdayData["DemandMet"]);
                            $("#lblISGSYesterday").text(genYesterdayData["ISGS"]);
                            $("#lblThermalYesterday").text(genYesterdayData["Thermal"]);
                            $("#lblHydroYesterday").text(genYesterdayData["Hydro"]);
                            $("#lblGasYesterday").text(genYesterdayData["Gas"]);
                            $("#lblNuclearYesterday").text(genYesterdayData["Nuclear"]);
                            $("#lblSolarYesterday").text(genYesterdayData["Solar"]);
                            $("#lblWindYesterday").text(genYesterdayData["Wind"]);
                            $("#lblFrequencyYesterday").text(genYesterdayData["Frequency"]);
                            var Date2 = genYesterdayData["Date_"];
                            SetChartValue(date1, date2, TodaysData, YesterdaysData);
                            CalculateDifference(TodaysData, YesterdaysData);

                        }
                        $(".waitMe").hide();
                    },
                    error: function (data)
                    {
                        $("#DisplayError").show();
                        var Message = '<?php echo ERROR_EXPECTION ?>';
                        $("#ErrorMessage").text(Message);
                        $(".waitMe").hide();
                    },
                })
            }

        }

        function CalculateDifference(genTodayData, genYesterdayData)
        {
            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
            var DemandMetYesterday = genYesterdayData["DemandMet"];
            var DemandMetToday = genTodayData["DemandMet"];
            var DemandMetDiff = 0;

            //Vikiraj 28March2020.. logic as discussed with Harishji
            /*if (DemandMetYesterday == 0 || DemandMetToday == 0)
             {
             DemandMetDiff = "-"
             } else
             {
             if (DemandMetYesterday > DemandMetToday && DemandMetYesterday > 0)
             {
             DemandMetDiff = DemandMetYesterday - DemandMetToday;
             DemandMetDiff = ((DemandMetDiff / DemandMetYesterday) * 100) * -1;
             } else if (DemandMetToday > 0)
             {
             DemandMetDiff = DemandMetToday - DemandMetYesterday;
             DemandMetDiff = (DemandMetDiff / DemandMetToday) * 100;
             }
             DemandMetDiff = (Math.round(DemandMetDiff * 100) / 100) + "%";
             }*/

            if (DemandMetYesterday == 0)
            {
                DemandMetDiff = "-"
            } else
            {
                DemandMetDiff = DemandMetToday - DemandMetYesterday;
                DemandMetDiff = (DemandMetDiff / DemandMetYesterday) * 100

                //Vikiraj 28March2020.. logic as discussed with Harishji;

                DemandMetDiff = (Math.round(DemandMetDiff * 100) / 100) + "%";
            }


            if (DemandMetYesterday == 0)
            {
                GenDataDiff = "-"
            } else
            {
                GenDataDiff = DemandMetToday - DemandMetYesterday;
                GenDataDiff = (GenDataDiff / DemandMetYesterday) * 100;

                GenDataDiff = (Math.round(GenDataDiff * 100) / 100) + "%";
            }

            $("#lblDemandMetDiff").text(DemandMetDiff);


            var ISGSYesterday = genYesterdayData["ISGS"];
            var ISGSToday = genTodayData["ISGS"];
            var ISGSDiff = 0;
            if (ISGSYesterday == 0 || ISGSToday == 0)
            {
                ISGSDiff = "-"
            } else
            {
                if (ISGSYesterday > ISGSToday && ISGSYesterday > 0)
                {
                    ISGSDiff = ISGSYesterday - ISGSToday;
                    ISGSDiff = ((ISGSDiff / ISGSYesterday) * 100) * -1;
                } else if (ISGSToday > 0)
                {
                    ISGSDiff = ISGSToday - ISGSYesterday;
                    ISGSDiff = (ISGSDiff / ISGSToday) * 100;
                }
                ISGSDiff = (Math.round(ISGSDiff * 100) / 100) + "%";
            }
            $("#lblISGSDiff").text(ISGSDiff);


            var ThermalToday = genTodayData["Thermal"];
            var ThermalYesterday = genYesterdayData["Thermal"];
            var ThermalDiff = 0;
            if (ThermalYesterday == 0 || ThermalToday == 0)
            {
                ThermalDiff = "-"
            } else
            {
                if (ThermalYesterday > ThermalToday && ThermalYesterday > 0)
                {
                    ThermalDiff = ThermalYesterday - ThermalToday;
                    ThermalDiff = ((ThermalDiff / ThermalYesterday) * 100) * -1;
                } else if (ISGSToday > 0)
                {
                    ThermalDiff = ThermalToday - ThermalYesterday;
                    ThermalDiff = (ThermalDiff / ThermalToday) * 100;
                }
                ThermalDiff = (Math.round(ThermalDiff * 100) / 100) + "%";
            }
            $("#lblThermalDiff").text(ThermalDiff);


            var HydroToday = genTodayData["Hydro"];
            var HydroYesterday = genYesterdayData["Hydro"];
            var HydroDiff = 0;
            if (HydroYesterday == 0 || HydroToday == 0)
            {
                HydroDiff = "-"
            } else
            {
                if (HydroYesterday > HydroToday && HydroYesterday > 0)
                {
                    HydroDiff = HydroYesterday - HydroToday;
                    HydroDiff = ((HydroDiff / HydroYesterday) * 100) * -1;
                } else if (HydroToday > 0)
                {
                    HydroDiff = HydroToday - HydroYesterday;
                    HydroDiff = (HydroDiff / HydroToday) * 100;
                }
                HydroDiff = (Math.round(HydroDiff * 100) / 100) + "%";
            }
            $("#lblHydroDiff").text(HydroDiff);


            var GasToday = genTodayData["Gas"];
            var GasYesterday = genYesterdayData["Gas"];
            var GasDiff = 0;
            if (GasYesterday == 0 || GasToday == 0)
            {
                GasDiff = "-"
            } else
            {
                if (GasYesterday > GasToday && GasYesterday > 0)
                {
                    GasDiff = GasYesterday - GasToday;
                    GasDiff = ((GasDiff / GasYesterday) * 100) * -1;
                } else if (GasToday > 0)
                {
                    GasDiff = GasToday - GasYesterday;
                    GasDiff = (GasDiff / GasToday) * 100;
                }
                GasDiff = (Math.round(GasDiff * 100) / 100) + "%";
            }
            $("#lblGasDiff").text(GasDiff);

            var NuclearToday = genTodayData["Nuclear"];
            var NuclearYesterday = genYesterdayData["Nuclear"];
            var NuclearDiff = 0;
            if (NuclearYesterday == 0 || NuclearToday == 0)
            {
                NuclearDiff = "-"
            } else
            {
                if (NuclearYesterday > NuclearToday && NuclearYesterday > 0)
                {
                    NuclearDiff = NuclearYesterday - NuclearToday;
                    NuclearDiff = ((NuclearDiff / NuclearYesterday) * 100) * -1;
                } else if (NuclearToday > 0)
                {
                    NuclearDiff = NuclearToday - NuclearYesterday;
                    NuclearDiff = (NuclearDiff / NuclearToday) * 100;
                }
                NuclearDiff = (Math.round(NuclearDiff * 100) / 100) + "%";
            }
            $("#lblNuclearDiff").text(NuclearDiff);



            var SolarToday = genTodayData["Solar"];
            var SolarYesterday = genYesterdayData["Solar"];
            var SolarDiff = 0;
            if (SolarYesterday == 0 || SolarToday == 0)
            {
                SolarDiff = "-"
            } else
            {
                if (SolarYesterday > SolarToday && SolarYesterday > 0)
                {
                    SolarDiff = SolarYesterday - SolarToday;
                    SolarDiff = ((SolarDiff / SolarYesterday) * 100) * -1;
                } else if (SolarToday > 0)
                {
                    SolarDiff = SolarToday - SolarYesterday;
                    SolarDiff = (SolarDiff / SolarToday) * 100;
                }
                SolarDiff = (Math.round(SolarDiff * 100) / 100) + "%";
            }
            $("#lblSolarDiff").text(SolarDiff);


            var WindToday = genTodayData["Wind"];
            var WindYesterday = genYesterdayData["Wind"];
            var WindDiff = 0;
            if (WindYesterday == 0 || WindToday == 0)
            {
                WindDiff = "-"
            } else
            {
                if (WindYesterday > WindToday && WindYesterday > 0)
                {
                    WindDiff = WindYesterday - WindToday;
                    WindDiff = ((WindDiff / WindYesterday) * 100) * -1;
                } else if (WindToday > 0)
                {
                    WindDiff = WindToday - WindYesterday;
                    WindDiff = (WindDiff / WindToday) * 100;
                }
                WindDiff = (Math.round(WindDiff * 100) / 100) + "%";
            }
            $("#lblWindDiff").text(WindDiff);










            var FrequencyToday = genTodayData["Frequency"];
            var FrequencyYesterday = genYesterdayData["Frequency"];
            var FrequencyDiff = 0;
            if (FrequencyYesterday == 0 || FrequencyToday == 0)
            {
                FrequencyDiff = "-"
            } else
            {
                if (FrequencyYesterday > FrequencyToday && FrequencyYesterday > 0)
                {
                    FrequencyDiff = FrequencyYesterday - FrequencyToday;
                    FrequencyDiff = ((FrequencyDiff / FrequencyYesterday) * 100) * -1;
                } else if (FrequencyToday > 0)
                {
                    FrequencyDiff = FrequencyToday - FrequencyYesterday;
                    FrequencyDiff = (FrequencyDiff / FrequencyToday) * 100;
                }
                FrequencyDiff = (Math.round(FrequencyDiff * 100) / 100) + "%";
            }
            $("#lblFrequencyDiff").text(FrequencyDiff);
            $(".waitMe").hide();
        }

        function SetChartValue(TodayDate, YesterdayDate, genTodayData, genYesterdayData)
        {
            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
            var DemandMetToday = genTodayData["DemandMet"];
            var DemandMetYesterday = genYesterdayData["DemandMet"];

            var ISGSToday = genTodayData["ISGS"];
            var ISGSYesterday = genYesterdayData["ISGS"];

            var ThermalToday = genTodayData["Thermal"];
            var ThermalYesterday = genYesterdayData["Thermal"];

            var HydroToday = genTodayData["Hydro"];
            var HydroYesterday = genYesterdayData["Hydro"];

            var GasToday = genTodayData["Gas"];
            var GasYesterday = genYesterdayData["Gas"];

            var NuclearToday = genTodayData["Nuclear"];
            var NuclearYesterday = genYesterdayData["Nuclear"];

            var SolarToday = genTodayData["Solar"];
            var SolarYesterday = genYesterdayData["Solar"];

            var WindToday = genTodayData["Wind"];
            var WindYesterday = genYesterdayData["Wind"];

            var FrequencyToday = genTodayData["Frequency"];
            var FrequencyYesterday = genYesterdayData["Frequency"];


            //set value in chart
            var DataLabel = ['<?php echo $DemandMet; ?> (MW)', '<?php echo $ISGS; ?> (MW)', '<?php echo $Thermal; ?> (MW)', '<?php echo $Hydro; ?> (MW)', '<?php echo $Gas; ?> (MW)', '<?php echo $Nuclear; ?> (MW)', '<?php echo $Solar; ?> (MW)', '<?php echo $Wind; ?> (MW)', '<?php echo $Frequency; ?> (Hz)']
            $('#inner_chart_Line1').html("");
            $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');
            var ctx = $("#dataStuctureChart").get(0).getContext('2d');




            ctx.canvas.height = 420; // setting height of canvas
            ctx.canvas.width = 750; // setting width of canvas

            new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: DataLabel,
                    datasets: [
                        {
                            label: YesterdayDate,
                            backgroundColor: "#ffc1b4",
                            data: [DemandMetYesterday, ISGSYesterday, ThermalYesterday, HydroYesterday, GasYesterday, NuclearYesterday, SolarYesterday, WindYesterday, FrequencyYesterday]

                        },
                        {
                            label: TodayDate,
                            backgroundColor: "#7fede3",
                            data: [DemandMetToday, ISGSToday, ThermalToday, HydroToday, GasToday, NuclearToday, SolarToday, WindToday, FrequencyToday]

                        }
                    ]
                },
                options: {
                    legend: {display: true},
                    responsive: false,
                    maintainAspectRatio: false,
                    fontColor: "black",
                    title: {
                        display: false,
                        text: 'Generation Data'
                    },
                    scales: {
                        xAxes: [{
                                ticks: {
                                    barPercentage: 0.1,
                                    autoSkip: false,
                                    fontColor: "black",
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Type of Generation',
                                    fontColor: "#18c3b3",
                                    fontSize: 14,
                                },
                                maxBarThickness: 70,
                                gridLines: {
                                    display: false
                                },
                            }],
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    fontColor: "black",
                                }
                            }]
                    },
                }
            });
        }

        $(document).ready(function (e)
        {
            $("#divGenerationData").addClass('SelectedView');////to show selected view            



        });

    </script>
    <style type="text/css">
        .div_datatable
        {
            height: 500px;
        }
        .Initial_td
        {
            background-color: #e4fffc !important;
            background-image: none !important;
        }
    </style>