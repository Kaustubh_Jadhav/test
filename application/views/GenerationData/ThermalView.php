<div> <div class="gendata">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" value="<?php echo $Thermal ?>" id="CommonPageTitle">
        <!--        <div class="divToggel" style="text-align:right;">
                    <div class="col-xs-11">
                        <input class="divToggel" data-style="ios" id="toggle-chart" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-table' aria-hidden='true'></i>" data-off="<i class='fa fa-line-chart'></i>">
                    </div>
                    <div class="col-xs-1"></div>
                </div>-->
        <div class="clearfix"></div>
        <?php $this->load->view('PartialView/ErrorViewShow.php'); ?>
        <div class="Datatable">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-8 col-xs-12 legend_section" id="Legend"></div>
                <div class="col-md-4"></div>
            </div>
            <div class="col-md-12 add-overflow-x">
                <div class="col-md-8 divChart" id="inner_chart_Line1">
                    <div id='graph'></div>
                </div>
                <div class="col-md-4 divTable no-padding-horizontal">
                    <div class="div_datatable" id="div_data">
                        <table id="DemandMet_datatable" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th><span>Time</span></th>
                                    <th data-type="numeric"><span id="lblYesterdayDate"><?php echo $Yesterday; ?></span></th>
                                    <th data-type="numeric"><span id="lblTodayDate"><?php echo $Today; ?></span></th>
                                    <th data-type="numeric"><span><?php echo $Diff; ?></span></th>
                                </tr>
                            </thead>
                            <tbody> 
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script src="<?php echo base_url(); ?>script/dygraph.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/dygraph.min.css" />
    <script type="text/javascript">
        function getCommonGenerationData()
        {
            var date1 = $("#txt_vpDate").val();
            var date2 = $("#txt_vpDate2").val();
            var SelectedDataValue = $("#ddlRegion1").find(':selected').data('value');
            var StateCode = 0;
            var RegionCode = 0;
            if (SelectedDataValue == "State")
            {
                StateCode = $("#ddlRegion1 option:selected").val();
            } else if (SelectedDataValue == "Region")
            {
                RegionCode = $("#ddlRegion1 option:selected").val();
            }
            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
            var d = date1.toString().split(" ");
            var dd = d[0];
            var months = {
                'Jan': '01',
                'Feb': '02',
                'Mar': '03',
                'Apr': '04',
                'May': '05',
                'Jun': '06',
                'Jul': '07',
                'Aug': '08',
                'Sep': '09',
                'Oct': '10',
                'Nov': '11',
                'Dec': '12'
            }
            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;
            var SelectedDate1 = yy + "-" + mm + "-" + dd;


            var d = date2.toString().split(" ");
            var dd = d[0];
            var months = {
                'Jan': '01',
                'Feb': '02',
                'Mar': '03',
                'Apr': '04',
                'May': '05',
                'Jun': '06',
                'Jul': '07',
                'Aug': '08',
                'Sep': '09',
                'Oct': '10',
                'Nov': '11',
                'Dec': '12'
            }
            var mm = months[d[1]];
            var yy = d[2];

            if (mm.length < 2)
                mm = '0' + mm;
            if (dd.length < 2)
                dd = '0' + dd;

            var SelectedDate2 = yy + "-" + mm + "-" + dd;

            if ((typeof StateCode != 'undefined') && (typeof RegionCode != 'undefined'))
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>getThermalData",
                    datatype: "JSON",
                    data: {
                        'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>',
                        "StateCode": StateCode,
                        "RegionCode": RegionCode,
                        "SelectedDate1": SelectedDate1,
                        "SelectedDate2": SelectedDate2,
                    },
                    type: "POST",
                    success: function (data) {

                        if (data["0"] == "-1")
                        {

                            $("#div_data").hide();
                            $('#inner_chart_Line1').html("");
                            $('#inner_chart_Line1').html("<div id='dataStuctureChart'></div>");
                            $("#DisplayError").show();
                            $("#ErrorMessage").text(data["1"]["msg"]);


                        } else
                        {
                            $("#div_data").show();
                            $("#div_error").hide();
                            var Today = data["SelectedDate1"];
                            var Yesterday = data["SelectedDate2"];
                            $("#lblTodayDate").text(Today);
                            $("#lblYesterdayDate").text(Yesterday);
                            $("#lblDatedData").text(Today);
                            var DemandTodayData = data["0"]["result1"]["Result"]["0"];
                            var DemandYesterdayData = data["1"]["result2"]["Result"]["0"];
                            var newRowContent = "";
                            var TimeslotTodayList = [];
                            var TimeslotYesterdayList = [];
                            var TimeslotFinalList = [];
                            var Todaydatalist = [];
                            var Yesterdaydatalist = [];
                            var IsDetailsAvailable = 0;

                            var ThermalDataString = "AllIndiaThermalGeneration";
                            if (StateCode != "0")
                            {
                                ThermalDataString = "State_Thermal";
                            } else if (RegionCode != "0")
                            {
                                ThermalDataString = "Generation_Thermal";
                            }
                            var TodaysValue = 0;
                            //create array for today data
                            if (data["0"]["result1"]["Count_"] > 0) {
                                $.each(DemandTodayData, function (key, value) {


                                    TimeslotTodayList.push(value.TimeSlot);
                                    TimeslotFinalList.push(value.TimeSlot);

                                    if (ThermalDataString == "Generation_Thermal")
                                    {
                                        TodaysValue = value.Generation_Thermal;
                                    } else if (ThermalDataString == "State_Thermal")
                                    {
                                        TodaysValue = value.State_Thermal;
                                    } else
                                    {
                                        TodaysValue = value.AllIndiaThermalGeneration;
                                    }
                                    if (TodaysValue > 0)
                                    {
                                        Todaydatalist.push(TodaysValue);
                                    } else
                                    {
                                        Todaydatalist.push('-');
                                    }



                                });
                            }

                            if (data["1"]["result2"]["Count_"] > 0)
                            {
                                $.each(DemandYesterdayData, function (key, value) {
                                    TimeslotYesterdayList.push(value.TimeSlot);
                                    if (ThermalDataString == "Generation_Thermal")
                                    {
                                        Yesterdaydatalist.push(value.Generation_Thermal);

                                    } else if (ThermalDataString == "State_Thermal")
                                    {
                                        Yesterdaydatalist.push(value.State_Thermal);

                                    } else
                                    {
                                        Yesterdaydatalist.push(value.AllIndiaThermalGeneration);
                                    }
                                });
                            }
                            //merge timeslot & remove duplicate
                            $.unique(($.merge(TimeslotFinalList, TimeslotYesterdayList)).sort());
                            TimeslotTodayList.sort();//sorting
                            TimeslotYesterdayList.sort();//sorting



                            var i1 = 0;//index of today
                            var i2 = 0;//index of yesterday
                            var TodaydatalistNew = [];//Create new array for chart
                            var YesterdaydatalistNew = [];//Create new array for chart
                            var str = [];
                            var str1 = "Date," + Yesterday + "," + Today + "\n";
                            //bind gridview/ table 
                            for (var i = 0; i < TimeslotFinalList.length; i++)
                            {

                                if ((Yesterdaydatalist[i2] > 0 || Todaydatalist[i1] > 0) && IsDetailsAvailable != 1)
                                {
                                    IsDetailsAvailable = 1;
                                }
                                if ((TimeslotTodayList[i1] == TimeslotFinalList[i]) && (TimeslotYesterdayList[i2] != TimeslotFinalList[i]))
                                {
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>-</td><td style='display: table-cell;'>" + Todaydatalist[i1]  + "</td><td style='display: table-cell;'> - </td></tr>";
                                    TodaydatalistNew.push(Todaydatalist[i1]);
                                    YesterdaydatalistNew.push(null)
                                    i1++;
                                } else if ((TimeslotTodayList[i1] != TimeslotFinalList[i]) && (TimeslotYesterdayList[i2] == TimeslotFinalList[i]))
                                {
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>" + Yesterdaydatalist[i2]  + "</td><td style='display: table-cell;'>-</td><td style='display: table-cell;'> - </td></tr>";
                                    YesterdaydatalistNew.push(Yesterdaydatalist[i2]);
                                    TodaydatalistNew.push(null);
                                    i2++;
                                } else if ((TimeslotTodayList[i1] == TimeslotYesterdayList[i2]) && (TimeslotYesterdayList[i2] == TimeslotFinalList[i]))
                                {
                                    //to caluculate difference
                                    var GenDataDiff = "-";
                                    if (Yesterdaydatalist[i2] == 0 || Todaydatalist[i1] == 0)
                                    {
                                        GenDataDiff = "-"
                                    } else
                                    {
                                        if (Yesterdaydatalist[i2] > Todaydatalist[i1] && Yesterdaydatalist[i2] > 0)
                                        {
                                            GenDataDiff = Yesterdaydatalist[i2] - Todaydatalist[i1];
                                            GenDataDiff = ((GenDataDiff / Yesterdaydatalist[i2]) * 100) * -1;
                                        } else if (Todaydatalist[i1] > 0)
                                        {
                                            GenDataDiff = Todaydatalist[i1] - Yesterdaydatalist[i2];
                                            GenDataDiff = (GenDataDiff / Todaydatalist[i1]) * 100;
                                        }
                                        GenDataDiff = (Math.round(GenDataDiff * 100) / 100) + "%";
                                    }
                                    //append data to table
                                    newRowContent = newRowContent + "<tr><td class='Initial_td' style='display: table-cell;font-weight: bold'  >" + TimeslotFinalList[i] + "</td><td style='display: table-cell;'>" + Yesterdaydatalist[i2]  + "</td><td style='display: table-cell;'> " + Todaydatalist[i1] + "</td><td style='display: table-cell;'> " + GenDataDiff + " </td></tr>";
                                    TodaydatalistNew.push(Todaydatalist[i1]);
                                    YesterdaydatalistNew.push(Yesterdaydatalist[i2]);
                                    i1++;
                                    i2++;

                                }
                                str.push([TimeslotFinalList[i], [TodaydatalistNew[i], YesterdaydatalistNew[i]]]);
                                var stringTime = timeToSeconds(SelectedDate1, TimeslotFinalList[i]);
                                if (TodaydatalistNew[i] == null)
                                {
                                    str1 += stringTime + ',' + YesterdaydatalistNew[i] + "," + '\n';

                                } else if (YesterdaydatalistNew[i] == null)
                                {
                                    str1 += stringTime + ',' + "," + TodaydatalistNew[i] + '\n';
                                } else
                                {
                                    str1 += stringTime + ',' + YesterdaydatalistNew[i] + "," + TodaydatalistNew[i] + '\n';
                                }

                            }


                            var yaxisMax = 0;
                            if (Math.max.apply(Math, Todaydatalist) > Math.max.apply(Math, Yesterdaydatalist))
                            {
                                yaxisMax = Math.max.apply(Math, Todaydatalist);
                            } else
                            {
                                yaxisMax = Math.max.apply(Math, Yesterdaydatalist);
                            }
                            var yaxisMin = 0;
                            if (Math.min.apply(Math, Todaydatalist) < Math.min.apply(Math, Yesterdaydatalist))
                            {
                                yaxisMin = Math.min.apply(Math, Todaydatalist);
                            } else
                            {
                                yaxisMin = Math.min.apply(Math, Yesterdaydatalist);
                            }
                            yaxisMax = yaxisMax;
                            yaxisMin = yaxisMin;

                            $("#DemandMet_datatable tbody").html(newRowContent);// gridview binding

                            if (IsDetailsAvailable == 0)
                            {
                                $("#inner_chart_Line1").hide();
                                $(".slimScrollDiv").hide();
                                $("#graph").hide();
                                $("#div_data").hide();
                                $("#DisplayError").show();
                                var Message = '<?php echo NoRecords ?>';
                                $("#ErrorMessage").text(Message);
                                $(".Datatable").css('height', '50%');
                                $("#Legend").hide();
                            } else
                            {
                                $("#Legend").show();
                                var IsChartShow = $('#toggle-chart').prop("checked");
                                //table-false
                                //chart -true
                                if (IsChartShow == true)
                                {
                                    $(".divTable").show();
                                    $("#inner_chart_Line1").hide();


                                } else
                                {
                                    $(".divTable").hide();
                                    $("#inner_chart_Line1").show();


                                }
                                $("#graph").show();
                                $(".slimScrollDiv").show();
                                $("#DisplayError").hide();
                                $(".Datatable").css('height', 'auto');
                            }

                            function legendFormatter(data) {
                                if (typeof data.xHTML != 'undefined')
                                {
                                    var SplitTime = data.xHTML.split(' ');
                                    var html = "";
                                    data.series.forEach(function (series) {
                                        if (!series.isVisible)
                                            return;
                                        if (typeof series.yHTML != 'undefined')
                                        {
                                            var labeledData = series.labelHTML + ': ' + series.yHTML + " MW";
                                            if (series.isHighlighted) {
                                                labeledData = '<b>' + labeledData + '</b>';
                                            }
                                            html += '<br>' + series.dashHTML + ' ' + labeledData;
                                        }
                                    });
                                    html += " (@Time" + ': ' + SplitTime[1] + ")";
                                    var html1 = html.replace(/\<br>/g, "  ");
                                    $("#Legend").html(html1);
                                } else
                                {
                                    var Fromdate = $("#txt_vpDate").val();
                                    $("#Legend").html("<div class='dygraph-legend-line' style='border-bottom-color: #ffc1b4;'></div> " + Yesterday + "  <div class='dygraph-legend-line' style='border-bottom-color: #64eade;'></div> " + Fromdate + "</div>");
                                }
                            }


                            new Dygraph(
                                    document.getElementById("graph"),
                                    str1,
                                    {
                                        customBars: false,
                                        rollPeriod: 0.1,
                                        ylabel: 'Thermal Generation (MW)',
                                        xlabel: 'Time',
                                        legend: 'follow',
                                        showRangeSelector: true,
                                        responsive: false,
                                        colors: ['#ffc1b4', '#64eade'],
                                        rangeSelectorPlotFillColor: '#f9dff2',
                                        rangeSelectorPlotStrokeColor: '#f3afe0',
                                        connectSeparatedPoints: true,
                                        labelsSeparateLines: true,
//                                        width: 750,
//                                        height: 420,
                                        width: 750,
                                        height: 360,
                                        legendFormatter: legendFormatter,
                                    }
                            );
                        }
                        $(".waitMe").hide();
                    },
                    error: function (data)
                    {
                        $("#DisplayError").show();
                        var Message = '<?php echo ERROR_EXPECTION ?>';
                        $("#ErrorMessage").text(Message);
                        $(".waitMe").hide();
                    },
                });
            }
        }


        $(document).ready(function (e)
        {
            $('.footable').footable();
            $("#divThermal").addClass('SelectedView');
            var Width = $(window).width();
            var Height_ = "100%"
            if (Width >= 768)
            {
                Height_ = "75%";
            }
            $('#div_data').slimScroll({
                size: '5px',
                width: '100%',
                height: Height_,
                color: '#4181a0',
                allowPageScroll: true,
                alwaysVisible: true,
                distance: '0px',
                color: "#000"
            });
        });

    </script>
    <style type="text/css">

        .Initial_td
        {
            background-color: #e4fffc !important;
            background-image: none !important;
        }

    </style>