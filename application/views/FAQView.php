<script type="text/javascript">
    $(function () {
        $("#accordion").accordion({
            heightStyle: "content"
        });
    });
    $(document).ready(function (e)
    {
       // $("#divFAQ").addClass('SelectedView');////to show selected view

        var WindowWidth = $(window).width();
        if (WindowWidth >= 991)
        {
            $(".sub_header_Inter").show();
        } else
        {
            $(".sub_header_Inter").hide();
        }

    });

    $(window).resize(function () {
        var WindowWidth = $(window).width();
        if (WindowWidth >= 991)
        {
            $(".sub_header_Inter").show();
        } else
        {
            $(".sub_header_Inter").hide();
        }
    });

</script>
<style>
    #accordion {
        font-size: 11px;
    }
</style>
<script src="<?php echo base_url(); ?>script/jquery-ui.min.js" type="text/javascript"></script>
<div class="container-fluid main-section-shadow-box" id="faq_view">
<input type="hidden" id="CommonPageTitle" value=" <?php echo $FAQ; ?>">
<div class="col-md-12 faq_Content">
    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
</div>
<div class="clearfix"></div>
<div id="accordion" class="faqAccordianContent_en">
    <h3>1. Sample Question No 1?</h3>
    <div>
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium
        <ol>
            <li>1. Consectetur</li>
            <li>2. Adipiscing</li>
            <li>3. Incididunt</li>
        </ol>
    </div>

    <h3>2. Sample Question No 2?</h3>
    <div>
        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.:
        <ol>
            <li>1. Ut enim ad minima veniam</li>
            <li>2. Quis nostrum exercitationem ullam corporis .</li>
            <li>3. Quae ab illo inventore veritatis et quasi architecto beatae.</li>
        </ol>
    </div>

    <h3>3. Sample Question No 3?</h3>
    <div>
        de Finibus Bonorum et Malorum:
        <ol>
            <li>1. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum</li>

            <li>2. Temporibus autem quibusdam et aut officiis debitis aut rerum</li>
            <li>3. Nam libero tempore, cum soluta nobis est </li>
        </ol>
    </div>
</div>
<div class="col-md-12" style="height: 50px;"></div>
</div>