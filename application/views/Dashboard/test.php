    function LoadTableForRepeater()
    {

        var date1 = $("#FromDate").val();
        $.ajax({
            url: "<?php echo base_url(); ?>getGenerationDataForTable",
            datatype: "JSON",
            data: {
                'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>',
            },
            type: "POST",
            success: function (data) {
                console.log(data);
                if (data["0"] == "-1")
                {
                    $("#div_data").hide();
                    $("#generation_div").hide();
                    $("#DisplayError").show();
                    $("#ErrorMessage").text(data["1"]["msg"]);
                    $('#inner_chart_Line1').html("");
                    $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');

                } else
                {

                    var genTodayData = data["0"]["result1"]["Result"]["0"];
                    $("#Demand_div").html("");
                    $("#ISGS_div").html("");
                    $("#Tharmal_div").html("");
                    $("#Hydro_div").html("");
                    $("#Gas_div").html("");
                    $("#Nuclear_div").html("");
                    $("#Solar_div").html("");
                    $("#Wind_div").html("");
                    $("#Frequency_div").html("");

                    var Url = '<?php echo base_url() ?>';

                    $("#Demand_div").html("<td style='text-align: left !important'><i class='fa fa-balance-scale'></i>&nbsp;<a class='HeadingData' href=" + Url + "demand-met>Demand Met</a></td>");
                    $("#ISGS_div").html("<td style='text-align: left !important'><i class='fa fa-arrows-alt'></i>&nbsp;<a class='HeadingData' href=" + Url + "isgs-generation>ISGS Generation</a></td>");
                    $("#Tharmal_div").html("<td style='text-align: left !important'><i class='fa fa-file-medical-alt'></i>&nbsp;<a class='HeadingData' href=" + Url + "thermal-generation>Thermal Generation</a></td>");
                    $("#Hydro_div").html("<td style='text-align: left !important'><i class='fa fa-tint'></i>&nbsp;<a class='HeadingData' href=" + Url + "hydro-generation>Hydro Generation</a></td>");
                    $("#Gas_div").html("<td style='text-align: left !important'><i class='fa fa-fire'></i>&nbsp;<a class='HeadingData' href=" + Url + "gas-generation>Gas Generation</a></td>");
                    $("#Nuclear_div").html("<td style='text-align: left !important'><i class='fa fa-atom'></i>&nbsp;<a class='HeadingData' href=" + Url + "nuclear-generation>Nuclear Generation</a></td>");
                    $("#Solar_div").html("<td style='text-align: left !important'><i class='fa fa-sun'></i>&nbsp;<a class='HeadingData' href=" + Url + "solar-generation>Solar Generation</a></td>");
                    $("#Wind_div").html("<td style='text-align: left !important'><i class='fa fa-asterisk'></i>&nbsp;<a class='HeadingData' href=" + Url + "wind-generation>Wind Generation</a></td>");
                    $("#Frequency_div").html("<td style='text-align: left !important' class='HeadingData1'><i class='fa fa-bolt'></i>&nbsp;<span style='text-decoration:none;'>Frequency</span></td>");

                    $.each(genTodayData, function (key, value) {

                        $("#Demand_div").append("<td>" + (value.DemandMet > 0 ? value.DemandMet : "-") + "</td>");
                        $("#ISGS_div").append("<td>" + (value.ISGS > 0 ? value.ISGS : "-") + "</td>");
                        $("#Tharmal_div").append("<td>" + (value.Thermal > 0 ? value.Thermal : "-") + "</td>");
                        $("#Hydro_div").append("<td>" + (value.Hydro > 0 ? value.Hydro : "-") + "</td>");
                        $("#Gas_div").append("<td>" + (value.Gas > 0 ? value.Gas : "-") + "</td>");
                        $("#Nuclear_div").append("<td>" + (value.Nuclear > 0 ? value.Nuclear : "-") + "</td>");
                        $("#Solar_div").append("<td>" + (value.Solar > 0 ? value.Solar : "-") + "</td>");
                        $("#Wind_div").append("<td>" + (value.Wind > 0 ? value.Wind : "-") + "</td>");
                        $("#Frequency_div").append("<td>" + (value.Frequency > 0 ? value.Frequency : "-") + "</td>");


                        if (value.Type_ == "NR")
                        {
                            $("#NR_Demand_div").html(value.DemandMet > 0 ? value.DemandMet : "-");
                            $("#NR_ISGS_div").html(value.ISGS > 0 ? value.ISGS : "-");
                            $("#NR_Thermal_div").html(value.Thermal > 0 ? value.Thermal : "-");
                            $("#NR_Hydro_div").html(value.Hydro > 0 ? value.Hydro : "-");
                            $("#NR_Gas_div").html(value.Gas > 0 ? value.Gas : "-");
                            $("#NR_Nuclear_div").html(value.Nuclear > 0 ? value.Nuclear : "-");
                            $("#NR_Wind_div").html(value.Wind > 0 ? value.Wind : "-");
                            $("#NR_Solar_div").html(value.Solar > 0 ? value.Solar : "-");

                        } else if (value.Type_ == "SR")
                        {
                            $("#SR_Demand_div").html(value.DemandMet > 0 ? value.DemandMet : "-");
                            $("#SR_ISGS_div").html(value.ISGS > 0 ? value.ISGS : "-");
                            $("#SR_Thermal_div").html(value.Thermal > 0 ? value.Thermal : "-");
                            $("#SR_Hydro_div").html(value.Hydro > 0 ? value.Hydro : "-");
                            $("#SR_Gas_div").html(value.Gas > 0 ? value.Gas : "-");
                            $("#SR_Nuclear_div").html(value.Nuclear > 0 ? value.Nuclear : "-");
                            $("#SR_Wind_div").html(value.Wind > 0 ? value.Wind : "-");
                            $("#SR_Solar_div").html(value.Solar > 0 ? value.Solar : "-");

                        } else if (value.Type_ == "ER")
                        {
                            $("#ER_Demand_div").html(value.DemandMet > 0 ? value.DemandMet : "-");
                            $("#ER_ISGS_div").html(value.ISGS > 0 ? value.ISGS : "-");
                            $("#ER_Thermal_div").html(value.Thermal > 0 ? value.Thermal : "-");
                            $("#ER_Hydro_div").html(value.Hydro > 0 ? value.Hydro : "-");
                            $("#ER_Gas_div").html(value.Gas > 0 ? value.Gas : "-");
                            $("#ER_Nuclear_div").html(value.Nuclear > 0 ? value.Nuclear : "-");
                            $("#ER_Wind_div").html(value.Wind > 0 ? value.Wind : "-");
                            $("#ER_Solar_div").html(value.Solar > 0 ? value.Solar : "-");

                        } else if (value.Type_ == "WR")
                        {
                            $("#WR_Demand_div").html(value.DemandMet > 0 ? value.DemandMet : "-");
                            $("#WR_ISGS_div").html(value.ISGS > 0 ? value.ISGS : "-");
                            $("#WR_Thermal_div").html(value.Thermal > 0 ? value.Thermal : "-");
                            $("#WR_Hydro_div").html(value.Hydro > 0 ? value.Hydro : "-");
                            $("#WR_Gas_div").html(value.Gas > 0 ? value.Gas : "-");
                            $("#WR_Nuclear_div").html(value.Nuclear > 0 ? value.Nuclear : "-");
                            $("#WR_Wind_div").html(value.Wind > 0 ? value.Wind : "-");
                            $("#WR_Solar_div").html(value.Solar > 0 ? value.Solar : "-");

                        } else if (value.Type_ == "NER")
                        {
                            $("#NER_Demand_div").html(value.DemandMet > 0 ? value.DemandMet : "-");
                            $("#NER_ISGS_div").html(value.ISGS > 0 ? value.ISGS : "-");
                            $("#NER_Thermal_div").html(value.Thermal > 0 ? value.Thermal : "-");
                            $("#NER_Hydro_div").html(value.Hydro > 0 ? value.Hydro : "-");
                            $("#NER_Gas_div").html(value.Gas > 0 ? value.Gas : "-");
                            $("#NER_Nuclear_div").html(value.Nuclear > 0 ? value.Nuclear : "-");
                            $("#NER_Wind_div").html(value.Wind > 0 ? value.Wind : "-");
                            $("#NER_Solar_div").html(value.Solar > 0 ? value.Solar : "-");
                        }

                    });


                }
                $(".waitMe").hide();
            },
            error: function (data)
            {
                $("#DisplayError").show();
                var Message = '<?php echo ERROR_EXPECTION ?>';
                $("#ErrorMessage").text(Message);
                $(".waitMe").hide();
            },
        })

    }

    
    
   
                        <tr id="Demand"></tr>
                        <tr id="ISGS"></tr>
                        <tr id="Tharmal"></tr>
                        <tr id="Hydro"></tr>
                        <tr id="Gas"></tr>
                        <tr id="Nuclear"></tr>
                        <tr id="Solar"></tr>
                        <tr id="Wind"></tr>
                        <tr id="Frequency"></tr>
        <table id="tbl_demand">
            <tr>
                <td colspan="5" id="demand_heading"><a class='HeadingData' href=" + Url + "demand-met>Demand Met</a></td>
                <td id="demand_total"></td>
            </tr>
            <tr>
                <th>ER</th>
                <th>NER</th>
                <th>NR</th>
                <th>SR</th>
                <th>WR</th>
            </tr>
            <tr>
                <td id="demand_ER"></td>
                <td id="demand_NER"></td>
                <td id="demand_NR"></td>
                <td id="demand_SR"></td>
                <td id="demand_WR"></td>
                <td rowspan="2" id="demand_icon"></td>
            </tr>
        </table>
                        
        <table id="tbl_isgs">
            <tr>
                <td colspan="5" id="isgs_heading"><a class='HeadingData' href=" + Url + "isgs>Demand Met</a></td>
                <td id="isgs_total"></td>
            </tr>
            <tr>
                <th>ER</th>
                <th>NER</th>
                <th>NR</th>
                <th>SR</th>
                <th>WR</th>
            </tr>
            <tr>
                <td id="isgs_ER"></td>
                <td id="isgs_NER"></td>
                <td id="isgs_NR"></td>
                <td id="isgs_SR"></td>
                <td id="isgs_WR"></td>
                <td rowspan="2" id="isgs_icon"></td>
            </tr>
        </table>
    
                <table id="generation_datatable_1" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                    <thead>
                        <tr>
                            <th></th>
                            <th data-type="numeric" style="text-align: center;">ER</th>
                            <th data-type="numeric" style="text-align: center;">NER</th>
                            <th data-type="numeric" style="text-align: center;">NR</th>
                            <th data-type="numeric" style="text-align: center;">SR</th>
                            <th data-type="numeric" style="text-align: center;">WR</th>
                            <th data-type="numeric" style="text-align: center;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="Demand_1"></tr>
                        <tr id="ISGS_1"></tr>
                        <tr id="Tharmal_1"></tr>
                        <tr id="Hydro_1"></tr>
                        <tr id="Gas_1"></tr>
                        <tr id="Nuclear_1"></tr>
                        <tr id="Solar_1"></tr>
                        <tr id="Wind_1"></tr>
                        <tr id="Frequency_1"></tr>
                    </tbody>
                </table>
                        
                        
                        <div id="div_demand col-md-4 col-xs-12">
                            <div class="col-md-12 col-xs-12">
                                <div class="col-md-10 col-xs-10">
                                    <h4><a href="">Demand Met</a></h4>
                                </div>
                                <div class="col-md-2 col-xs-2"></div>
                            </div>
                        </div>