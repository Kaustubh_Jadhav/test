<div class="col-md-12 Menu_Head">
    <span id="CloseMenu"><i class="fa fa-times close_btn" aria-hidden="true"></i></span>
    <p style="margin: 2px;"><img style="height: 65px;" src="<?php echo base_url(); ?>/images/ministry-ofpower_Mobile.png" alt="Ministry Of Power" title="Ministry Of Power"></p>
    <p class="heading-mobileview-menu" style="margin: 0px;">GENERATION APP</p>
</div>
<div class="clearfix"></div>
<div class="mobile-side-menu-wrapper">
<div class="col-md-12 MobileMenuSection">
    <a onclick="OpenMenu();" class="SlideMenu_link">
        <div class="Option_sec">
            <i class="fas fa-home"></i> Dashboard   
        </div></a>
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>interregional-transfer'">
        <div class="Option_sec" style="display: flex">
            <div><i class="fa fa-globe"></i></div>&nbsp;<div style="margin-top: -3px;"> <?php echo $Lang["InterRegionalTransmission"]; ?></div>
        </div>
    </a> 
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>generation-data'">
        <div class="Option_sec">
            <i class="fa fa-bolt" aria-hidden="true"></i>&nbsp;<?php echo $Lang["GenerationData"]; ?>      
        </div>
    </a> 
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>demand-met'">
        <div class="Option_sec">
            <i class="fa fa-balance-scale" style="margin-left: -6px;"></i>&nbsp;<?php echo $Lang["DemandMet"]; ?>   
        </div>
    </a>  
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>isgs-generation'">
        <div class="Option_sec">
            <i class="fa fa-arrows-alt"></i>&nbsp;<?php echo $Lang["ISGS"]; ?>      
        </div>
    </a> 
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>thermal-generation'">
        <div class="Option_sec">
            <i class="fa fa-file-medical-alt"></i>&nbsp;<?php echo $Lang["Thermal"]; ?>  
        </div>
    </a>     
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>hydro-generation'">
        <div class="Option_sec">
            <i class="fa fa-tint"></i>&nbsp;<?php echo $Lang["Hydro"]; ?>    
        </div>
    </a>   
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>gas-generation'">
        <div class="Option_sec">
            <i class="fa fa-fire"></i>&nbsp;<?php echo $Lang["Gas"]; ?>      
        </div>
    </a> 
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>nuclear-generation'">
        <div class="Option_sec">
            <i class="fa fa-atom"></i>&nbsp;<?php echo $Lang["Nuclear"]; ?>     
        </div>
    </a>  
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>solar-generation'">
        <div class="Option_sec">
            <i class="fa fa-sun"></i>&nbsp;<?php echo $Lang["Solar"]; ?>     
        </div>
    </a>  
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>wind-generation'">
        <div class="Option_sec">
            <i class="fa fa-asterisk"></i>&nbsp;<?php echo $Lang["Wind"]; ?>      
        </div>
    </a> 
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>national-summary'">
        <div class="Option_sec">
            <i class="fa fa-copy"></i>&nbsp;<?php echo $Lang["NationalSummary"]; ?>&nbsp;/&nbsp;RTD
        </div>
    </a>
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>inter-regional-exchange'">
        <div class="Option_sec" style="display: flex;">
            <div><i class="fas fa-exchange-alt"></i> </div>&nbsp; <div style="margin-top: -3px;"><?php echo $Lang["InterRegionalExchange"]; ?> </div>    
        </div>
    </a>  
<!--    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>ATC-monitoring'">
        <div class="Option_sec">
            <i class="fa fa-eye"></i> <?php echo $Lang["ATCMonitoring"]; ?>     
        </div>
    </a>  -->
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>important-line-ER'">
        <div class="Option_sec" style="display: flex;">
            <div><i class="fa fa-equals"></i></div>&nbsp; <div style="margin-top: -3px;"> <?php echo $Lang["ImpLineOfER"]; ?> </div>   
        </div>
    </a>   
    <a class="SlideMenu_link" onclick="window.location.href = '<?php echo base_url() ?>faq'">
        <div class="Option_sec" style="display: flex;">
            <div><i class="fas fa-question-circle"></i></div>&nbsp;<div style="margin-top: -3px;"><?php echo $Lang["FAQ"]; ?></div>      
        </div>
    </a> 
</div>
</div>



<script type="text/javascript">

$(document).ready(function(e)
{
    
    $("#CloseMenu").click(function(e)
    {
         $('#SlideMenu').hide('slide', {direction: 'left'}, 700);
         $("body").css('overflow','auto');
        
    });
    
});



</script>