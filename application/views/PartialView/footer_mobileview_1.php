<div id="myFooter" class="container-fluid mobile-footer-menu" id="footer-mobile-menu">    
        <div class="col-md-12 col-xs-12 text-center footer-mobile-menu-box">
            <div class="col-md-3 col-xs-3">
                <a href="<?php echo base_url() ?>faq">
                    <img src="<?php echo base_url(); ?>/images/faq_icon.png" alt="FAQ" title="FAQ">
                    <br>FAQ
                </a>
            </div>
            <div class="col-md-3 col-xs-3">
                
                                    <a class="LanguageTranslateText_en_rtd hindi_tran" onclick="TranslatePage()"><?php echo $Lang["Translation_details"] ?></a>
                <a onclick="TranslatePage()">                    
                    <img src="<?php echo base_url(); ?>/images/<?php echo $Lang["MobileFooterMenuImage"] ?>" alt="<?php echo $Lang["Translation_details"] ?>" title="<?php echo $Lang["Translation_details"] ?>">                   
                    <br><?php echo $Lang["Translation_details_New"] ?>
                </a>
            </div>
            <div class="col-md-3 col-xs-3">
                <!--<a href="https://wa.me/?text=urlencodedtext">Share this</a>-->
                <a href="https://wa.me/?text=urlencodedtext">
                    <img src="<?php echo base_url(); ?>/images/share_icon.png" alt="Share" title="Share">
                    <br>SHARE
                </a>
            </div>
            <div class="col-md-3 col-xs-3">
                <a>
                <img src="<?php echo base_url(); ?>/images/help_icon.png" alt="Help" title="Help">
                <br>HELP
                </a>
            </div>
        </div>
</div>
<script type="text/javascript">      
    $(window).on("scroll", function () {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {            
            $(".mobile-footer-menu").css("padding-bottom","10px");
        }
        else{
            $(".mobile-footer-menu").css("padding-bottom","60px");  
        }
    });
</script>


<!--<script type="text/javascript">
    $(window).on("scroll", function () {
        var footer = document.getElementById("myFooter");
        var sticky = footer.offsetTop;
        
        if (window.pageYOffset > sticky) {
            footer.classList.add("sticky");
        } else {
           footer.classList.remove("sticky");
        }
    });
</script>

 <style type="text/css">
    .sticky{
        position: fixed;
        z-index: 9999;
        width: 100%;
    }   
</style>-->