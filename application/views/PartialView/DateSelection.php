<!--<div class="col-md-12 sub_header">   
    <span class="Generation_Name">
<?php echo $Lang["Title"] . $Lang["HeaderDate1"]; ?>&nbsp;
    </span>
    <span class="control_section">
        <span id="SelectedDate" class="option_div"></span>
        <span class="option_div AllIndia_div"><span id="AllIndia" style="font-size: 14px;">All India</span></span>
        <span class="option_div region_div"><span id="RegionCode" style="font-size: 14px;"></span><i class='fa fa-times close_icon' aria-hidden='true' onclick="ClearRegion()"></i></span>
        <span class="option_div state_div"><span id="StateCode" style="font-size: 14px;"></span><i class='fa fa-times close_icon' aria-hidden='true' onclick="ClearState()"></i></span>
        <i class="fa fa-filter filter_icon" onclick="OpenFilter()"></i>  
        <input class="divToggel" data-style="ios" id="toggle-chart" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-table' aria-hidden='true'></i>" data-off="<i class='fa fa-line-chart'></i>">                   
    </span>  
</div>-->

<div class="col-md-12 sub_header">       
    <div class="col-md-12 no-padding-horizontal">
        <div class="col-md-6 col-xs-12" style="text-align: right;">            
            <span class="Generation_Name padding-top-25">
                <?php echo $Lang["Title"] . $Lang["HeaderDate1"]; ?>&nbsp;
            </span>
        </div>
        <div class="control_section col-md-6 col-xs-12 no-padding-horizontal">
            <span id="SelectedDate" class="option_div col-md-2 col-xs-4 no-padding-horizontal"></span>
            <span class="option_div AllIndia_div col-md-2 col-xs-4 no-padding-horizontal"><span id="AllIndia" style="font-size: 14px;">All India</span></span>
            <span class="option_div region_div"><span id="RegionCode" style="font-size: 14px;"></span><i class='fa fa-times close_icon' aria-hidden='true' onclick="ClearRegion()"></i></span>
            <span class="option_div state_div"><span id="StateCode" style="font-size: 14px;"></span><i class='fa fa-times close_icon' aria-hidden='true' onclick="ClearState()"></i></span>
            <i class="fa fa-filter filter_icon col-md-1 col-xs-1 no-padding-horizontal" onclick="OpenFilter()" style="float: none;"></i>  
            <div class="col-xs-2 no-padding-horizontal float-right chart-table-toggle">
                <input class="divToggel" data-style="ios" id="toggle-chart" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-table' aria-hidden='true'></i>" data-off="<i class='fa fa-line-chart'></i>">                   
            </div>
        </div>  
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">

    var OldDate = null;
    //Common function to bind all state dropdown
//    function BindState(Id)
//    {
//        //1: On region change
//        //2: While model pop up open
//        //3; Clear State code 
//        var IsStateBind = 1;
//        if (Id == 1)
//        {
//            IsStateBind = 1;
//        } else if (Id == 2)
//        {
//            var IsValue = $('#ddlState option:selected').val();
//            if (typeof IsValue == "undefined")
//            {
//                IsStateBind = 1;
//            } else
//            {
//                IsStateBind = 0;
//            }
//        } else if (Id == 3)
//        {
//            var IsValue = $('#ddlState option:selected').val();
//            if (typeof IsValue == "undefined")
//            {
//                $("#SelectedState").val(0);
//                IsStateBind = 1;
//
//            } else
//            {
//                $('#ddlState').val(0);
//                IsStateBind = 0;
//                $("#SelectedState").val(0);
//            }
//        }
//
//        if (IsStateBind == 1)
//        {
//            $("#wrapper").waitMe({effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: ''});
//            $("#myModal").addClass("ApplyZIndex");
//            var Region = $('#ddlRegion option:selected').val();
//            $.ajax({
//                url: "<?php echo base_url(); ?>FillDdlState",
//                datatype: "JSON",
//                data: {'csrf_token_name': '<?php echo $this->security->get_csrf_hash() ?>', 'RegionCode': Region, },
//                type: "post",
//                success: function (data) {
//                    if (data["0"] == "-1")
//                    {
//                        $("#div_data").hide();
//                        $("#div_data1").hide();
//                        $("#div_error").show();
//                        $('#inner_chart_Line1').html("");
//                        $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');
//                        $("#lbl_error").text(data["1"]["msg"]);
//                    } else
//                    {
//                        var data1 = data["result"];
//                        var data2 = data1["Result"];
//                        var data3 = data2["0"];
//                        $('#ddlState option').remove();
//                        $("#ddlState").append($("<option></option>").val("0").html("-- All Regions --"));
//                        $.each(data3, function (key, value) {
//
//                            $("#ddlState").append($("<option></option>").val(value.StateCode).html(value.StateName));
//                        });
//                        $(".waitMe").hide()
//                        $("#myModal").removeClass("ApplyZIndex");
//                        var SelectedState = $("#SelectedState").val();
//                        $("#ddlState").val(SelectedState);
//                    }
//                },
//                error: function (data)
//                {
//                    $("#div_data").hide();
//                    $("#div_data1").hide();
//                    $('#inner_chart_Line1').html("");
//                    $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');
//                    $("#DisplayError").show();
//                    var Message = '<?php echo ERROR_BIND_STATE ?>';
//                    $("#ErrorMessage").text(Message);
//                    $(".waitMe").hide();
//                    $("#myModal").removeClass("ApplyZIndex");
//                },
//            });
//        }
//    }

    //Common function to bind all region dropdown
//    function BindRegionData()
//    {
//        $.ajax({
//            url: "<?php echo base_url(); ?>FillDdlRegion",
//            datatype: "JSON",
//            data: {'csrf_token_name': $('input[name="csrf_token_name"]').val()},
//            type: "post",
//            success: function (data) {
//
//                if (data["0"] == "-1")
//                {
//                    $("#div_data").hide();
//                    $("#div_data1").hide();
//                    $("#generation_datatable").hide();
//                    $("#div_error").show();
//                    $('#inner_chart_Line1').html("");
//                    $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');
//                    $("#lbl_error").text(data["1"]["msg"]);
//                } else
//                {
//                    var data1 = data["result"];
//                    var data2 = data1["Result"]
//                    var data3 = data2["0"];
//                    $('#ddlRegion option').remove();
//                    $("#ddlRegion").append($("<option></option>").val("0").html("-- All India --"));
//                    var SelectedRegion = $("#SelectedRegion").val();
//                    $.each(data3, function (key, value) {
//                        if (value.RegionCode == SelectedRegion)
//                        {
//                            $("#ddlRegion").append($("<option selected='selected'></option>").val(value.RegionCode).html(value.RegionName));
//                        } else
//                        {
//                            $("#ddlRegion").append($("<option></option>").val(value.RegionCode).html(value.RegionName));
//                        }
//
//                    });
//                }
//
//            },
//            error: function (data)
//            {
//                $("#div_data").hide();
//                $("#div_data1").hide();
//                $("#generation_datatable").hide();
//                $('#inner_chart_Line1').html("");
//                $('#inner_chart_Line1').html('<canvas id="dataStuctureChart"><canvas>');
//                $("#DisplayError").show();
//                var Message = '<?php echo ERROR_BIND_REGION ?>';
//                $("#ErrorMessage").text(Message);
//                $(".waitMe").hide();
//            },
//        });
//    }


    $(document).ready(function (e)
    {

        //On page load bind dropdown and load all data by common method    
        // BindRegionData();
        var startDate = new Date($("#SelectedDate1").val());
        //startDate.setDate(startDate.getDate() - 1); //set yesterday date
        $("#txt_vpDate").datepicker({
            format: "dd M yyyy",
            autoclose: true,
            setEndDate: startDate,
        }).datepicker("setDate", startDate); //.datepicker('setDate', startDate); 
        $("#txt_vpDate").datepicker('setEndDate', new Date());

        var startDate1 = new Date($("#SelectedDate2").val());
        $("#txt_vpDate2").datepicker({
            format: "dd M yyyy",
            autoclose: true,
            setEndDate: startDate1,
        }).datepicker("setDate", startDate1); //.datepicker('setDate', startDate); 
        $("#txt_vpDate2").datepicker('setEndDate', new Date());
        var IsOther = $("#IsOther").val();
        var SelectedState = $("#SelectedState").val();



        $("#btn_apply").click(function (e)
        {
            //var date1 = new Date($("#txt_vpDate").val());
            //var date2 = new Date($("#txt_vpDate2").val());
            var date1 = $("#txt_vpDate").val();
            var date2 = $("#txt_vpDate2").val();
            if (date1 != date2)
            {
                $.when(getCommonGenerationData()).then(function () {
                    ChangeOption(1);
                    var SelectedDate = $("#txt_vpDateMobile").val();
                    $("#SelectedDate").text(SelectedDate);
                    //$(".control_section").toggle(700);
                    //$("#option_section").toggle(700);
//                var IsChartShow = $('#toggle-chart').prop("checked");
//                //table-false
//                //chart -true
//                if (IsChartShow == true)
//                {
//                    $(".divTable").show();
//                    $("#inner_chart_Line1").hide();
//                    alert('Chart Hide');
//
//
//                } else
//                {
//                    $(".divTable").hide();
//                    $("#inner_chart_Line1").show();
//                    alert('Chart show');
//
//                }

                });
            } else
            {
                var Message = '<?php echo DateValidation ?>';
                $("#DateError").text(Message);
                $("#DateErrorSection").show();
                $("#DateErrorSection").fadeOut(10000);
                return false;

            }
        });
        if (IsOther == 1)
        {
            ChangeOption(0);
        } else
        {
            if (IsOther == 3 && (SelectedState == "AP" || SelectedState == "GJT" || SelectedState == "KRT" || SelectedState == "MHA" || SelectedState == "MPD" || SelectedState == "RJ" || SelectedState == "TND"))
            {
                ChangeOption(0);

            } else if (IsOther == 2 && (SelectedState == "AP" || SelectedState == "CTG" || SelectedState == "GJT" || SelectedState == "KRT" || SelectedState == "MHA" || SelectedState == "MPD" || SelectedState == "PNB" || SelectedState == "RJ" || SelectedState == "TLG" || SelectedState == "TND" || SelectedState=="UP"))
            {
                ChangeOption(0);
            } else
            {
                $(".AllIndia_div").show();
                $(".region_div").hide();
                $(".state_div").hide();
                var SelectedDate = $("#txt_vpDate").val();
                $("#SelectedDate").text(SelectedDate);
            }
        }

        //BindState(0); 
        getCommonGenerationData();
    });
    function OpenFilter()
    {
        // $.when(BindState(2)).then(function () {

        $('#myModal').appendTo("body").modal('show');
        //});
    }

    function ChangeOption(Id)
    {

        var SelectedDataValue = $("#ddlRegion1").find(':selected').data('value');
        var StateCode = 0;
        var RegionCode = 0;
        var Type = $("#CommonPageTitle").val();
        if (SelectedDataValue == "State" && Type != 'Frequency')
        {
            StateCode = $("#ddlRegion1 option:selected").val();
            RegionCode = 'All Region';
            $(".region_div").hide();
            $("#RegionCode").text(RegionCode);
            $("#StateCode").text(StateCode);
            $(".state_div").show();
            $(".AllIndia_div").hide();

        } else if (SelectedDataValue == "Region" && Type != 'Frequency')
        {
            RegionCode = $("#ddlRegion1 option:selected").val();
            $(".region_div").show();
            $("#RegionCode").text(RegionCode);
            StateCode = "All State";
            $("#StateCode").text(StateCode);
            $(".state_div").hide();
            $(".AllIndia_div").hide();

        } else
        {
            var HfdSelectedRegion = $("#SelectedRegion").val();
            var HdfSelectedState = $("#SelectedState").val();
            if (Id == 0 && Type != 'Frequency')
            {
                if (HdfSelectedState != "0")
                {
                    $(".region_div").hide();
                    $("#RegionCode").text(HfdSelectedRegion);
                    $("#StateCode").text(HdfSelectedState);
                    $("#ddlRegion1").val(HdfSelectedState);
                    $(".state_div").show();
                    $(".AllIndia_div").hide();
                } else if (HfdSelectedRegion != "0")
                {
                    $(".region_div").show();
                    $("#RegionCode").text(HfdSelectedRegion);
                    StateCode = "All State";
                    $("#StateCode").text(StateCode);
                    $("#ddlRegion1").val(HfdSelectedRegion);
                    $(".state_div").hide();
                    $(".AllIndia_div").hide();
                } else
                {
                    $(".AllIndia_div").show();
                    $(".region_div").hide();
                    $(".state_div").hide();
                }

            } else
            {
                $(".AllIndia_div").show();
                $(".region_div").hide();
                $(".state_div").hide();
            }

        }
        var SelectedDate = $("#txt_vpDate").val();
        $("#SelectedDate").text(SelectedDate);





//        if (typeof RegionCode == "undefined")
//        {
//            StateCode = $("#SelectedState").val();
//            RegionCode = $("#SelectedRegion").val();
//        }
//        if (StateCode == "0" && RegionCode == "0")
//        {
//            $(".region_div").hide();
//            $(".state_div").hide();
//        } else
//        {
//            if (RegionCode != "0" && StateCode != "0")
//            {
//                $(".region_div").hide();
//                $("#RegionCode").text(RegionCode);
//                $("#StateCode").text(StateCode);
//                $(".state_div").show();
//                $(".AllIndia_div").hide();
//            } else if (RegionCode != "0" || StateCode != "0")
//            {
//                if (RegionCode == "0")
//                {
//                    RegionCode = 'All Region';
//                    $(".region_div").hide();
//                    $("#RegionCode").text(RegionCode);
//                    $("#StateCode").text(StateCode);
//                    $(".state_div").show();
//                    $(".AllIndia_div").hide();
//                } else if (StateCode == "0")
//                {
//                    $(".region_div").show();
//                    $("#RegionCode").text(RegionCode);
//                    StateCode = "All State"
//                    $("#StateCode").text(StateCode);
//                    $(".state_div").hide();
//                    $(".AllIndia_div").hide();
//                }
//            } else
//            {
//                $(".AllIndia_div").show();
//            }
//
//        }
    }

    function ClearRegion()
    {
        $(".region_div").hide();
        $(".state_div").hide();
        $("#ddlRegion1").val(0);
        $("#SelectedRegion").val(0);
        $("#SelectedState").val(0);
        getCommonGenerationData();
        $(".AllIndia_div").show();

    }

    function ClearState()
    {
        $(".state_div").hide();
        //$.when(BindState(3)).then(function () {
        $("#ddlRegion1").val(0);
        getCommonGenerationData();
        $(".AllIndia_div").show();
        $("#SelectedState").val(0);
        //});


    }

    function ToShowCalender1()
    {
        $("#txt_vpDate").focus();
        OldDate = $("#txt_vpDate").val();
    }

    function ToShowCalender2()
    {
        $("#txt_vpDate2").focus();
        OldDate = $("#txt_vpDate2").val();
    }

</script>        
