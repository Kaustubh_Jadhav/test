<div class="col-md-12 ErrorDiv" id="DisplayError" style="display: none;">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="alert alert-danger">
            <span id="ErrorMessage"></span>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>