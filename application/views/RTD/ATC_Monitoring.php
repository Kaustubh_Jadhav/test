<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div class="container-fluid main-section-shadow-box">
<div class="col-md-12 col-xs-12 Top_Padding">
    <div class="panel-group" id="accordion">
<input type="hidden" value="<?php echo $Lang["ATCMonitoring"] ?>" id="CommonPageTitle">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1001" data-parent="#accordion" class="col_panelTitle">
                        +&nbsp;IMPORT</a>
                </h4>
            </div>
            <div id="collapse1001" class="panel-collapse collapse in">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 table_section1">
                    <div class="col-md-12 col-xs-12">
                        <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">CORRIDOR / CONTROL AREA</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">TTC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ATC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ACTUAL FLOW (MW)</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        1	
                                    </td>
                                    <td>
                                        NR	
                                    </td>
                                    <td>
                                        16150	
                                    </td>
                                    <td>
                                        15350	
                                    </td>
                                    <td>
                                        8097
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2	
                                    </td>
                                    <td>
                                        ER			
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        -5581
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3	
                                    </td>
                                    <td>
                                        NER	
                                    </td>
                                    <td>
                                        1350	
                                    </td>
                                    <td>
                                        1305	
                                    </td>
                                    <td>
                                        0
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        4	
                                    </td>
                                    <td>
                                        SR	
                                    </td>
                                    <td>
                                        10000	
                                    </td>
                                    <td>
                                        9250	
                                    </td>
                                    <td>
                                        4629
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        5	
                                    </td>
                                    <td>
                                        WR			
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        -7479
                                    </td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1002" data-parent="#accordion" class="col_panelTitle">
                        +&nbsp; EXPORT</a>
                </h4>
            </div>
            <div id="collapse1002" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 table_section1">
                    <div class="col-md-12 col-xs-12">
                        <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">CORRIDOR / CONTROL AREA</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">TTC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ATC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ACTUAL FLOW (MW)</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        1	
                                    </td>
                                    <td>
                                        NR	
                                    </td>
                                    <td>
                                        4500	
                                    </td>
                                    <td>
                                        3800	
                                    </td>
                                    <td>
                                        -8097
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2	
                                    </td>
                                    <td>
                                        ER			
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        5581
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3	
                                    </td>
                                    <td>
                                        NER	
                                    </td>
                                    <td>
                                        2000	
                                    </td>
                                    <td>
                                        1955	
                                    </td>
                                    <td>
                                        -482
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        4	
                                    </td>
                                    <td>
                                        SR	
                                    </td>
                                    <td>
                                        999999	
                                    </td>
                                    <td>
                                        999999	
                                    </td>
                                    <td>
                                        -4629
                                    </td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1003" data-parent="#accordion" class="col_panelTitle">
                        +&nbsp; EXPORT</a>
                </h4>
            </div>
            <div id="collapse1003" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 table_section1">
                    <div class="col-md-12 col-xs-12">
                        <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">CORRIDOR / CONTROL AREA</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">TTC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ATC</th>
                            <th data-type="numeric" class="th_head" style="text-align: center;">ACTUAL FLOW (MW)</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        1	
                                    </td>
                                    <td>
                                        NR->WR	
                                    </td>
                                    <td>
                                        2500	
                                    </td>
                                    <td>
                                        2000	
                                    </td>
                                    <td>
                                        -4753
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2	
                                    </td>
                                    <td>
                                        WR->NR	
                                    </td>
                                    <td>
                                        11300	
                                    </td>
                                    <td>
                                        10800	
                                    </td>
                                    <td>
                                        4753
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3	
                                    </td>
                                    <td>
                                        WR->SR	
                                    </td>
                                    <td>
                                        5200	
                                    </td>
                                    <td>
                                        4700	
                                    </td>
                                    <td>
                                        1321
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        4	
                                    </td>
                                    <td>
                                        SR->WR	
                                    </td>
                                    <td>
                                        999999
                                    </td>
                                    <td>
                                        999999	
                                    </td>
                                    <td>
                                        -1321
                                    </td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="height_div"></div>
</div>



<script type="text/javascript">


    $("#MainDetails").html("<span id='DesktopTitle'>ATC MONITORING </span>DATA FOR ");
    $(document).ready(function () {

        $("#ATCMonitor").addClass('SelectedView');////to show selected view

    });
</script>
