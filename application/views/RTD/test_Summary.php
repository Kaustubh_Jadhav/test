<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<div class="container-fluid main-section-shadow-box">
<div class="col-md-12 col-xs-12 Top_Padding">
    <input type="hidden" value="<?php echo $Lang["NationalSummary"] ?>" id="CommonPageTitle">  
    <div class="panel-group" id="accordion1">
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse9">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse9" class="col_panelTitle">
                       <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>FREQUENCY / GENERATION</a>
                </h4>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 mobile_sec table_section">
                    <div class="col-md-6 col-xs-12 Table_sec_padding">
                        <table class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th style="text-align: left !important;">Frequency</th>
                                    <th class="th_head">NEW GRID</th>
                                    <th class="th_head">SR GRID</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left !important;">Freq.(Hz)</td>
                                    <td><?php echo $NewData["DURGA_CS.BUS.F_B1.*.HZ"] ?></td>
                                    <td><?php echo $NewData["SMNHL_CS.BUS.F_B1.*.HZ"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">UI Rate(Paise/Unit)</td>
                                    <td><?php echo $NewData["REP_NLDC.LINE.UMCP_DSM.*.MW"] ?></td>
                                    <td><?php echo $NewData["REP_NLDC.LINE.UMCP_DSM.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">DEM. MET(MW)</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.NEW_GRID_DMD.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_SCS.SENT.SR_DEMAND.*.MW"] ?></td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>

                    <div class="col-md-6 col-xs-12 Table_sec_padding">
                        <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th style="text-align: left !important;">Generation</th>
                                    <th class="th_head">Values</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left !important;">GAS</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.TOTAL_NLDC_GAS.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">THERMAL</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.TOTAL_THM_ONLY.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">NUCLEAR</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.TOTAL_NLDC_NPC.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">HYDRO</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.TOTAL_NLDC_HYD.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">WIND</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.ALL_INDIA_WIND.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">SOLAR</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.ALL_IND_SOLAR.*.MW"] ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">TOTAL</td>
                                    <td><?php echo $NewData["REP_NLDC.SENT.NLDC_DEMAND.*.MW"] ?></td>
                                </tr>


                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse10">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse10" class="col_panelTitle">
                        <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>BANGLADESH</a>
                </h4>
            </div>
            <div id="collapse10" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 mobile_sec col-xs-12 table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;z-index: 1;">

                            <table style="width: 100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan" width="50%" style="height: 37px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">BANGLADESH</td>
                                    </tr>                  
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 672px;min-width:100%;">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 102px;">Frequency</th>
                                        <th class="th_head" style="width: 102px;">Schedule</th> 
                                        <th class="th_head" style="width: 102px;">Actual</th> 
                                        <th class="th_head" style="width: 102px;">UI</th> 
                                        <th class="th_head" style="width: 132px;">Voltage(400KV)</th>  
                                        <th class="th_head" style="width: 132px;">Voltage(230KV)</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left;"><?php echo $NewData["BHVDC_CS.BUS.E_B1.*.HZ"] ?></td>
                                        <td style="text-align: left;"><?php echo $NewData["REPOT_CS.SENT.BDESH_SCHEDULE.*.MW"] ?></td>
                                        <td style="text-align: left;"><?php echo $NewData["REP_NLDC.SENT.BANGLADESH_ACT.*.MW"] ?></td>
                                        <td style="text-align: left;"><?php echo $NewData["REP_NLDC.UI.BANGLADESH_UI.*.MW"] ?></td>
                                        <td style="text-align: left;"><?php echo $NewData["BHVDC_CS.BUS.F_B1.*.KV"] ?></td>
                                        <td style="text-align: left;"><?php echo $NewData["BHVDC_CS.BUS.E_B1.*.KV"] ?></td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse1">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1" class="col_panelTitle">
                        <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>North Region (NR)</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 col-xs-12 mobile_sec table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-)</i></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 80px;min-width:100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">NR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">GEN</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">DEM. MET</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">EFF. UI RATE</td>
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="Responsive_table toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 675px !important;min-width:100%">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 50px;">PUN</th> 
                                        <th class="th_head" style="width: 50px;">HAR</th> 
                                        <th class="th_head" style="width: 50px;">RAJ</th> 
                                        <th class="th_head" style="width: 50px;">DEL</th> 
                                        <th class="th_head" style="width: 50px;">UP</th>  
                                        <th class="th_head" style="width: 50px;">UTT</th>
                                        <th class="th_head" style="width: 50px;">CHD</th>
                                        <th class="th_head" style="width: 50px;">HP</th> 
                                        <th class="th_head" style="width: 50px;">J&K</th> 
                                        <th class="th_head" style="width: 75px;">TOTAL</th> 
                                        <th class="th_head" style="width: 75px;">ISGS</th> 
                                        <th class="th_head" style="width: 75px;">NR_IR</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.PSEB_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.HVPNL_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.RRVPNL_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.DVB_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.UPPCL_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.UTTR_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.CHND_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.HPSEB_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.JKSEB_S.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.SHDL_STATE.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.TOTAL_PG.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL1.SHDL_IR.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["NRLDC_PG.LINE.PSEB_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.HVPNL_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.RRVPNL_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.DVB_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.UPPCL_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.UTTR_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.CHND_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.HPSEB_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.JKSEB_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.DRWL.DRWL_STATE.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SSCOM.TOTAL_PG_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SENT.TOTAL_IR.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["NRLDC_PG.LINE.PSEB_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.HVPNL_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.RRVPNL_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.DVB_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.UPPCL_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.UTTR_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.CHND_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.HPSEB_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.JKSEB_OD_UD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SHDL.SHDL_STATE_ODU.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.OGUG.TOTAL_PG.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.OGUG.UI_IR.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["RPTPS_PG.SSCOM.TOTAL_PS_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTHS_PG.SSCOM.TOTAL_HS_GEN_F.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTRS_PG.SSCOM.TOTAL_RS_GEN_R.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTDV_PG.SSCOM.TOTAL_DV_GEN_B.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTUP_PG.SSCOM.TOTAL_UP_GEN_T.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTUP_PG.SSCOM.HYDRO_UTT_GEN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td><?php echo $NewData["RPTHP_PG.SSCOM.TOTAL_HP_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["RPTJK_PG.SSCOM.HYDRO_JK_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.SSCOM.TOTAL_PG_STATE.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["NRLDC_PG.LD.PSEB_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.HVPNL_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.RRVPNL_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.DVB_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.UPPCL_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.UTTR_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LINE.CHND_DRWL.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.HPSEB_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.JKSEB_LOAD.*.MW"] ?></td>
                                        <td><?php echo $NewData["NRLDC_PG.LD.TOTAL_NR_LOAD.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N3_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.N1_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div> 
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse2">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse2" class="col_panelTitle">
                       <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>East Region (ER)</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 col-xs-12 mobile_sec">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-)</i></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 83px;min-width:100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">ER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">GEN</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">DEM. MET</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">EFF. UI RATE</td>
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="Responsive_table toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 666px !important;min-width:100%">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 65px;">WBSEB</th> 
                                        <th class="th_head" style="width: 65px;">BSEB</th> 
                                        <th class="th_head" style="width: 65px;">OPTCL</th> 
                                        <th class="th_head" style="width: 65px;">JSEB</th>  
                                        <th class="th_head" style="width: 65px;">DVC</th>  
                                        <th class="th_head" style="width: 65px;">SIKKIM</th>
                                        <th class="th_head" style="width: 92px;">TOTAL</th>
                                        <th class="th_head" style="width: 92px;">ISGS</th> 
                                        <th class="th_head" style="width: 92px;">ER_IR</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["REPOT_CS.SENT.WBSEB_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.BSEB_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.GRIDC_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.JSEB_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.DVC_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.SIKKI_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_ST_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTGEN_CS_SCH1.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.EXPORT_SCHD.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_WB.SENT.TOTAL_WB_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_BH.SENT.TOTAL_BH_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_GR.SENT.TOTAL_GR_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_JH.SENT.TOTAL_JH_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_DV.SENT.TOTAL_DV_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_SI.SENT.TOTAL_SI_DWRL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_ST_EXPO.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_CS_GEN2.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_IR_EXCH.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_CS.UI.UI_WBSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_BSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_GRIDCO_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_JSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_DVC_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_SIKKIM_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_ST_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_CS_MW1.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_IR_MW.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_WB.SENT.TOTAL_WB_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_BH.SENT.TOTAL_BH_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_GR.SENT.TOTAL_GR_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_JH.SENT.TOTAL_JH_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_DV.SENT.TOTAL_DV_GEN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_STATE_GN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_WB_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_BH_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_GR_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_JH_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_DV_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_SI_DMND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.TOTAL_REG_DMND.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.E1_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse3">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse3" class="col_panelTitle">
                        <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>West Region (WR)</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 col-xs-12 mobile_sec table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-)</i></p>
                    </div>
                    <div class="clearfix"></div>            
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 80px;min-width:100%;;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">WR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">GEN</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">DEM. MET</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">EFF. UI RATE</td>
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="Responsive_table toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 822px !important;min-width:100%">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 60px;">MSEB</th> 
                                        <th class="th_head" style="width: 60px;">GEB</th> 
                                        <th class="th_head" style="width: 60px;">MPSEB</th> 
                                        <th class="th_head" style="width: 60px;">CSEB</th>  
                                        <th class="th_head" style="width: 60px;">DNH</th>  
                                        <th class="th_head" style="width: 60px;">D&DIU</th>
                                        <th class="th_head" style="width: 60px;">GOA</th>
                                        <th class="th_head" style="width: 60px;">ESSAR</th> 
                                        <th class="th_head" style="width: 114px;">TOTAL</th> 
                                        <th class="th_head" style="width: 114px;">ISGS</th> 
                                        <th class="th_head" style="width: 114px;">WR_IR</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["REPT_MSB.SENT.TOTAL_MSB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GEB.SENT.TOTAL_GEB_SCHD.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REPT_MPB.SENT.TOTAL_MPB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_CSB.SENT.TOTAL_CSB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DNH_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DD_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GOA.SENT.TOTAL_GOA_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.ESSARST_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.TOTAL_REG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.PL.WR_ISGS_SCH.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WRCS_SCHD.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPT_MSB.SENT.MSEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GEB.SENT.GEB_ACTUAL.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REPT_MPB.SENT.MPEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_CSB.SENT.CSEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DNH_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DD_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GOA.SENT.GOA_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.PL.ESSARST_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.TOTAL_REG_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.PL.WR_ISGS_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WRCS_ACTUAL.*.MW"] ?></td>

                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPT_MSB.UI.UI_MSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GEB.UI.UI_GEB_MW.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REPT_MPB.UI.UI_MPEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_CSB.UI.UI_CSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_DNH_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_DD_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GOA.UI.UI_GOA_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_ESSAR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_TOTAL_REG.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_CS_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_WR_IR.*.MW"] ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REPT_MSB.SCOM.MSEB_GENRATION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GEB.SCOM.GEB_GENERATION.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REPT_MPB.SCOM.MPEB_GENRATION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_CSB.SCOM.CSEB_GENRATION.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.TOTAL_STAT_GEN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REPT_MSB.SENT.MSEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GEB.SENT.GEB_DEMAND.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REPT_MPB.SENT.MPEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_CSB.SENT.CSEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DNH_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.DD_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_GOA.SENT.GOA_DEMAND.*.MW"] ?></td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WRCS_DEMAND.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>  
                                        <td><?php echo $NewData["REP_NLDC.LINE.W1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W3_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.W2_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse4">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse4" class="col_panelTitle">
                        <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>South Region (SR)</a>
                </h4>
            </div>  
            <div id="collapse4" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 col-xs-12 mobile_sec table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-)</i></p>
                    </div>
                    <div class="clearfix"></div>     
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 80px;min-width:100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">SR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">GEN</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">DEM. MET</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">EFF. UI RATE</td>
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="Responsive_table toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 613px !important;min-width:100%">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 50px;">AP</th> 
                                        <th class="th_head" style="width: 50px;">TLG</th> 
                                        <th class="th_head" style="width: 50px;">AP+TLG</th> 
                                        <th class="th_head" style="width: 50px;">TN</th>  
                                        <th class="th_head" style="width: 50px;">KAR</th>  
                                        <th class="th_head" style="width: 50px;">KER</th>
                                        <th class="th_head" style="width: 50px;">PONDI.</th>
                                        <th class="th_head" style="width: 50px;">GOA</th> 
                                        <th class="th_head" style="width: 71px;">TOTAL</th> 
                                        <th class="th_head" style="width: 71px;">ISGS</th> 
                                        <th class="th_head" style="width: 71px;">SR_IR</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["REPT_APB.shdl.SEEMANDRA_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TLG.SHDL.TELANGANA_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SHDL.AP_TLGNA_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TNB.SENT.TOTAL_TNB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KEB.SENT.TOTAL_KEB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KSB.SENT.TOTAL_KSB_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_UTP.SENT.TOTAL_UTP_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SHDL.GOA_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.TOTAL_REG_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.PL.TOTAL_CS_SHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.SR_IR_SHD.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPT_APB.SENT.SEEMANDRA_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TLG.SENT.TELANGANA_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.AP_TLGNA_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TNB.SENT.TNEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KEB.SENT.KEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KSB.SENT.KSEB_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_UTP.SENT.UTP_ACTUAL.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.GOA_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.TOTAL_REG_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.PL.TOTAL_CS_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.SR_IR_ACT.*.MW"] ?></td>

                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPT_APB.UI.SEEMANDRA_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TLG.UI.TELANGANA_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.UI.AP_TLGNA_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TNB.UI.UI_TNEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KEB.UI.UI_KEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KSB.UI.UI_KSEB_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_UTP.UI.UI_UTP_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.UI.GOA_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.UI.TOTAL_REG_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.UI.TOTAL_CS_UI.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.UI.UI_SR_IR.*.MW"] ?></td>
                                    </tr>
                                    <tr>                                
                                        <td><?php echo $NewData["REPT_APB.SENT.APEB_GENERTION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TLG.SENT.TLGN_GENERTION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.AP_TLGNA_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TNB.SENT.TNEB_GENERTION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KEB.SENT.KEB_GENERTION.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KSB.SENT.KSEB_GENERTION.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.SR_TOTAL_SUM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REPT_APB.SENT.APEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TLG.SENT.TLGNA_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.AP_TLGNA_DMD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_TNB.SENT.TNEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KEB.SENT.KEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_KSB.SENT.KSEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_UTP.SENT.UTP_ACTUAL.*.MW"] ?></td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPT_SCS.SENT.SR_DEMAND.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S1_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S3_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.S1_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-></td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div> 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse5">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse5" class="col_panelTitle">
                      <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>North East Region (NER)</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 mobile_sec table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-)</i></p>
                    </div>
                    <div class="clearfix"></div>     
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-4 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 80px;min-width:100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">NER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">GEN</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">DEM. MET</td>              
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">EFF. UI RATE</td>
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="table_design col-xs-8 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;">
                            <table id="table_details" class="Responsive_table toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 542px !important;min-width:100%">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width: 50px;">ASEB</th> 
                                        <th class="th_head" style="width: 50px;">MESEB</th> 
                                        <th class="th_head" style="width: 50px;">TRIP</th> 
                                        <th class="th_head" style="width: 50px;">MAN</th>  
                                        <th class="th_head" style="width: 50px;">MIZO</th>  
                                        <th class="th_head" style="width: 50px;">NAGA</th>
                                        <th class="th_head" style="width: 50px;">AP</th>
                                        <th class="th_head" style="width: 64px;">TOTAL</th> 
                                        <th class="th_head" style="width: 64px;">ISGS</th> 
                                        <th class="th_head" style="width: 64px;">NER_IR</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ASEB_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MESEB_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TED_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MANIP_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MIZO_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.NAGA_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ARUN_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TOTAL_PG_SCHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.SCOM.SCHD_PG_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ER_PG_SCHD.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_PG.LINE.ASEB_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MESEB_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TED_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MANIP_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MIZO_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.NAGA_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ARUN_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TOTAL_PG_ACT_N.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.SCOM.TOTAL_PG_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ER_EXCHANGE.*.MW"] ?></td>

                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_PG.LINE.ASEB_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MESEB_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TED_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MANIP_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MIZO_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.NAGA_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ARUN_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TOTAL_PG_DEVTN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.SENT.GEN_PG_DEVTN.*.MW"] ?></td>                                        
                                        <td><?php if(array_key_exists("bnchv_pg.bus.f_2b.*.kv", $NewData)){echo $NewData["bnchv_pg.bus.f_2b.*.kv"];}else{ echo "-"; } ?></td>
                                        <!--<td>-</td>-->
                                    </tr>
                                    <tr>                                

                                        <td><?php echo $NewData["REPOT_AS.SCOM.TOTAL_AS_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_ME.SCOM.TOTAL_ME_GEN.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_TE.SCOM.TOTAL_TE_GEN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><?php echo $NewData["REPOT_PG.SCOM.TOTAL_STATE_GN.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ASEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MESEB_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.TED_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MANIP_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.MIZO_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.NAGA_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.LINE.ARUN_DEMAND.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_PG.SCOM.TOT_REG_DEMAND.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A2_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A1_DSM.*.MW"] ?></td>
                                        <td><?php echo $NewData["REP_NLDC.LINE.A2_DSM.*.MW"] ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse6">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse6" class="col_panelTitle">
                       <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>IR Exchange</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 mobile_sec table_section">
                    <div class="col-md-12 Table_sec_padding">
                        <p class="disclaimer"><i>All figures in MW, OD(+)/UD(-), Region 1 --> Region 2 = +ve</i></p>
                    </div>
                    <div class="clearfix"></div>     
                    <div class="col-md-12 Table_sec_padding">
                        <div class="col-xs-5 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                            <table style="width: 120px;min-width:100%" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th class="td_colspan">IR Exchange</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left !important;">SHDL</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">ACT</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left !important;">UI</td>              
                                    </tr>
                                </tbody>    
                            </table>
                        </div>
                        <div class="col-xs-7 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;overflow-x: auto;">
                            <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 500px;min-width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="th_head" style="width:300px;">ER-NR</th> 
                                        <th class="th_head" style="width:300px;">WR-NR</th> 
                                        <th class="th_head" style="width:300px;">ER-SR</th> 
                                        <th class="th_head" style="width:300px;">ER-WR</th>  
                                        <th class="th_head" style="width:300px;">WR-SR</th>  
                                        <th class="th_head" style="width:300px;">ER-NER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $NewData["REPOT_CS.SENT.NR_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WR_NR_EXCH_SHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.SR_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.WR_SCHEDULE.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WR_SR_EXCH_SHD.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.SENT.NER_SCHEDULE.*.MW"] ?></td>
                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_CS.LINE.NR_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WR_NR_EXCH_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.LINE.SR_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.LINE.WR_NIC.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.SENT.WR_SR_EXCH_ACT.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.LINE.NER_NIC.*.MW"] ?></td>

                                    </tr>
                                    <tr>

                                        <td><?php echo $NewData["REPOT_CS.UI.UI_NR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_WR_NR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_SR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_WR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPT_WCS.UI.UI_WR_SR_MW.*.MW"] ?></td>
                                        <td><?php echo $NewData["REPOT_CS.UI.UI_NER_MW.*.MW"] ?></td>        
                                    </tr>
                                </tbody>   
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse7">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse7" class="col_panelTitle">
                       <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>PUMP Storage</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">  
                <div class="col-md-12">&nbsp;</div>                
                <div class="col-md-12 col-xs-12 Table_sec_padding table_section">
                    <div class="col-xs-6 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                        <table style="width: 120px;min-width: 100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th class="td_colspan">PUMP Storage</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left !important;">CAP.</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left !important;">GEN.</td>
                                </tr>
                            </tbody>    
                        </table>
                    </div>
                    <div class="col-xs-6 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;overflow-x: auto;">
                        <table style="width: 450px;min-width: 100%;" id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true" style="width: 768px;min-width: 100%;">
                            <thead>
                                <tr>
                                    <th class="th_head" style="width: 25%;">PURULIA</th> 
                                    <th class="th_head" style="width: 25%;">KADAMPARAI</th> 
                                    <th class="th_head" style="width: 25%;">SRI SALEM</th> 
                                    <th class="th_head" style="width: 25%;">GHATGHAR</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>900</td>
                                    <td>400</td>
                                    <td>900</td>
                                    <td>250</td>
                                </tr>
                                <tr>
                                    <td><?php echo $NewData["PPSP__WB.LINE.PPSP_CALC.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_TNB.PL.KDMPRI_GEN.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_APB.PL.SLMLPH_GEN.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_MSB.PL.GHAT_GEN.*.MW"] ?></td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" href="#collapse8">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse8" class="col_panelTitle">
                       <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>Regional Virtual Entity</a>
                </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12 col-xs-12 Table_sec_padding">
                    <div class="col-xs-5 col-md-3" style="padding-right: 0px;padding-left: 0px;margin-left: -1px;z-index: 1;">
                        <table style="width: 120px;min-width: 100%;" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                            <thead>
                                <tr>
                                    <th class="td_colspan">Reg Virtual Entity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left !important;">SHDL.</td>
                                </tr>
                            </tbody>    
                        </table>
                    </div>
                    <div class="col-xs-7 col-md-9" style="padding-left: 0px;padding-right: 0px;margin-left: -1px;overflow-x: auto;">
                        <table id="table_details" class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" style="width: 582px;min-width: 100%;" data-sorting="true">
                            <thead>
                                <tr>
                                    <th class="th_head padding_head" style="width:97px;">All India</th> 
                                    <th class="th_head" style="width:97px;">NR</th> 
                                    <th class="th_head" style="width:97px;">WR</th> 
                                    <th class="th_head" style="width:97px;">SR</th>  
                                    <th class="th_head" style="width:97px;">ER</th>  
                                    <th class="th_head" style="width:97px;">NER</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $NewData["REP_NLDC.SHDL.VAE_SCH_INDIA.*.MW"] ?></td>
                                    <td><?php echo $NewData["NRLDC_PG.SHDL.VAE_SCHD.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_WCS.SHDL.VAE_SCHD.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPT_SCS.SHDL.VAE_SCHD.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPOT_CS.SHDL.VAE_SCHD.*.MW"] ?></td>
                                    <td><?php echo $NewData["REPOT_PG.SHDL.VAE_SCHD.*.MW"] ?></td>
                                </tr>
                            </tbody>   
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">&nbsp;</div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
<script type="text/javascript">
   
    $("#MainDetails").html("<span id='DesktopTitle'>NATIONAL SUMMARY</span> DATA FOR " + '<?php echo date('d M Y') ?>');

    $(document).ready(function () {

        $("#divNationalSum").addClass('SelectedView');////to show selected view

    });

    function toggleIcon(e) {
        $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus-sign glyphicon-minus-sign');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);


</script>