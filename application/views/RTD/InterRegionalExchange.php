<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<input type="hidden" value="<?php echo $Lang["InterRegionalExchange"] ?>" id="CommonPageTitle"> 
<div class="container-fluid main-section-shadow-box" id="interregionalexchange_view">
    <a id="ToogelPanel" style="color: #000 !important;float: right;text-decoration: underline;cursor: pointer;" onclick="ToogelPanel()">Expand All</a>
    <div class="col-md-12 col-xs-12 Top_Padding no-padding-horizontal">
        <div class="panel-group" id="accordion1">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse101">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse101" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(A) ER - NR</a>
                    </h4>
                </div>
                <div id="collapse101" class="panel-collapse collapse">                   
                    <div class="col-md-12">&nbsp;</div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 col-xs-12 table_section">
                        <div class="col-md-12 col-xs-12 table_design no-padding-left">
                            <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                        <th data-type="numeric" class="th_head" style="text-align: center;">(A) ER - NR</th>
                                        <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                        <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            1	
                                        </td>
                                        <td>
                                            765 KV Sasaram-Fatehpur	
                                        </td>
                                        <td>
                                            <?php echo $NewData["SASAR_CS.ZBR.G_FATHE_SASAR.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["FATPR_PG.ZBR.G_FATPR_SASRM.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2	
                                        </td>
                                        <td>
                                            765 KV Gaya-Balia	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GAYA__CS.zbr.G_BALIA_GAYA_.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.zbr.G_BALIA_GAYA_.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3	
                                        </td>
                                        <td>
                                            400 KV Muzaff-Gorakhpur 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["MUZAF_CS.LINE.F_GRKPR_MUZAF1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GRKPR_PG.LINE.F_GRKPR_MUZAF1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4	
                                        </td>
                                        <td>
                                            400 KV Muzaff-Gorakhpur 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["MUZAF_CS.LINE.F_GRKPR_MUZAF2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GRKPR_PG.LINE.F_GRKPR_MUZAF2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5	
                                        </td>
                                        <td>
                                            400 KV Patna-Balia 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["PATNA_CS.LINE.F_BALIA_PATNA1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_balia_patna1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            6	
                                        </td>
                                        <td>
                                            400 KV Patna-Balia 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["PATNA_CS.LINE.F_BALIA_PATNA2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_BALIA_PATNA2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            7	
                                        </td>
                                        <td>
                                            400 KV Patna-Balia 3
                                        </td>
                                        <td>
                                            <?php echo $NewData["PATNA_CS.LINE.F_BALIA_PATNA3.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_balia_patna3.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            8	
                                        </td>
                                        <td>
                                            400 KV Patna-Balia 4	
                                        </td>
                                        <td>
                                            <?php echo $NewData["PATNA_CS.LINE.F_BALIA_PATNA4.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_balia_patna4.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            9	
                                        </td>
                                        <td>
                                            400 KV Biharshariff-Balia 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BIHAR_CS.LINE.F_BALIA_BIHAR1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_BALIA_BIHAR1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            10	
                                        </td>
                                        <td>
                                            400 KV Biharshariff-Balia 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BIHAR_CS.LINE.F_BALIA_BIHAR2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BALIA_PG.ZBR.F_BALIA_BIHAR2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            11	
                                        </td>
                                        <td>
                                            400 KV Sasaram-Allahabad	
                                        </td>
                                        <td>
                                            <?php echo $NewData["SASAR_CS.ZBR.F_ALBAD_SASAR1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["ALBAD_PG.LINE.F_ALBAD_SASAR.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            12	
                                        </td>
                                        <td>
                                            400 KV Sasaram-Sarnath	
                                        </td>
                                        <td>
                                            <?php echo $NewData["SASAR_CS.ZBR.F_SARND_SASAR1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["VRNSI_PG.LINE.F_SASAR_VRNSI1.*.MW"] ?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            13
                                        </td>
                                        <td>
                                            220 KV Pusauli-Sahupuri	
                                        </td>
                                        <td>
                                            <?php echo $NewData["SASAR_CS.LINE.E_SAHUP_SASAR.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["SAHUP_UP.LINE.e_SAHUP_SASAR.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            14	
                                        </td>
                                        <td>
                                            400kV Motihari-Gorakhpur 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["MOTIH_CS.ZBR.F_GRKPR_MOTIH1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GRKPR_PG.ZBR.F_GRKPR_MOTIH1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            15	
                                        </td>
                                        <td>
                                            400kV Motihari-Gorakhpur 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["MOTIH_CS.ZBR.F_GRKPR_MOTIH2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GRKPR_PG.ZBR.F_GRKPR_MOTIH2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            16	
                                        </td>
                                        <td>
                                            400 KV Biharshariff-Varanasi 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BIHAR_CS.ZBR.F_BIHAR_VRNSI1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["VRNSI_PG.ZBR.F_BIHAR_VRNSI1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            17	
                                        </td>
                                        <td>
                                            400 KV Biharshariff-Varanasi 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["BIHAR_CS.ZBR.F_BIHAR_VRNSI2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["VRNSI_PG.ZBR.F_BIHAR_VRNSI2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            18	
                                        </td>
                                        <td>
                                            765 KV Gaya-Varanasi 1	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GAYA__CS.ZBR.G_GAYA__VRNSI1.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["VRNSI_PG.ZBR.G_GAYA__VRNSI1.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            19	
                                        </td>
                                        <td>
                                            765 KV Gaya-Varanasi 2	
                                        </td>
                                        <td>
                                            <?php echo $NewData["GAYA__CS.ZBR.G_GAYA__VRNSI2.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["VRNSI_PG.ZBR.G_GAYA__VRNSI2.*.MW"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            20	
                                        </td>
                                        <td>
                                            800kV HVDC Agra-Alipurduar Pole 3	
                                        </td>
                                        <td>
                                            <?php echo $NewData["AGRA__PG.LD.HVDC_POLE_3.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["ALIPU_CS.DCPO.ALIPU_POLE3.*.MWPO"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            21	
                                        </td>
                                        <td>
                                            800kV HVDC Agra-Alipurduar Pole 4	
                                        </td>
                                        <td>
                                            <?php echo $NewData["AGRA__PG.LD.HVDC_POLE_4.*.MW"] ?>	
                                        </td>
                                        <td>
                                            <?php echo $NewData["ALIPU_CS.DCPO.ALIPU_POLE4.*.MWPO"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            Total	
                                        </td>
                                        <td>
                                            <?php echo $NewData["NRLDC_PG.LINE.ER_NIC.*.MW"] ?>
                                        </td>
                                        <td>
                                            <?php echo $NewData["REPOT_CS.LINE.NR_NIC.*.MW"] ?>	
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse102">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse102" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(B) WR - NR</a>
                    </h4>
                </div>
                <div id="collapse102" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 col-xs-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(B) WR - NR</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                765 KV Gwalior-Agra 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["GWALI_CS.ZBR.g_AGRA__GWLIR1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["AGRA__PG.LINE.G_AGRA__GWLIR1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                765 KV Gwalior-Agra 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["GWALI_CS.ZBR.G_AGRA__GWLIR2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["AGRA__PG.LINE.G_AGRA__GWLIR2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3	
                                            </td>
                                            <td>
                                                765 KV Gwalior- Phagi 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["GWALI_CS.ZBR.G_GWALI_PHAGI1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["PHAGI_RS.LINE.G_GWALI_PHAGI1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4	
                                            </td>
                                            <td>
                                                765 KV Gwalior- Phagi 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["GWALI_CS.ZBR.G_GWALI_PHAGI2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["PHAGI_RS.LINE.G_GWALI_PHAGI2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5	
                                            </td>
                                            <td>
                                                400 KV Zerda-Kankaroli	
                                            </td>
                                            <td>
                                                <?php echo $NewData["knsri_ge.line.F_knkrl_zerda.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["KNKRL_PG.ZBR.F_KNKRL_ZERDA.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6	
                                            </td>
                                            <td>
                                                400 KV Zerda-Bhinmal	
                                            </td>
                                            <td>
                                                <?php echo $NewData["knsri_ge.line.f_BHNML_ZERDA.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BHNML_PG.LINE.F_BHNML_ZERDA.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                7
                                            </td>
                                            <td>
                                                400 KV Vindhyachal PS-Rihand 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["VIND7_CS.LINE.F_RIHND_VIND71.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["RIHND_NT.LINE.F_POOLP_RIHND1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                8	
                                            </td>
                                            <td>
                                                400 KV Vindhyachal PS -Rihand 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["VIND7_CS.LINE.F_RIHND_VIND72.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["RIHND_NT.LINE.F_POOLP_RIHND2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                9	
                                            </td>
                                            <td>
                                                HVDC Vindhyachal Back-to-Back	
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_WCS.SENT.VSTPS_HVDC.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["CALCU_PG.LINE.F_VIN_NT__VIND.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                10	
                                            </td>
                                            <td>
                                                HVDC Adani-Mahendragarh-Pole 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ADANI_GE.POLE1.POLE_1_DATA.*.DCMW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["MNDRG_PG.LINE.F_MNDRG_MUNHV1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                11	
                                            </td>
                                            <td>
                                                HVDC Adani-Mahendragarh Pole 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ADANI_GE.POLE2.POLE_2_DATA.*.DCMW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["MNDRG_PG.LINE.F_MNDRG_MUNHV2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                12	
                                            </td>
                                            <td>
                                                220 KV Badod-Modak
                                                <!--  Not Found-->
                                            </td>
                                            <td>
                                                -	
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                13	
                                            </td>
                                            <td>
                                                220 KV Badod-Kota	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BADOD_MP.LINE.E_BADOD_KOTA.*.MW"] ?>	
                                            </td>
                                            <td>
                                                -
                                                <!--  Not Found-->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                14	
                                            </td>
                                            <td>
                                                220 KV Malanpur-Auraiya	
                                            </td>
                                            <td>
                                                <?php echo $NewData["MLNPR_MP.LINE.E_AURIY_MALAN.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["AURIY_NT.LINE.E_AURIY_MALAN.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                15	
                                            </td>
                                            <td>
                                                220 KV Mehgaon-Auraiya	
                                            </td>
                                            <td>
                                                <?php echo $NewData["MEHGN_MP.LINE.E_AURIY_MGH220.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["AURIY_NT.LINE.E_AURIY_MGH220.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                16	
                                            </td>
                                            <td>
                                                Vindhyachal Angle	
                                            </td>
                                            <td>
                                                <?php echo $NewData["VINDH_WR.BUS.ANGLE.*.MW"] ?>	
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                17	
                                            </td>
                                            <td>
                                                Sujalpur-RAPPC 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["SJLPR_CS.ZBR.F_RAPPC_SJLPR1.*.MW"] ?>

                                            </td>
                                            <td>
                                                <?php echo $NewData["RAPPC_NP.LINE.F_RAPPC_SJLPR1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                18	
                                            </td>
                                            <td>
                                                Sujalpur-RAPPC 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["SJLPR_CS.ZBR.F_RAPPC_SJLPR2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAPPC_NP.LINE.F_RAPPC_SJLPR2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                19	
                                            </td>
                                            <td>
                                                HVDC Champa-Kurushetra 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHMPA_CS.STTN.DCPOLE1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["KURUK_PG.DCCNV.KURUK_HVDC1.*.DCMW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                20	
                                            </td>
                                            <td>
                                                HVDC Champa-Kurushetra 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHMPA_CS.STTN.DCPOLE2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["KURUK_PG.DCCNV.KURUK_HVDC2.*.DCMW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                21	
                                            </td>
                                            <td>
                                                HVDC Champa-Kurushetra 3	
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHMPA_CS.STTN.DCPOLE3.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["KURUK_PG.DCCNV.KURUK_HVDC3.*.DCMW"] ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                22	
                                            </td>
                                            <td>
                                                HVDC Champa-Kurushetra 4	
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHMPA_CS.STTN.DCPOLE4.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["KURUK_PG.DCCNV.KURUK_HVDC4.*.DCMW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                23	
                                            </td>
                                            <td>
                                                765kV Jabalpur-Orai1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JBLPS_CS.ZBR.G_JBLPS_ORAI1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ORAI__PG.ZBR.G_JBLPS_ORAI1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                24	
                                            </td>
                                            <td>
                                                765kV Jabalpur-Orai2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JBLPS_CS.ZBR.G_JBLPS_ORAI2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ORAI__PG.ZBR.G_JBLPS_ORAI2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                25	
                                            </td>
                                            <td>
                                                765kV Gwalior-Orai	
                                            </td>
                                            <td>
                                                <?php echo $NewData["GWALI_CS.ZBR.G_GWLIR_ORAI.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ORAI__PG.LINE.G_GWLIR_ORAI.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                26	
                                            </td>
                                            <td>
                                                765kV Satna-Orai
                                            </td>
                                            <td>
                                                <?php echo $NewData["SATNA_CS.ZBR.G_ORAI_SATNA.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ORAI__PG.ZBR.G_ORAI_SATNA.*.MW"] ?>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                27	
                                            </td>
                                            <td>
                                                765kV Chittorgarh-Banaskantha-1
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHIT7_PG.ZBR.G_BNSK7_CHIT71.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BNSK7_CS.ZBR.G_BNSK7_CHIT71.*.MW"] ?>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                28	
                                            </td>
                                            <td>
                                                765kV Chittorgarh-Banaskantha-2
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHIT7_PG.ZBR.G_BNSK7_CHIT72.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BNSK7_CS.ZBR.G_BNSK7_CHIT72.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Total	
                                            </td>
                                            <td>
                                                <?php echo $NewData["NRLDC_PG.LINE.WR_NIC.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_WCS.SENT.WR_NR_EXCH_ACT.*.MW"] ?>	
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse103">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse103" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(C) ER - NER</a>
                    </h4>
                </div>
                <div id="collapse103" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(C) ER - NER</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                400 KV Siliguri-Bongaigaon 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["SI400_CS.LINE.F_BONGA_SI4001.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BONGA_PG.ZBR.FZBONGA_SI4001.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                400 KV Siliguri-Bongaigaon 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["SI400_CS.LINE.F_BONGA_SI4002.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BONGA_PG.ZBR.FZBONGA_SI4002.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3	
                                            </td>
                                            <td>
                                                400 KV Alipurduar-Bongaigaon 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ALIPU_CS.LINE.F_ALIPU_BONGA1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BONGA_PG.LINE.F_ALIPU_BONGA1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4	
                                            </td>
                                            <td>
                                                400 KV Alipurduar-Bongaigaon 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ALIPU_CS.LINE.F_ALIPU_BONGA2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BONGA_PG.LINE.F_ALIPU_BONGA2.*.MW"] ?>
                                            </td>
                                        <tr>
                                            <td>
                                                5	
                                            </td>
                                            <td>
                                                220 KV Alipurduar-Bongaigaon 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ALIPU_CS.LD.LD_SALAKATI1.*.MW"] ?>	
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6	
                                            </td>
                                            <td>
                                                220 KV Alipurduar-Bongaigaon 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ALIPU_CS.LD.LD_SALAKATI2.*.MW"] ?>	
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Total	
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_PG.LINE.ER_EXCHANGE.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_CS.LINE.NER_NIC.*.MW"] ?>	
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse104">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse104" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(D) ER - WR</a>
                    </h4>
                </div>
                <div id="collapse104" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" style="text-align: center;">(D) ER - WR</th>
                                            <th data-type="numeric" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                765 KV New Ranchi-Dharamjaygarh 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["NRANC_CS.ZBR.G_DJYGH_NRANC1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_NRANC1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                765 KV New Ranchi-Dharamjaygarh 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["NRANC_CS.ZBR.G_DJYGH_NRANC2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_NRANC2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3
                                            </td>
                                            <td>
                                                765 KV Jharsuguda-Dharmjaygarh 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.G_DJYGH_JHARS1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_JHARS1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4	
                                            </td>
                                            <td>
                                                765 KV Jharsuguda-Dharmjaygarh 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.G_DJYGH_JHARS2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_JHARS2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5	
                                            </td>
                                            <td>
                                                400 KV Jharsuguda-Raigarh 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.F_JHARS_RAIGR1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAIGR_CS.LINE.F_JHARS_RAIGR1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6	
                                            </td>
                                            <td>
                                                400 KV Jharsuguda-Raigarh 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.F_JHARS_RAIGR2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAIGR_CS.LINE.F_JHARS_RAIGR2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                7	
                                            </td>
                                            <td>
                                                400 KV Jharsuguda-Raigarh 3	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.F_JHARS_RAIGR3.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAIGR_CS.LINE.F_RAIGR_ROURK1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                8	
                                            </td>
                                            <td>
                                                400 KV Jharsuguda-Raigarh 4	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.F_JHARS_RAIGR4.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAIGR_CS.LINE.F_RAIGR_ROURK2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                9	
                                            </td>
                                            <td>
                                                400 KV Ranchi-Sipat 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["RANCH_CS.ZBR.F_RANCH_SIPAT1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["sipat_CS.zbr.F_ranch_sipat1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                10	
                                            </td>
                                            <td>
                                                400 KV Ranchi-Sipat 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["RANCH_CS.ZBR.F_RANCH_SIPAT2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["sipat_CS.zbr.F_ranch_sipat2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                11	
                                            </td>
                                            <td>
                                                220 KV Budhipadar-Korba 1	
                                                <!--Not Found-->
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                12	
                                            </td>
                                            <td>
                                                220 KV Budhipadar-Korba 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BUDHI_GR.LINE.E_BUDHI_KORBA2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["KE220_CG.LINE.E_BUDHI_KORBA2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                13	
                                            </td>
                                            <td>
                                                220 KV Budhipadar-Raigarh	
                                            </td>
                                            <td>
                                                <?php echo $NewData["BUDHI_GR.LINE.E_BUDHI_RGH221.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RG220_CG.LINE.E_BUDHI_RGH221.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                14	
                                            </td>
                                            <td>
                                                765 kV Dharamjaigarh - Jharsuguda-3	
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_JHARS3.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.G_DJYGH_JHARS3.*.MW"] ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                15	
                                            </td>
                                            <td>
                                                765 kV Dharamjaigarh - Jharsuguda-4	
                                            </td>
                                            <td>
                                                <?php echo $NewData["DJYGH_CS.ZBR.G_DJYGH_JHARS4.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.LINE.G_DJYGH_JHARS4.*.MW"] ?>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                16	
                                            </td>
                                            <td>
                                                765 kV Durg-Jharsuguda ckt-1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.ZBR.G_JHARS_RPRPS1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RPRPS_CS.ZBR.G_JHARS_RPRPS1.*.MW"] ?>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                17	
                                            </td>
                                            <td>
                                                767 kV Durg-Jharsuguda ckt-2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JHARS_CS.ZBR.G_JHARS_RPRPS2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REP_NLDC.LINE.SCED_NRLDC.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Total	
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_WCS.SENT.WR_ER_EXCH_ACT.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_CS.LINE.WR_NIC.*.MW"] ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse105">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse105" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(E) WR - SR</a>
                    </h4>
                </div>
                <div id="collapse105" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(E) WR - SR</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                Bhadrawati HVDC
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_WCS.SENT.BHDRV_HVDC.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_SCS.SENT.CHAND_ACTUAL.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                765 KV Solapur-Raichur 1
                                            </td>
                                            <td>
                                                <?php echo $NewData["SOLPR_CS.ZBR.G_RACHR_SOLPR1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RACHR_CS.ZBR.G_RACHR_SOLPR1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3	
                                            </td>
                                            <td>
                                                765 KV Solapur-Raichur 2
                                            </td>
                                            <td>
                                                <?php echo $NewData["SOLPR_CS.ZBR.G_RACHR_SOLPR2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["RACHR_CS.ZBR.G_RACHR_SOLPR2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4
                                            </td>
                                            <td>
                                                765 KV Wardha(PG)-Nizamabad(PG) 1
                                            </td>
                                            <td>
                                                <?php echo $NewData["WARDH_CS.ZBR.G_NIZAM_WARDH1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["NIZAM_CS.ZBR.G_NIZAM_WARDH1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5	
                                            </td>
                                            <td>
                                                765 KV Wardha(PG)-Nizamabad(PG) 2
                                            </td>
                                            <td>
                                                <?php echo $NewData["WARDH_CS.ZBR.G_NIZAM_WARDH2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["NIZAM_CS.ZBR.G_NIZAM_WARDH2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6	
                                            </td>
                                            <td>
                                                400 KV Kolhapur(PG)-Kudgi(PG) 1
                                            </td>
                                            <td>
                                                <?php echo $NewData["KOLPR_CS.LINE.F_KOLPR_KUDG41.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["KUDG4_CS.LINE.F_KOLPR_KUDG41.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                7	
                                            </td>
                                            <td>
                                                400 KV Kolhapur(PG)-Kudgi(PG) 2
                                            </td>
                                            <td>
                                                <?php echo $NewData["KOLPR_CS.LINE.F_KOLPR_KUDG42.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["KUDG4_CS.LINE.F_KOLPR_KUDG42.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                8	
                                            </td>
                                            <td>
                                                220 KV Mudasangi-Chikkodi
                                            </td>
                                            <td>
                                                <?php echo $NewData["klpr2_ms.LINE.E_CHIKD_KLPR2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHIKD_KE.LINE.E_CHIKD_KLPR2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                9	
                                            </td>
                                            <td>
                                                220 KV Talangde-Chikkodi
                                            </td>
                                            <td>
                                                <?php echo $NewData["klpr3_ms.LINE.E_CHIKD_KLPR3.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["CHIKD_KE.LINE.E_CHIKD_KLPR3.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Total
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_SCS.SENT.SR_WR_EXCH_ACT.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_WCS.SENT.WR_SR_EXCH_ACT.*.MW"] ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse106">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse106" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(F) ER - SR</a>
                    </h4>
                </div>
                <div id="collapse106" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(F) ER - SR</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                765 KV Angul-Srikakulam 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ANGUL_CS.ZBR.G_ANGUL_SRKLM1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["SRKLM_CS.ZBR.G_ANGUL_SRKLM1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                765 KV Angul-Srikakulam 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["ANGUL_CS.ZBR.G_ANGUL_SRKLM2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["SRKLM_CS.ZBR.G_ANGUL_SRKLM2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3	
                                            </td>
                                            <td>
                                                400 KV Jeypore-Gazuwaka 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JEYPO_CS.LINE.F_GAZUA_JEYPO1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["GJKPS_CS.LINE.F_GAZUA_JEYPO1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4	
                                            </td>
                                            <td>
                                                400 KV Jeypore-Gazuwaka 2	
                                            </td>
                                            <td>
                                                <?php echo $NewData["JEYPO_CS.LINE.F_GAZUA_JEYPO2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["GJKPS_CS.LINE.F_GAZUA_JEYPO2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5	
                                            </td>
                                            <td>
                                                Talcher Interconnector	
                                                <!--Not Found-->
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_CS.LINE.TACHER_IC.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_CS.LINE.TACHER_IC.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6	
                                            </td>
                                            <td>
                                                220 KV U.Sileru-Balimela	
                                            </td>
                                            <td>
                                                <?php echo $NewData["USLPH_AP.LINE .E_BALIM_UPSLR.mes1.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["BALIM_GR.LINE .E_BALIM_UPSLR.mes1.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                7	
                                            </td>
                                            <td>
                                                Talcher-Kolar HVDC Pole 1	
                                            </td>
                                            <td>
                                                <?php echo $NewData["THVDC_CS.POLE1.ACTUAL_DC_PWR.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_SCS.SENT.KOLAR_POLE1.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                8	
                                            </td>
                                            <td>
                                                Talcher-Kolar HVDC Pole 2
                                            </td>
                                            <td>
                                                <?php echo $NewData["THVDC_CS.POLE2.ACTUAL_DC_PWR.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_SCS.SENT.KOLAR_POLE2.*.MW"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Total	
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPT_SCS.SENT.SR_ER_EXCH_ACT.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["REPOT_CS.LINE.SR_NIC.*.MW"] ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse107">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse107" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(G) ER - NEPAL</a>
                    </h4>
                </div>
                <div id="collapse107" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(G)ER-Nepal</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	 
                                            </td>
                                            <td>
                                                220KV Muzaffarpur-Dhalkebar 1		
                                            </td>
                                            <td>
                                                <?php echo $NewData["MUZAF_CS.LD.E_MUZAF_NEPAL1.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	 
                                            </td>
                                            <td>
                                                220KV Muzaffarpur-Dhalkebar 2		
                                            </td>
                                            <td>
                                                <?php echo $NewData["MUZAF_CS.LD.E_MUZAF_NEPAL2.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3	 
                                            </td>
                                            <td>
                                                132KV Raxaul-New Parwanipur		
                                            </td>
                                            <td>
                                                <?php echo $NewData["RAXAU_BH.LINE.D_PRNPR_RAXUL.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse108">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse108" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(H) NER - NR</a>
                    </h4>
                </div>
                <div id="collapse108" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">NER - NR</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                800kV HVDC Agra-Biswanath Chairali Pole-1
                                            </td>
                                            <td>
                                                <?php echo $NewData["AGRA__PG.LD.HVDC_POLE_1.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["BNCHV_PG.DCPO.BNCHV_POLE1.*.MWPO"] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                800kV HVDC Agra-Biswanath Chairali Pole-2
                                            </td>
                                            <td>
                                                <?php echo $NewData["AGRA__PG.LD.HVDC_POLE_2.*.MW"] ?>
                                            </td>
                                            <td>
                                                <?php echo $NewData["BNCHV_PG.DCPO.BNCHV_POLE2.*.MWPO"] ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse9">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse9" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(I) NR - NEPAL</a>
                    </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(I)NR-Nepal</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	 
                                            </td>
                                            <td>
                                                Tanakpur-Mahendra Nagar		
                                            </td>
                                            <td>
                                                <?php echo $NewData["TNKPR_NH.LD.E_NEPAL_TNKPR.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#collapse110">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse110" class="col_panelTitle">
                            <i class="more-less glyphicon glyphicon-plus-sign panel_icon"></i>(J) NER - BANGLADESH</a>
                    </h4>
                </div>
                <div id="collapse110" class="panel-collapse collapse">                   
                    <div style="padding-left: 0px;padding-right: 0px;overflow-x: auto;">
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12 table_section">                        
                            <div class="col-md-12 col-xs-12 table_design no-padding-left">
                                <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
                                    <thead>
                                        <tr>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">(H)NER-Bangladesh</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                                            <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1	
                                            </td>
                                            <td>
                                                Surjamani-Comilla 1
                                            </td>
                                            <td>
                                                <?php echo $NewData["SURJA_TE.LINE.D_COMIL_SURJA1.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2	
                                            </td>
                                            <td>
                                                Surjamani-Comilla 2
                                            </td>
                                            <td>
                                                <?php echo $NewData["SURJA_TE.LINE.D_COMIL_SURJA2.*.MW"] ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">

    $("#MainDetails").html("<span id='DesktopTitle'>INTER REGIONAL EXCHANGE </span>DATA FOR ");
    var IsPanelOpen = 1;
    $(document).ready(function () {

        $("#divInterRegExc").addClass('SelectedView');////to show selected view

    });

    function toggleIcon(e) {
        $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus-sign glyphicon-minus-sign');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);


    $(window).on("scroll", function () {
        if ((window.innerHeight + window.scrollY) - 150 >= document.body.offsetHeight) {
            var IsMenuShow = '';
            if ($(window).width() > 991)
            {
                IsMenuShow = $("#DesktopMenu").css("display");
                if (IsMenuShow != 'block')
                {
                    ShowNewMenu();
                }
                $("#DesktopMenu").removeClass("Menu_scroll");
//                $(".box_section").css('opacity', "0.7");
                $(".box_section").css('opacity', "1");
            }
        } else
        {
            IsMenuShow = $("#DesktopMenu").css("display");
//            $("#DesktopMenu").addClass("Menu_scroll");
            if (IsMenuShow == "block")
            {
                $("#DesktopMenu").css('position', 'relative');
                ShowNewMenu();
            }
            $(".box_section").css('opacity', "0.7");
        }

    });

    function ToogelPanel() {
        if (IsPanelOpen == 1)
        {
            $(".collapse").collapse('show');
            $("#ToogelPanel").text("Collapse All");
            IsPanelOpen = 0;
        } else
        {
            $(".collapse").collapse('hide');
            $("#ToogelPanel").text("Expand All");
            IsPanelOpen = 1;
        }
    }

</script>
