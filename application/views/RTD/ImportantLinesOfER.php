<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="main-section-shadow-box" id="importantlinesofer_view">
    <input type="hidden" value="<?php echo $Lang["ImpLineOfER"] ?>" id="CommonPageTitle">
    <div class="col-md-12 Top_Padding add-overflow-x">
        <table  class="toggle-square toggle-medium footable table tbl_smallFont table-striped table-bordered table-hover" data-sorting="true">
            <thead>
                <tr>
                    <th data-type="numeric" class="th_head" style="text-align: center;">S.No.</th>
                    <th data-type="numeric" class="th_head" style="text-align: center;">(A) ER Lines</th>
                    <th data-type="numeric" class="th_head" style="text-align: center;">Flow(MW)</th>
                    <th data-type="numeric" class="th_head" style="text-align: center;">Other End Flow</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        1	
                    </td>
                    <td>
                        400 kV Bongaigaon-Alipurduar-1	
                    </td>
                    <td>
                        <?php echo $NewData["BONGA_PG.LINE.F_ALIPU_BONGA1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.F_ALIPU_BONGA1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        2	
                    </td>
                    <td>
                        400 kV Bongaigaon-Alipurduar-2	
                    </td>
                    <td>
                        <?php echo $NewData["BONGA_PG.LINE.F_ALIPU_BONGA2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.F_ALIPU_BONGA2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        3	
                    </td>
                    <td>
                        400 kV Bongaigaon-Binaguri-1
                    </td>
                    <td>
                        <?php echo $NewData["BONGA_PG.ZBR.FZBONGA_SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_BONGA_SI4001.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        4
                    </td>
                    <td>
                        400 kV Bongaigaon-Binaguri-2
                    </td>
                    <td>
                        <?php echo $NewData["BONGA_PG.ZBR.FZBONGA_SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_BONGA_SI4002.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        5	
                    </td>
                    <td>
                        400 kV Binaguri-Purnea-1
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_PURNW_SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.ZBR.F_PURNW_SI4001.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        6	
                    </td>
                    <td>
                        400 kV Binaguri-Purnea-2	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_PURNW_SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.ZBR.F_PURNW_SI4002.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        7	
                    </td>
                    <td>
                        400 kV Binaguri-Kishanganj-1
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_KISHN_SI4003.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.F_KISHN_SI4003.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        8	
                    </td>
                    <td>
                        400 kV Binaguri-Kishanganj-2
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.F_KISHN_SI4004.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.F_KISHN_SI4004.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        9	
                    </td>
                    <td>
                        400 kV Farakka-Behrampore-1	
                    </td>
                    <td>
                        <?php echo $NewData["FARAK_CS.ZBR.F_BERHA_FARAK1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BERHA_CS.LINE.F_BERHA_FARAK1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        10	
                    </td>
                    <td>
                        400 kV Farakka-Behrampore-2	
                    </td>
                    <td>					 
                        <?php echo $NewData["FARAK_CS.line.F_BERHA_FARAK2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BERHA_CS.LINE.F_BERHA_FARAK2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        11	
                    </td>
                    <td>
                        400 kV Kahalgaon-Barh-1	
                    </td>
                    <td>
                        <?php echo $NewData["KAHAL_CS.LINE.F_BARH__KAHAL1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BARH__CS.LINE.F_BARH__KAHAL1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        12	
                    </td>
                    <td>
                        400 kV Kahalgaon-Barh-2	
                    </td>
                    <td>
                        <?php echo $NewData["KAHAL_CS.LINE.F_BARH__KAHAL2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BARH__CS.LINE.F_BARH__KAHAL2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        13	
                    </td>
                    <td>
                        400 kV Muzzafarpur-Gorakhpur-1
                    </td>
                    <td>
                        <?php echo $NewData["MUZAF_CS.LINE.F_GRKPR_MUZAF1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["GRKPR_PG.LINE.F_GRKPR_MUZAF1.*.MW"] ?>	
                    </td>
                </tr>
                <tr>
                    <td>
                        14	
                    </td>
                    <td>
                        400 kV Muzzafarpur-Gorakhpur-2	
                    </td>
                    <td>
                        <?php echo $NewData["MUZAF_CS.LINE.F_GRKPR_MUZAF2.*.MW"] ?>
                    </td>
                    <td>
                        <?php echo $NewData["GRKPR_PG.LINE.F_GRKPR_MUZAF2.*.MW"] ?>	

                    </td>
                </tr>
                <tr>
                    <td>
                        15	
                    </td>
                    <td>
                        400 kV Malda-Farakaa-1	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.LINE.F_FARAK_MALDA1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["FARAK_CS.LINE.F_FARAK_MALDA1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        16	
                    </td>
                    <td>
                        400 kV Malda-Farakaa-2	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.LINE.F_FARAK_MALDA2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["FARAK_CS.LINE.F_FARAK_MALDA2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        17	
                    </td>
                    <td>
                        400 kV Purnea-Muzzaffarpur-1	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.ZBR.F_MUZAF_PURNW1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MUZAF_CS.ZBR.F_MUZAF_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        18	
                    </td>
                    <td>
                        400 kV Purnea-Muzzaffarpur-2	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.ZBR.F_MUZAF_PURNW2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MUZAF_CS.ZBR.F_MUZAF_PURNW2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        19	
                    </td>
                    <td>
                        400 kV Purnea-Biharshariff-1	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.F_BIHAR_PURNW1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BIHAR_CS.ZBR.F_BIHAR_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        20	
                    </td>
                    <td>
                        400 kV Purnea-Biharshariff-2	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.F_BIHAR_PURNW2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BIHAR_CS.ZBR.F_BIHAR_PURNW2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        21	
                    </td>
                    <td>
                        400 kV Purnea-Malda-1	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.F_MALDA_PURNW1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.ZBR.F_MALDA_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        22	
                    </td>
                    <td>
                        400 kV Purnea-Malda-2	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.F_MALDA_PURNW2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.ZBR.F_MALDA_PURNW2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        23	
                    </td>
                    <td>
                        220 kV Bongaigaon-Alipurduar-1	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LD.LD_SALAKATI1.*.MW"] ?>	
                    </td>
                    <td>
                        -
                    </td>
                </tr>
                <tr>
                    <td>
                        24	
                    </td>
                    <td>
                        220 kV Bongaigaon-Alipurduar-2	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LD.LD_SALAKATI2.*.MW"] ?>	
                    </td>
                    <td>
                        -
                    </td>
                </tr>
                <tr>
                    <td>
                        25	
                    </td>
                    <td>
                        220 kV Alipurduar-Birpara-1	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.E_ALIPG_BIRPA1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BIRPA_CS.LINE.E_ALIPG_BIRPA1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        26	
                    </td>
                    <td>
                        220 kV Alipurduar-Birpara-2	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.E_ALIPG_BIRPA2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BIRPA_CS.LINE.E_ALIPG_BIRPA2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        27	
                    </td>
                    <td>
                        220 kV Birpara-Binaguri-1	
                    </td>
                    <td>
                        <?php echo $NewData["BIRPA_CS.LINE.E_BIRPA_SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_BIRPA_SI4001.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        28	
                    </td>
                    <td>
                        220 kV Birpara-Binaguri-2	
                    </td>
                    <td>
                        <?php echo $NewData["BIRPA_CS.LINE.E_BIRPA_SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_BIRPA_SI4001.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        29	
                    </td>
                    <td>
                        220 kV Binaguri-New Jalpaiguri-1	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_NJP___SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        -
                    </td>
                </tr>
                <tr>
                    <td>
                        30	
                    </td>
                    <td>
                        220 kV Binaguri-New Jalpaiguri-2	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_NJP___SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        -
                    </td>
                </tr>
                <tr>
                    <td>
                        31	
                    </td>
                    <td>
                        220 kV Binaguri-Siliguri-1	
                    </td>
                    <td>
                        <?php echo $NewData["SI220_CS.LINE.E_SI220_SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_SI220_SI4001.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        32	
                    </td>
                    <td>
                        220 kV Binaguri-Siliguri-2	
                    </td>
                    <td>
                        <?php echo $NewData["SI220_CS.LINE.E_SI220_SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.LINE.E_SI220_SI4002.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        33	
                    </td>
                    <td>
                        220 kV Siliguri-Kishanganj-1	
                    </td>
                    <td>
                        <?php echo $NewData["SI220_CS.LINE.E_KISHN_SI2201.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.E_KISHN_SI2201.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        34	
                    </td>
                    <td>
                        220 kV Siliguri-Kishanganj-2	
                    </td>
                    <td>
                        <?php echo $NewData["SI220_CS.LINE.E_KISHN_SI2202.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.E_KISHN_SI2202.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        35	
                    </td>
                    <td>
                        220 kV Kishanganj-Dalkhola-1	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.E_DALKH_KISHN1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_KISHN1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        36	
                    </td>
                    <td>
                        220 kV Kishanganj-Dalkhola-2	
                    </td>
                    <td>
                        <?php echo $NewData["KISHN_CS.LINE.E_DALKH_KISHN2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_KISHN2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        37	
                    </td>
                    <td>
                        220 kV Dalkhola-Malda-1	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_MALDA1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.LINE.E_DALKH_MALDA1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        38	
                    </td>
                    <td>
                        220 kV Dalkhola-Malda-2	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_MALDA2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["MALDA_CS.LINE.E_DALKH_MALDA2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        39	
                    </td>
                    <td>
                        220 kV Dalkhola-Purnea-1	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_PURNE1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNE_CS.LINE.E_DALKH_PURNE1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        40	
                    </td>
                    <td>
                        220 kV Dalkhola-Purnea-2	
                    </td>
                    <td>
                        <?php echo $NewData["DALKH_CS.LINE.E_DALKH_PURNE2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNE_CS.LINE.E_DALKH_PURNE2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        41	
                    </td>
                    <td>
                        220 kV Purnea-Purnea New-1	
                    </td>
                    <td>
                        <?php echo $NewData["PURNE_CS.LINE.E_PURNE_PURNW1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.E_PURNE_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        42	
                    </td>
                    <td>
                        220 kV Purnea-Purnea New-2	
                    </td>
                    <td>
                        <?php echo $NewData["PURNE_CS.LINE.E_PURNE_PURNW2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.E_PURNE_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        43	
                    </td>
                    <td>
                        220 kV Purnea New-Begusarai-1	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.E_BEGUS_PURNW1.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BEGUS_BH.LINE.E_BEGUS_PURNW1.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        44	
                    </td>
                    <td>
                        220 kV Purnea New-Begusarai-2	
                    </td>
                    <td>
                        <?php echo $NewData["PURNW_CS.LINE.E_BEGUS_PURNW2.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["BEGUS_BH.LINE.E_BEGUS_PURNW2.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        45
                    </td>
                    <td>
                        400 kV Binaguri-Alipurduar-1	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.ZBR.F_ALIPG_SI4001.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.F_ALIPG_SI4001.*.MW"] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        46	
                    </td>
                    <td>
                        400 kV Binaguri-Alipurduar-2	
                    </td>
                    <td>
                        <?php echo $NewData["ALIPU_CS.LINE.F_ALIPG_SI4002.*.MW"] ?>	
                    </td>
                    <td>
                        <?php echo $NewData["SI400_CS.ZBR.F_ALIPG_SI4002.*.MW"] ?>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    var IsPanelOpen = 1;
    $("#MainDetails").html("<span id='DesktopTitle'>IMPORTANT LINES OF ER </span>DATA FOR ");
    $(document).ready(function () {

        $("#ImpLineOfER").addClass('SelectedView');////to show selected view

    });


    $(window).on("scroll", function () {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            var IsMenuShow = '';
            if ($(window).width() > 991)
            {
                IsMenuShow = $("#DesktopMenu").css("display");
                if (IsMenuShow != 'block')
                {
                    ShowNewMenu();
                }
                $("#DesktopMenu").removeClass("Menu_scroll");
//                $(".box_section").css('opacity', "0.7");
                $(".box_section").css('opacity', "1");
            }
        } else
        {
            IsMenuShow = $("#DesktopMenu").css("display");
//            $("#DesktopMenu").addClass("Menu_scroll");
            if (IsMenuShow == "block")
            {
                $("#DesktopMenu").css('position', 'relative');
                ShowNewMenu();
            }
            $(".box_section").css('opacity', "0.7");
        }

    });

</script>
