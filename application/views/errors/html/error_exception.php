<table style="position: absolute;top: 0;left: 0;" align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" color="black" id="wrapper">
    <tbody>
        <tr valign="top" align="center" style="background-color: #000000;">
            <td width="100%" height="600px" style="padding-top: 40px;">
                <img src="<?php echo Url_ ?>/images/ErrorImage.jpg" border="0" usemap="#Map"><br><br>
                <span style="color: #fff;line-height:24px;"><a href="<?php echo Url_ ?>" style="color: #fff;">Click Here To Go To Dashboard</a><br>Or<br><a onclick="ReportErrorMessage();" style="color: #fff;text-decoration:underline;cursor:pointer;" id="Report_link">Report This Problem</a></span><br>
                <div id="ShowError" style="width: 514px; display: none;"></div>
                <input type="hidden" id="Severity" value="<?php echo get_class($exception); ?>">
                <input type="hidden" id="ErrorMessage" value="<?php echo $message; ?>">
                <input type="hidden" id="FileName" value="<?php echo $exception->getFile(); ?>">
                <input type="hidden" id="LineNo" value="<?php echo $exception->getLine(); ?>">
            </td>
        </tr>
    </tbody>
</table>
<script src="<?php echo Url_ ?>/script/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo Url_ ?>/script/waitMe.js" type="text/javascript"></script>
<link href="<?php echo Url_ ?>/css/waitMe.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
                    $(document).ready(function (e) {
                        $("#ShowError").hide();
                    });

                    function ReportErrorMessage() {
                        //$("#wrapper").waitMe({ effect: 'bounce', text: 'In Progress..', maxSize: '', textPos: 'Vertical', source: '' });
                        $("#Report_link").text("Please wait...");
                        $("#Report_link").css("cursor", "default");
                        $("#Report_link").attr('onclick', 'void(0)');
                        var Url = window.location.href;
                        var ErrorMessage = $("#ErrorMessage").val();
                        var Severity = $("#Severity").val();
                        var PageDetails = $("#FileName").val();
                        var LineNo = $("#LineNo").val();
                        var CurrentDate = '<?php echo date('d M Y h:i A') . " (IST)" ?>';
                        var body = "<html><body><p>Dear Sir/Madam,</p><p>Something went wrong on Generation App server with below mentioned error details.</p><p><b>Error Url : </b>" + Url + "</p><p><b>Error Message: </b>" + ErrorMessage + "</p>  <p><b>Line Number: </b>" + LineNo + "</p>  <p><b>Page Details: </b>" + PageDetails + "</p>   <p><b>Current Timestamp: </b>" + CurrentDate + "</p><p>Thank You !<br/>Generation App Tech Team</p></body></html>";
                        var Subject = "Generation App : Exception occured in App";
                        var RecipientEmailIDs = '<?php echo RecipientEmailIDs_ ?>';
                        var cc = '<?php echo cc_ ?>';
                        var bcc = '<?php echo bcc_ ?>';
                        var aliasName = '<?php echo aliasName_ ?>';
                        var IpAddress = '<?php echo file_get_contents("https://api.ipify.org") ?>';
                        var ReplyTo = '<?php echo ReplyTo_ ?>';


                        $.ajax({
                            type: "GET",
                            url: "http://util.cruxbytes.com/mail-sending-web.aspx?" + encodeURIComponent('si=' + encodeURIComponent('http://localhost/') + '&sub=' + encodeURIComponent(Subject) + '&body=' + encodeURIComponent(body) + '&mail=' + encodeURIComponent(RecipientEmailIDs) + '&Port=' + encodeURIComponent('587') + '&reply=' + encodeURIComponent(ReplyTo) + '&sid=' + encodeURIComponent('') + '&spwd=' + encodeURIComponent('') + '&smtp=' + encodeURIComponent('') + '&path=' + encodeURIComponent('') + '&alias=' + encodeURIComponent(aliasName) + '&bcc=' + encodeURIComponent(bcc) + '&cc=' + encodeURIComponent(cc) + '&sip=' + encodeURIComponent(IpAddress)),
                            //data: 'data',
                            dataType: "json",
                            success: OnSuccess_,
                            error: OnErrorCall_
                        });

                        function OnSuccess_(response) {
                            if (response == "1 mail-sending-web.aspx running successfully") {
                                // $("#wrapper").waitMe('hide');
                                $("#Report_link").text("Report This Problem");
                                $("#ShowError").css("background-color", "#dff0d8");
                                $("#ShowError").css("color", "#3c763d");
                                $("#ShowError").css("line-height", "35px");
                                $("#ShowError").show();
                                $("#ShowError").fadeOut(7000);
                                $("#ShowError").html("Error reported successfully and ticket has been send to Tech-support Team.");
                                $("#Report_link").css("cursor", "default");
                                $("#Report_link").attr('onclick', 'void(0)');

                            } else {
                                //$("#wrapper").waitMe('hide');
                                $("#Report_link").text("Report This Problem");
                                $("#ShowError").css("background-color", "#f2dede");
                                $("#ShowError").css("color", "#a94442");
                                $("#ShowError").css("line-height", "35px");
                                $("#ShowError").show();
                                $("#ShowError").fadeOut(7000);
                                $("#Report_link").css("cursor", "default");
                                $("#Report_link").attr('onclick', 'ReportErrorMessage()');
                                $("#ShowError").html("Unknown error occured . Please try again later");
                            }
                        }

                        function OnErrorCall_(response) {
                            if (response.status == "200" && response.statusText == 'OK')
                            {
                                $("#Report_link").text("Report This Problem");
                                $("#ShowError").css("background-color", "#dff0d8");
                                $("#ShowError").css("color", "#3c763d");
                                $("#ShowError").css("line-height", "35px");
                                $("#ShowError").show();
                                $("#ShowError").fadeOut(7000);
                                $("#ShowError").html("Error reported successfully and ticket has been send to Tech-support Team.");
                                $("#Report_link").css("cursor", "default");
                                $("#Report_link").attr('onclick', 'void(0)');

                            } else
                            {
                                $("#Report_link").text("Report This Problem");
                                $("#ShowError").css("background-color", "#f2dede");
                                $("#ShowError").css("color", "#a94442");
                                $("#ShowError").show();
                                $("#ShowError").fadeOut(7000);
                                $("#ShowError").css("line-height", "35px");
                                $("#Report_link").css("cursor", "pointer");
                                $("#Report_link").attr('onclick', 'ReportErrorMessage()');
                                $("#ShowError").html("Unknown error occured . Please try again later");

                            }


                        }
                    }
</script>

<style type="text/css">
    #wrapper
    {
        position: absolute;
        top: 0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
    }


</style>