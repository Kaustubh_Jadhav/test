/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//using Zing Chart
function setChartValue(list, Todaydatalist, Yesterdaydatalist, yaxisMax, yaxisMin, Today, Yesterday, $title)
{

    //  var str = [[10, [2031.34, 4000]], [20, [5001.34, 1023]], [30, [8001.34, 2023]]];
    //var str = str.substring(0, str.length - 1);
    // console.log(str);
    var myConfig = {
        "title": {// Main Title
            "text": $title
        },
        "legend": {
            "layout": "1x2", //row x column
            "x": "30%", //x axis position
            "y": "7%", // y axis position
        },
        gui: {// right click pop up property setting [enabled: all/ none]
            "behaviors": [
                {
                    "id": "Reload",
                    "text": "Reload Chart",
                    "enabled": "none"
                },
                {
                    "id": "SaveAsImagePNG",
                    "text": "View As PNG",
                    "enabled": "none"
                },
                {
                    "id": "DownloadPDF",
                    "text": "Export PDF",
                    "enabled": "none"
                },
                {
                    "id": "DownloadSVG",
                    "text": "Export SVG",
                    "enabled": "none"
                },
                {
                    "id": "Print",
                    "enabled": "none"
                },
                {
                    "id": "LogScale",
                    "enabled": "none"
                },
                {
                    "id": "ViewSource",
                    "enabled": "none"
                },
                {
                    "id": "FullScreen",
                    "text": "Full Size",
                    "enabled": "none"
                },
            ],
        },
        "type": "line", //chart Type

        scaleX: {

            values: list, //Label value bind dynamic

            item: {//label style y axis
                fontSize: 10,
                "font-color": "black"
            },

            zooming: true,
            zoomToValues: [0, (list.length - 1)], //default zoom value
            guide: {
                alpha: 1,
                lineStyle: 'solid',
                visible: true
            },
            minorTicks: 7,
            minorGuide: {
                alpha: 0.7,
                lineStyle: 'dotted'//backgroud line
            },

        },
        scrollX: {

        },
        preview: {},
        crosshairX: {
            alpha: 0.3,
            //  lineWidth: '100%',
            plotLabel: {
                //   text: '%v : %t', //tooltip format

                borderRadius: 3,
                decimals: 2,
                multiple: true,
                placement: 'node-top'
            },
            scaleLabel: {
                borderRadius: 3,
                //  color:red,
            }
        },
        scaleY: {
            item: {//label style y axis
                fontSize: 10,
                "font-color": "black"
            },
            values: yaxisMin + ':' + yaxisMax + ':1', },
        plot: {
            backgroundColor: '#fff',
            lineColor: '#2196F3',
            lineWidth: 1,
            "aspect": "spline",
            marker: {
//                    visible: false
                size: 3
            },
            tooltip: {
                visible: false
            }
        },
        "series": [
            {"values": Todaydatalist,
                "line-color": "#00cc99",
                "text": Today,
                "legend-text": Today
            },

            {"values": Yesterdaydatalist,
                "text": Yesterday,
                "legend-text": Yesterday
            }

        ]

    };

    zingchart.render({
        id: 'dataStuctureChart',
        data: myConfig,
        height: 400,
        width: '100%'
    });

}

function resetZoom() {
    window.myLine.resetZoom()
}

//using Chart.js
function setJsChartValue(list, Todaydatalist, Yesterdaydatalist, yaxisMax, yaxisMin, Today, Yesterday, $title)
{

    var timeFormat = 'MM/DD/YYYY HH:mm';
    //list = [0, 4, 8, 12, 16, 20, 26];
    alert(timeFormat);
    return config = {
        "title": {// Main Title
            "text": $title
        },
        "legend": {
            "layout": "1x2", //row x column
            "x": "30%", //x axis position
            "y": "7%", // y axis position
        },
        type: 'line', //chart Type
        preview: {},
        data: {

            labels: list, // Date Objects
            datasets: [{
                    label: "My First dataset",
                    data: Todaydatalist,
                    fill: false,
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: "Chart.js Time Scale"
            },
            scales: {
                xAxes: [{
                        time: {
                            format: timeFormat,
                            tooltipFormat: 'll HH:mm'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        },
                        ticks: {
                            maxRotation: 0
                        }
                    }, ],
                yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'value'
                        }
                    }]
            },
            zoom: {
                enabled: true,
                drag: false,
                mode: "x",
                limits: {
                    max: 10000,
                    min: 10000
                }
            },
            pan: {
                enabled: true,
                mode: "x",
                speed: 10,
                threshold: 10
            },
            onClick: function (e) {
                //alert(e.type);
                resetZoom();
            },
        }
    }


}



function resetZoom() {
    window.myLine.resetZoom();
}
//based on:
//https://github.com/chartjs/chartjs-plugin-zoom/blob/master/samples/zoom-time.html
var timeFormat = "HH:mm";
function randomScalingFactor() {
    return Math.round(Math.random() * 100 * (Math.random() > 0.5 ? -1 : 1));
}
function randomColorFactor() {
    return Math.round(Math.random() * 255);
}
function randomColor(opacity) {
    return (
            "rgba(" +
            randomColorFactor() +
            "," +
            randomColorFactor() +
            "," +
            randomColorFactor() +
            "," +
            (opacity || ".3") +
            ")"
            );
}

function resetZoom() {
    window.myLine.resetZoom();
}
function testJschart(list, Todaydatalist)
{

    var List2 = [];
    var TodayList = [];
    for (var i = 0; i < 100; i++)
    {
        List2.push(i);
    }
    for (var j = 0; j < 100; j++)
    {
        TodayList.push(j);
    }
    return config = {
        type: "line",
        data: {
            labels: list, // Date Objects
            datasets: [
                {
                    label: "My First dataset",
                    data: Todaydatalist,
                    fill: false,
                    // borderDash: [5, 5]
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: "Chart.js Time Scale"
            },
            scales: {
                xAxes: [
                    {
                        //  type: "time",
                        time: {
                            format: timeFormat,
                            // round: 'day'
                            tooltipFormat: "ll HH:mm"
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "x axis"
                        },
                        ticks: {
                            maxRotation: 0
                        }
                    }
                ],
                yAxes: [
                    {
                        scaleLabel: {
                            display: true,
                            labelString: "y axis"
                        }
                    }
                ]
            },
            zoom: {
                enabled: true,
                drag: false,
                mode: "x",
                limits: {
                    max: 10,
                    min: 0.5
                }
            },
            pan: {
                enabled: true,
                mode: "x",
                speed: 10,
                threshold: 10
            },
            onClick: function (e) {
                //alert(e.type);
                resetZoom();
            },
        }
    };


}