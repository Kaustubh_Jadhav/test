﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Puraniks;
using System.Text.RegularExpressions;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    DataBase12 obj;
    
    
    string Email_Registration_From = "Cruxbytes";
    string MAP_Key = "2010";
    string name, contactno, emailid = "";
    int q1, q2, q3, q4;
    string q1yes, q2yes, q3yes, q4yes;
    string q5 = "";
     int FeedBackID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillProjectDropDown();
            ddrproject.Focus();
        }
        else
            txtname.Focus();

        ltr.Text = "Developed By " + "<a href='http://www.cruxbytes.com/' class='a-link-text' target='_blank'>CruxBytes</a>";

        if (Request.QueryString["i"] != null)
        {
            if (Request.QueryString["i"] == "1")
                lblmsg.Text = "Thank you for your feedback.";
        }
    }

    public void clear()
    {
        txtname.Text = "";
        txtemailID.Text = "";
        txtcontactno.Text = "";
        txtq5.Text = "";
        hdfq1yes.Value = "";
        hdfq2yes.Value = "";
        hdfq3yes.Value = "";
        hdfq4yes.Value = "";
     
    }
    protected void FillProjectDropDown()
    {
        try
        {
            DataSet ds = new DataSet();
            obj = new DataBase12();
            ds = obj.GetProjectList();
            ddrproject.DataSource = ds;
            ddrproject.DataTextField = "Projectname";
            ddrproject.DataValueField = "ProjectID";
            ddrproject.DataBind();
         
            ddrproject.Items.Insert(0, new ListItem("--Select--", "0"));
            ddrproject.SelectedIndex = 0;
           
            //ddrproject.Items.Insert(1, new ListItem("Rumah Bali", "1"));
            //ddrproject.Items.Insert(2, new ListItem("Puranik City", "2"));
            //ddrproject.Items.Insert(3, new ListItem("Puranik Home Town", "3"));
            //ddrproject.Items.Insert(2, new ListItem("Capitol", "4"));
            //ddrproject.Items.Insert(3, new ListItem("Aldea Espanola", "5"));
        

        }
        catch (Exception ex)
        {
            lblmsg.Text = ex.Message;
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (IsValidSave())
            {
                name = txtname.Text;
                hdfname.Value = name.ToString();
                if (string.IsNullOrEmpty(txtcontactno.Text))
                {
                    contactno = "-";
                }
                else
                {
                    contactno = txtcontactno.Text;
                }
               
                
                    emailid = txtemailID.Text;
               

                if (string.IsNullOrEmpty(hdfq1yes.Value))
                {
                    q1 = 0;

                }
                else
                {
                    q1 = Convert.ToInt32(hdfq1yes.Value);
                }

                if (q1 == 1)
                {
                    q1yes = "Yes";
                }
                else if (q1 == 2)
                {
                    q1yes = "No";
                }
                else
                {
                    q1yes = "-";
                }
                if (string.IsNullOrEmpty(hdfq2yes.Value))
                {
                    q2 = 0;
                }
                else
                {
                    q2 = Convert.ToInt32(hdfq2yes.Value);
                }
                if (q2 == 1)
                {
                    q2yes = "Yes";
                }
                else if (q2 == 2)
                {
                    q2yes = "No";
                }
                else
                {
                    q2yes = "-";
                }
                if (string.IsNullOrEmpty(hdfq3yes.Value))
                {
                    q3 = 0;
                }
                else
                {
                    q3 = Convert.ToInt32(hdfq3yes.Value);
                }
                if (q3 == 1)
                {
                    q3yes = "Yes";
                }
                else if (q3 == 2)
                {
                    q3yes = "No";
                }
                else
                {
                    q3yes = "-";
                }
                if (string.IsNullOrEmpty(hdfq4yes.Value))
                {
                    q4 = 0;
                }
                else
                {
                    q4 = Convert.ToInt32(hdfq4yes.Value);
                }
                if (q4 == 1)
                {
                    q4yes = "Yes";
                }
                else if (q4 == 2)
                {
                    q4yes = "No";
                }
                else
                {
                    q4yes = "-";
                }
                if (string.IsNullOrEmpty(txtq5.Text))
                {
                    q5 = "-";
                }
                else
                {
                    q5 = txtq5.Text;
                }
                int result = 0;
                int projectID =Convert.ToInt32(ddrproject.SelectedIndex.ToString());
                if (ddrproject.SelectedIndex > 0)
                {
                  
                    projectID = Convert.ToInt32(ddrproject.SelectedItem.Value);
                  

                }

                obj = new DataBase12();
                result = obj.Save_FeedBackDetails(FeedBackID, hdfname.Value, contactno, emailid, q1, q2, q3, q4, q5,projectID);

                if (result > 0)
                {

                    int n = SendAlertToAdmin(hdfname.Value);
                    if (n > 0)
                    {
                        lblmsg.Text = "Thank you for your feedback";
                        Response.Redirect("customer-feedback-form.aspx?i=1", false);

                    }
                    else
                    {
                        // lblmsg.Text = "Email could not be sent due to technical failure. Please contact the administrator .";
                    }

                    if (string.IsNullOrEmpty(emailid))
                    {

                    }
                    else
                    {
                        int p = SendAlertToConsumer(hdfname.Value, emailid);
                        if (p > 0)
                        {
                            lblmsg.Text = "Thank you for your feedback";

                        }
                        else
                        {
                            //lblmsg.Text = "Email could not be sent due to technical failure. Please contact the administrator .";
                        }
                    }
                    clear();

                }

            }
        }
        catch (Exception ex)
        { }

    }
    private bool IsValidSave()
    {
        bool flag = true;
        if (txtname.Text == "")
        {
            lblmsg.Text = "Please enter name";
            flag = false;
        }
        if (txtcontactno.Text == "")
        {
            lblmsg.Text = "Please enter contact number";
            flag = false;
        }
        if (ddrproject.SelectedIndex <= 0)
        {
            lblmsg.Text = "Please select project";
            flag = false;
        }
        if (txtq5.Text == "")
        {
            lblmsg.Text = "Please provide your comments";
            flag = false;
        }
        return flag;

    }

    

    public int SendAlertToAdmin(string name)
    {
        try
        {
            int nEmailResult = 0;
            SendEmail objsendemail = new SendEmail();

            SystemAlerts obj = new SystemAlerts();

            string webSiteCommonUrl = System.Configuration.ConfigurationSettings.AppSettings["WEBSITECOMMONURL"];
            string EmailID = System.Configuration.ConfigurationSettings.AppSettings["EMAIL_BAGENERAL"];

            obj.EmailID = EmailID.ToString();
            if (obj.EmailID != null && obj.EmailID.Trim() != "")
            {

            
               
                obj.Sender = Email_Registration_From.ToString();
                obj.Subject = "Feedback Received";

                obj.Body = "<html>" +
                                "<body>" +
                                "<p>Dear ADMIN, </p>" +
                                        "<p>A feedback has been received with the following details:</p> "+ 
                                        "<p><b>Name</b> " + name.ToUpper()+
                                        "<br /><b>Contact No: </b>" + contactno +
                                        "<br /><b>Email ID:</b> " + emailid.ToString()+
                                        "<br /><b>Q1:</b> " + q1yes +
                                        "<br /><b>Q2:</b> " + q2yes +
                                        "<br /><b>Q3:</b> " + q3yes +
                                        "<br /><b>Q4:</b> " + q4yes +
                                        "<br /><b>Q5:</b> " + txtq5.Text +
                                        "<p>This is system generated email. Please do not reply.</p>" +
                                        "<p>Warm Regards,<br />" +
                                        "<a href='www.cruxbytes.com' target='_blank'>CruxBytes</a></p>" +
                                        "<p></p>" +
                                "</body>" +
                                "</html>";


               nEmailResult= objsendemail.SendMail(obj);
               lblmsg.Text = obj.msg;
               
            }
            return nEmailResult;
        }
        catch (Exception ex)
        {

            return 0;
           
        }


    }

    public int SendAlertToConsumer(string name, string EmailID)
    {
        try
        {
            SendEmail objsendemail = new SendEmail();

            SystemAlerts obj = new SystemAlerts();
            int nEmailResult = 0;
           
            string webSiteCommonUrl = System.Configuration.ConfigurationSettings.AppSettings["WEBSITECOMMONURL"];
          //  string EmailID = System.Configuration.ConfigurationSettings.AppSettings["EMAIL_BAGENERAL"];
            obj.EmailID = EmailID.ToString();
            if (obj.EmailID != null && obj.EmailID.Trim() != "")
            {

                obj.Sender = Email_Registration_From.ToString();
                obj.Subject = "Auto Response From Puranik";

                obj.Body = "<html>" +
                                "<body>" +
                                "<p>Dear " + name.ToUpper() + ",</p>" +
                                        "<p>Thank you very much for providing us with your feedback. Your inputs are valuable to us!</p>" +
                                        "<p></p>" +
                                        "<p>This is system generated email. Please do not reply.</p>" +
                                        "<p>Warm Regards,<br />" +
                                        "<a href='www.puranikbuilders.com' target='_blank'>Puranik Builder Pvt Ltd.</a></p>" +
                                        "<p></p>" +
                                "</body>" +
                                "</html>";


                 nEmailResult=objsendemail.SendMail(obj);
                 lblmsg.Text = obj.msg;
            }

            return nEmailResult;
        }
        catch (Exception ex)
        {

            return 0;
           
        }


    }
   
}
