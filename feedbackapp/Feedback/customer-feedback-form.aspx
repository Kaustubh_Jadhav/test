﻿

<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="customer-feedback-form.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Customer Feedback Form - Puranik Builders Pvt Ltd.</title>
   

   <script type="text/javascript" src="JS/Common_Validation_n10.js"></script>
<script language="javascript" type="text/javascript">
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }
    function valid()
     {
         var msg = document.getElementById('<%=lblmsg.ClientID %>');
       
         var txtUserName = document.getElementById('<%=txtname.ClientID%>');
         var email = document.getElementById('txtemailID');
         var ddrproject = document.getElementById('ddrproject');
         var contactno = document.getElementById('txtcontactno');
         var comments = document.getElementById('<%=txtq5.ClientID%>');
         
         if (ddrproject.value == 0) {
             alert('Please select project');
             ddrproject.focus();
             return false;
         }
        
        if (txtUserName.value == "") 
        {
            alert("Please enter your name");
            txtUserName.focus();
            return false;

        }

        if (email.value != "")
            {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus;
                return false;
            }





        }
        if (contactno.value == "") {
            alert("Please enter your contact number");
            contactno.focus();
            return false;

        }

        if (comments.value == "") {
            alert("Please provide your comments");
            comments.focus();
            return false;

        }
        
alert("Thank you for your feedback");
            return true;					
	    
    }
    function btnq1yes(ImageID,Optvalue) {
        var hdfq1yes = document.getElementById('<%=hdfq1yes.ClientID %>');
        var btnq1yes = document.getElementById('<%=btnq1yes.ClientID %>');
        var btnq1no = document.getElementById('<%=btnq1no.ClientID %>');

        var img = document.getElementById(ImageID.id);
        hdfq1yes.value = Optvalue;
        if (Optvalue == 1) {
            btnq1yes.src = "Images/q1yes-hover.png";
            btnq1no.src = "Images/q1no.png";

        }
        else if (Optvalue == 2) {

            btnq1yes.src = "Images/q1yes.png";
            btnq1no.src = "Images/q1no-hover.png";
        }
        else {
            Optvalue = 0;
        }
        
       
      
        return false;
    }


    function btnq2yes(ImageID, Optvalue) {
        var hdfq2yes = document.getElementById('<%=hdfq2yes.ClientID %>');
        var btnq2yes = document.getElementById('<%=btnq2yes.ClientID %>');
        var btnq2no = document.getElementById('<%=btnq2no.ClientID %>');

        var img = document.getElementById(ImageID.id);
        hdfq2yes.value = Optvalue;
        if (Optvalue == 1) {
            btnq2yes.src = "Images/q2yes-hover.png";
            btnq2no.src = "Images/q2no.png";

        }
        else if (Optvalue == 2) {

        btnq2yes.src = "Images/q2yes.png";
        btnq2no.src = "Images/q2no-hover.png";
        }
        else {
            Optvalue = 0;
        }



        return false;
    }




    function btnq3yes(ImageID, Optvalue) {
        var hdfq3yes = document.getElementById('<%=hdfq3yes.ClientID %>');
        var btnq3yes = document.getElementById('<%=btnq3yes.ClientID %>');
        var btnq3no = document.getElementById('<%=btnq3no.ClientID %>');

        var img = document.getElementById(ImageID.id);
        hdfq3yes.value = Optvalue;
        if (Optvalue == 1) {
            btnq3yes.src = "Images/q3yes-hover.png";
            btnq3no.src = "Images/q3no.png";

        }
        else if (Optvalue == 2) {

        btnq3yes.src = "Images/q3yes.png";
        btnq3no.src = "Images/q3no-hover.png";
        }
        else {
            Optvalue = 0;
        }



        return false;
    }




    function btnq4yes(ImageID, Optvalue) {
        var hdfq4yes = document.getElementById('<%=hdfq4yes.ClientID %>');
        var btnq4yes = document.getElementById('<%=btnq4yes.ClientID %>');
        var btnq4no = document.getElementById('<%=btnq4no.ClientID %>');

        var img = document.getElementById(ImageID.id);
        hdfq4yes.value = Optvalue;
        if (Optvalue == 1) {
            btnq4yes.src = "Images/q4yes-hover.png";
            btnq4no.src = "Images/q4no.png";

        }
        else if (Optvalue == 2) {

        btnq4yes.src = "Images/q4yes.png";
        btnq4no.src = "Images/q4no-hover.png";
        }
        else {
            Optvalue = 0;
        }



        return false;
    }
    function clear() {

        var hdfq1yes = document.getElementById('<%=hdfq1yes.ClientID %>');
        var hdfq2yes = document.getElementById('<%=hdfq2yes.ClientID %>');
        var hdfq3yes = document.getElementById('<%=hdfq3yes.ClientID %>');
        var hdfq4yes = document.getElementById('<%=hdfq4yes.ClientID %>');



        hdfq1yes.value = "";
        hdfq2yes.value = "";
        hdfq3yes.value = "";
        hdfq4yes.value = "";
        document.getElementById('<%=txtname.ClientID %>') = "";
        document.getElementById('<%=txtcontactno.ClientID %>') = "";
        document.getElementById('<%=txtemailID.ClientID %>') = "";
        document.getElementById('<%=txtq5.ClientID %>') = "";
        return false;
    }
    
    
</script>

<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(1); }
</script>
       
      <link  href="CSS/Stylesheet.css" type="text/css" rel="Stylesheet" />
    
</head>
<body  onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
     <form id="form1" runat="server">
      <asp:HiddenField ID="hdfq1yes" runat="server" />
   <asp:HiddenField ID="hdfq1no" runat="server" />
  
    <asp:HiddenField ID="hdfq2yes" runat="server" />
    <asp:HiddenField ID="hdfq2no" runat="server" />
  
    <asp:HiddenField ID="hdfq3yes" runat="server" />
  
  <asp:HiddenField ID="hdfq3no" runat="server" />
  
    <asp:HiddenField ID="hdfq4yes" runat="server" />
     <asp:HiddenField ID="hdfq4no" runat="server" />
  <asp:HiddenField ID="hdfname" runat="server" />
 
  
    <div style="clear:both;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
    
    
    <td><img src="Images/logo.jpg" alt="Logo" /></td>
    </tr>
    <tr><td><div id="page-heading">Your Feedback Please</div></td></tr>
   <tr><td>
       <asp:Label ID="lblmsg" runat="server" Text="" CssClass="text-orange"></asp:Label></td></tr>
    <tr>
    <td>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
   <tr>
<td style="width:100%;">
<table  width="100%" cellpadding="0" cellspacing="0" border="0">
   <tr>
    <td style="width:120px;height:30px;">
        <asp:Label ID="lblproject" runat="server" Text="Project" CssClass="text"></asp:Label><span class="text-orange" style="padding-left:33px;">*</span>
        </td><td>
            <asp:DropDownList ID="ddrproject" runat="server" Width="200px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
    <td style="width:120px;height:30px;">
    
    
        <asp:Label ID="lblname" runat="server" Text="Name" CssClass="text"></asp:Label><span class="text-orange" style="padding-left:40px;">*</span>
        </td><td>
            <asp:TextBox ID="txtname" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
        </td>
    </tr>
       <tr>
    <td style="width:120px;height:30px;">
        <asp:Label ID="lblemailid" runat="server" Text="Email ID" CssClass="text"></asp:Label>
        </td><td>
            <asp:TextBox ID="txtemailID" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
           
        </td>
    </tr>
    <tr>
    <td style="width:120px;height:30px;">
        <asp:Label ID="lblcontactno" runat="server" Text="Contact No" CssClass="text"></asp:Label><span class="text-orange" style="padding-left:10px;">*</span>
        </td><td>
            <asp:TextBox ID="txtcontactno" runat="server" MaxLength="10" Width="100px" onkeypress="return onlyNumbers();"></asp:TextBox>
             
        </td>
    </tr>
  
    
    </table>
       </td>
       </tr>
    
    
       <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
    <td colspan="2"><span class="text">Dear Visitor</span></td>
    </tr>
    <tr>
    <td colspan="2"><span class="text">Thank you for visiting us and showing interest in our Projects. Kindly help us to improve our processes by giving your valuable feedback</span></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
     <tr>
     <td>
         <span  class="text">1)&nbsp; Did you like our Project offerings?</span>
     
      
     </td>
    </tr>
    <tr><td>
<%--   <div  class="btn1y" width="45px" height="45px">
   <a onclick="return btnq1yes(this,1);" id="btnq1yes" runat="server"></a>
    </div> --%>
     <asp:ImageButton ID="btnq1yes" runat="server"  ToolTip="Great"  
            ImageUrl="~/Images/q1yes.png"   OnClientClick="return btnq1yes(this,1);" />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     
            <asp:ImageButton ID="btnq1no" runat="server"  ToolTip="Nothing Great" 
                ImageUrl="~/Images/q1no.png"     OnClientClick="return btnq1yes(this,2);" />
    </td>
    </tr>
      <tr><td>&nbsp;</td></tr>
      <tr>
     <td>
         <span class="text">2)&nbsp;Did you like the ambiance of the Sales office?</span>
     
      
     </td>
    </tr>
    <tr><td>
     
        <asp:ImageButton ID="btnq2yes" runat="server"  ToolTip="Great"
            ImageUrl="~/Images/q2yes.png"    OnClientClick="return btnq2yes(this,1);"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="btnq2no" runat="server"  ToolTip="Hmmm" 
                ImageUrl="~/Images/q2no.png"    OnClientClick="return btnq2yes(this,2);"  />
    </td>
    </tr>
      <tr><td>&nbsp;</td></tr>
      <tr>
     <td>
         <span class="text">3)&nbsp;Were all your queries regarding the Project answered to your satisfaction?</span>
     
      
     </td>
    </tr>
    <tr><td>
    
        <asp:ImageButton ID="btnq3yes" runat="server"  ToolTip="Great information" 
            ImageUrl="~/Images/q3yes.png"    OnClientClick="return btnq3yes(this,1);"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="btnq3no" runat="server"  ToolTip="Not really" 
                ImageUrl="~/Images/q3no.png"    OnClientClick="return btnq3yes(this,2);" />
    </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
      <tr>
     <td>
         <span class="text">4)&nbsp; Would you recommend our Project to your Friends and Colleagues?</span>
     
      
     </td>
    </tr>
    
    <tr><td>
    
        <asp:ImageButton ID="btnq4yes" runat="server"  ToolTip="Yes"
            ImageUrl="~/Images/q4yes.png"   OnClientClick="return btnq4yes(this,1);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="btnq4no" runat="server"  ToolTip="No" 
                ImageUrl="~/Images/q4no.png"    OnClientClick="return btnq4yes(this,2);" />
    </td>
    </tr>
      <tr><td>&nbsp;</td></tr>
    <tr><td><span class="text">5)&nbsp;Anything that we need to improve upon?(Product/service)</span><span class="text-orange" style="padding-left:3px;">*</span></td></tr>
    <tr><td>
        <asp:TextBox ID="txtq5" runat="server"  MaxLength="100"  TextMode="MultiLine"  onKeyUp="return taCount(this,'myCounter','100')"
            Rows="3" Width="400"></asp:TextBox></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>
                <asp:Button ID="btnsubmit" runat="server" Text="Submit Feedback"   CssClass="Button-bg"
                    onclick="btnsubmit_Click" OnClientClick="return valid();" Height="50px"/></td></tr>
    </table>
    </td>
    </tr>
    </table>
    
    </td>
    
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td  class="line" valign="bottom">&nbsp;</td></tr>
  <%--  <tr><td align="left" style="padding-top:5px;"><span class="footer-text1"><asp:literal ID="ltrdevelpedBy" runat="server" Text="Developed By"></asp:literal>  <a href="http://www.cruxbytes.com/" class="a-link-text" target="_blank" style="text-decoration:none;"><asp:literal ID="ltrCruxBytes" runat="server" Text="CruxBytes"></asp:literal></a></span> </td></tr>
 --%> 
 
   <tr><td align="left" style="padding-top:5px;"><span class="footer-text1">  <asp:literal ID="ltr" runat="server" Text=""></asp:literal> </span> </td></tr>
 
   </table>
   
    </div>
    </form>
</body>
</html>
