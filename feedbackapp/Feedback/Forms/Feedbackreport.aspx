﻿<%@ Page Title="Feedback Report - Puranik Builders Pvt Ltd." Language="C#" MasterPageFile="~/Master/Main.master" AutoEventWireup="true" CodeFile="Feedbackreport.aspx.cs" Inherits="Forms_Feedbackreport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script language="javascript" type="text/javascript">

//        function StartProgressBar() {
//            var myExtender = $find('ProgressBarModalPopupExtender');
//            myExtender.show();
//            return true;
//        }
     
        function validateSubmit() {

            var txtFrom = document.getElementById('<%=txtFromdt.ClientID %>');
            var txtTo = document.getElementById('<%=txtTodt.ClientID %>');

            if (!empty_chk(txtFrom, txtFrom.value, "Please select from date.")) {
                isContact = 0;
                return false;
            }
            if (!empty_chk(txtTo, txtTo.value, "Please Select to date.")) {
                isContact = 0;
                return false;
            }
            if (txtFrom.value.trim() != '' && txtTo.value.trim() != '') {
                if (txtFrom.value.trim() != txtTo.value.trim()) {
                    if (validateBetweenDates(txtFrom.value, txtTo.value)) {
                        alert("To Date must be greater than From Date")
                        return false;
                    }
                  
                }
            }
//            StartProgressBar();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Release">
    </asp:ToolkitScriptManager>

   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            
              
                            <ContentTemplate>
           <%--  <asp:ModalPopupExtender ID="ProgressBarModalPopupExtender" runat="server" BackgroundCssClass="ModalBackground_2" behaviorID="ProgressBarModalPopupExtender"
                     TargetControlID="hiddenField" PopupControlID="Panel2" />
            <asp:Panel ID="Panel2" runat="server" Style="display: none; background-color: #FFFFFF;">
                 <img src="../Images/green-loader.gif" alt="Loader" />
                 </asp:Panel>--%>
            <asp:HiddenField ID="hiddenField" runat="server" />

              <div id="page-heading"> 
              <asp:Label id="lblPage_Heading" runat="server" Text="Feedback Report"> </asp:Label> 
              </div>
              
                <div style="clear:both;">
               
            
                
              
                
                 <table width="100%">
                  
                  <tr><td align="left"> <asp:Label ID="lblUserRecored" runat="server" CssClass="text-orange" 
                                Visible="true"></asp:Label>
                                </td></tr>
                                    
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                 <td align="left" height="24px" width="100px">
                                          
                                            <asp:Label ID="Label1" runat="server" Text="Select Project" CssClass="textbold"></asp:Label>
                                        </td>
                                        <td width="220px">
            <asp:DropDownList ID="ddrproject" runat="server" Width="180px">
            </asp:DropDownList>
        </td>
                                                    <td align="left" height="24px" width="90px">
                                                        
                                                        <asp:Label ID="lbl" runat="server" Text="From Date" CssClass="textbold" ></asp:Label>
                                                    </td>
                                                    <td align="left" height="24px" width="150px">
                                                        <asp:TextBox ID="txtFromdt" runat="server" 
                                                            MaxLength="10" ondrop="return false;" onkeypress="return false;" 
                                                            onpaste="return false;" Width="90px"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                            TargetControlID="txtFromdt">
                                                        </asp:CalendarExtender>
                                                        <a class="tooltip" title="Click inside the box to select the date">
                                                        <img border="0" height="16" src="../images/help_icon.gif" width="12" /></a>
                                                    </td>
                                                    <td width="70px">
                                                       
                                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="textbold"></asp:Label>
                                                    </td>
                                                    <td width="120px">
                                                        <asp:TextBox ID="txtTodt" runat="server" 
                                                            MaxLength="10" ondrop="return false;" onkeypress="return false;" 
                                                            onpaste="return false;" Width="90px"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" 
                                                            TargetControlID="txtTodt">
                                                        </asp:CalendarExtender>
                                                        <a class="tooltip" title="Click inside the box to select the date">
                                                        <img border="0" height="16" src="../images/help_icon.gif" /></a>
                                                    </td>
                                                   
                                                    <td>
                                                        <asp:Button ID="btnsubmit" runat="server" CssClass="Button-bg" Height="24px" 
                                                            OnClick="btnsubmit_Click" Text="Show Report" style="height: 30px" OnClientClick="return validateSubmit();"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                          
                                                
                                              <asp:Panel ID="pnlshow" runat="server">
                                                <asp:GridView ID="gvReport" runat="server" AllowPaging="true" 
                                                    AlternatingRowStyle-CssClass="gridaltitem" AutoGenerateColumns="false" 
                                                    CssClass="grid" 
                                                    GridLines="Horizontal" HeaderStyle-CssClass="gridheadertable1" 
                                                    
                                                    PageSize="20" 
                                                    RowStyle-CssClass="griditem" 
                                                    onpageindexchanging="gvReport_PageIndexChanging">
                                                    <Columns>
                                                     <asp:TemplateField HeaderText="Project" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "projectname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblname" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="EmaildID" HeaderText="Email ID" 
                                                            ItemStyle-Width="18%" ItemStyle-HorizontalAlign="Center"  />
                                                     
                                                        
                                                        <asp:TemplateField HeaderText="Contact No" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSubject" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "ContactNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                           
                                                        <asp:TemplateField HeaderText="Q1" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblq1" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "q1") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
                                                        <asp:TemplateField HeaderText="Q2" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblq2" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "q2") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Q3" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblq2" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "q3") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Q4" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblq4" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "q4") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Q5" ItemStyle-HorizontalAlign="Center" 
                                                            ItemStyle-Width="24%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblq5" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "q5") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center"  HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCreatedon" runat="server" 
                                                                    Text='<%# DataBinder.Eval(Container.DataItem, "Createdon") %>'></asp:Label>
                                                            </ItemTemplate>
                                                          
                                                        </asp:TemplateField>
                                                    </Columns>
                                                   
                                                    <PagerStyle BackColor="White" CssClass="gridPagerStyle" ForeColor="Black" 
                                                        HorizontalAlign="Center" />
                                                </asp:GridView>
                                                </asp:Panel>
                                            
                                        </td>
                                    </tr>
               
                    
                     </table>
                        

       
                                                             
                    <asp:HiddenField ID="hdffromdate" runat="server" />     
       <asp:HiddenField ID="hdftodate" runat="server" />                      
            <asp:HiddenField ID="hdfprojectid" runat="server" />                                    
  <%--<asp:ModalPopupExtender BackgroundCssClass="modalBackground" DynamicServicePath="" runat="server" PopupControlID="Pnlinterprete" Enabled="True" id="MPE2"  behaviorID="MPE2" TargetControlID="HiddenField1"></asp:ModalPopupExtender>
    <asp:HiddenField ID="HiddenField1" runat="server"  />    --%>
                               </ContentTemplate>
                        </asp:UpdatePanel>
                    <br/>
            </div>
</asp:Content>

