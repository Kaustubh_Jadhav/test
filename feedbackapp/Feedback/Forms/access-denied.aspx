﻿<%@ Page Title="Puranik Builders Pvt Ltd." Language="C#" MasterPageFile="~/Master/General.master" AutoEventWireup="true" CodeFile="Access-Denied.aspx.cs" Inherits="Forms_access_denied" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Release">
                                        </asp:ToolkitScriptManager>
                                        <asp:UpdatePanel ID="pnlAutosave" runat="server">
                        <ContentTemplate>
        <asp:Timer ID="AutoRefreshTimer" runat="server" OnTick="AutoRefreshTimer_Tick" Interval="5000">
                            </asp:Timer>
                        </ContentTemplate>
                    </asp:UpdatePanel>
 <div id="page-heading" style="width="100%">Access Denied </div>  

 
		
			<div  id="form" style="clear:both;margin-left:15px;"> 
				<p class="text">Sorry, you do not have access to this page. <a href="../Login.aspx" class="text-orange"  ><u>Click here </u></a>  to go to the home page.
				
                    
                </p> 
				
				
			</div> 
</asp:Content>

