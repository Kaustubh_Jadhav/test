﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_access_denied : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
    }
    protected void AutoRefreshTimer_Tick(object sender, EventArgs e)
    {
        try
        {

            Response.Redirect("../login.aspx");

        }
        catch (Exception ex)
        { }
    }
}
