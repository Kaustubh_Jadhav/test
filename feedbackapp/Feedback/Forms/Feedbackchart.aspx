<%@ Page Title="Feedback Chart - Puranik Builders Pvt Ltd." Language="C#" MasterPageFile="~/Master/Main.master" AutoEventWireup="true" CodeFile="Feedbackchart.aspx.cs" Inherits="Forms_Feedbackchart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script language="javascript" type="text/javascript">

  

    function validateSubmit() {

        var txtFrom = document.getElementById('<%=txtFromdt.ClientID %>');
        var txtTo = document.getElementById('<%=txtTodt.ClientID %>');

        if (!empty_chk(txtFrom, txtFrom.value, "Please select from date.")) {
            isContact = 0;
            return false;
        }
        if (!empty_chk(txtTo, txtTo.value, "Please Select to date.")) {
            isContact = 0;
            return false;
        }
        if (txtFrom.value.trim() != '' && txtTo.value.trim() != '') {
            if (txtFrom.value.trim() != txtTo.value.trim()) {
                if (validateBetweenDates(txtFrom.value, txtTo.value)) {
                    alert("To Date must be greater than From Date")
                    return false;
                }
            }
        }
     
    }
    
</script>   
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

          <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
            
<div id="page-heading">Feedback Chart</div>
              
                <div style="clear:both;">
             <asp:UpdatePanel runat="server">
<ContentTemplate>
<asp:Label ID="LblErrorMessage" runat="server" CssClass="text-orange"></asp:Label>
<table width="100%">
<tr><td>
 <table width="100%">
                  
                       
                                    <tr>
                                    <td align="left" height="24px" width="100px">
                                          
                                            <asp:Label ID="Label1" runat="server" Text="Select Project" CssClass="textbold"></asp:Label>
                                        </td>
                                        <td width="220px">
            <asp:DropDownList ID="ddrproject" runat="server" Width="180px">
            </asp:DropDownList>
        </td>
                                        <td align="left" height="24px" width="90px">
                                          
                                            <asp:Label ID="lbl" runat="server" Text="From Date" CssClass="textbold"></asp:Label>
                                        </td>
                                        <td align="left" height="24px" valign="middle" width="150px">
                                            <asp:TextBox ID="txtFromdt" runat="server"
                                                MaxLength="10" ondrop="return false;" onkeypress="return false;"  Width="90px"
                                                onpaste="return false;"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                TargetControlID="txtFromdt">
                                            </asp:CalendarExtender>
                                            <a class="tooltip"  title="Click inside the box to select the date">
                                            <img border="0" height="16" src="../Images/help_icon.gif" width="16" /></a>
                                        </td>
                                        <td width="70px">
                                            <div>
                                                 <asp:Label ID="lblToDate" runat="server" CssClass="textbold"
                                                    Text="To Date"></asp:Label>
                                            </div>
                                        </td>
                                        <td width="140px">
                                            <asp:TextBox ID="txtTodt" runat="server" CssClass="inputTxtSmall"  Width="90px"
                                                MaxLength="10" ondrop="return false;" onkeypress="return false;" 
                                                onpaste="return false;"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" 
                                                TargetControlID="txtTodt">
                                            </asp:CalendarExtender>
                                            <a class="tooltip"  title="Click inside the box to select the date">
                                            <img border="0" height="16" src="../images/help_icon.gif" width="16" /></a>
                                        </td>
                                        
                                        <td align="left">
                                            <asp:Button ID="btnSubmit" runat="server" border="0" 
                                                OnClientClick="return validateSubmit();"  CssClass="Button-bg"
                                                style="height: 30px; border-style: none; margin-right: 30px;" 
                                                Text="Show Report" onclick="btnSubmit_Click" />
                                        </td>
                                    </tr>
                                    
              
                                  
                        </table>
</td></tr>

<tr><td>
<asp:Panel ID="pnl_Chart" runat="server" Visible="false" Width="95%">
    <asp:Chart ID="Chart1" runat="server" ImageLocation="~/document/Images_Chart/ChartPic_#SEQ(300,3)" ImageStorageMode="UseImageLocation" Width="800px"  BackGradientStyle="LeftRight">
        <Series>
            <asp:Series Name="Series1" ChartType="Column" ChartArea="ChartArea1"   LegendText="Yes" LabelBorderColor="DarkMagenta" LabelBackColor="Yellow" LabelForeColor="Black" >
         
                </asp:Series>
    <asp:Series Name="Series2" Color="red" LegendText="No" LabelBorderColor="Yellow" LabelBackColor="DarkMagenta" LabelForeColor="White">
 
 
 
</asp:Series>

           
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" Area3DStyle-IsClustered="true" >
           <AxisX Title="Question" TitleForeColor="DarkOrange">
           </AxisX>
            <AxisY Title="No of Replies" TitleForeColor="DarkOrange"></AxisY>
           
            </asp:ChartArea>
            
        </ChartAreas>
        <BorderSkin SkinStyle="Emboss" />   
      <Legends>
      <asp:Legend Name="legend1"></asp:Legend>
    
      </Legends>
       </asp:Chart>
      </asp:Panel>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>

   <tr>
                     <td align="left"><asp:LinkButton ID="lnkfeedback" runat="server" CssClass="a-link-text-big" OnClick="lnkfeedback_Click" >View Detailed Report</asp:LinkButton></td></tr>

</table>
    <asp:HiddenField ID="hdffromdate" runat="server" />     
       <asp:HiddenField ID="hdftodate" runat="server" />  
        <asp:HiddenField ID="hdfprojectid" runat="server" />        
</ContentTemplate>
</asp:UpdatePanel> 
            </div>     

                                   </asp:Content>