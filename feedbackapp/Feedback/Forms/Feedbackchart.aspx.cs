﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Puraniks;
using System.Text.RegularExpressions;
using System.Data;
using System.Globalization;

public partial class Forms_Feedbackchart : System.Web.UI.Page
{
    DataBase12 obj;
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {


            txtTodt.Text = Convert.ToDateTime(DateTime.UtcNow).ToString("dd/MM/yyyy");
           
            txtFromdt.Text = Convert.ToDateTime(DateTime.UtcNow.AddDays(-30)).ToString("dd/MM/yyyy");
            FillProjectDropDown();
            
        }
            loadChart();
        
        
    }
    protected void FillProjectDropDown()
    {
        try
        {
            DataSet ds = new DataSet();
            obj = new DataBase12();
            ds = obj.GetProjectList();
            ddrproject.DataSource = ds;
            ddrproject.DataTextField = "Projectname";
            ddrproject.DataValueField = "ProjectID";
            ddrproject.DataBind();

            ddrproject.Items.Insert(0, new ListItem("-- Show ALL --", "0"));
           

            //ddrproject.Items.Insert(1, new ListItem("Rumah Bali", "1"));
            //ddrproject.Items.Insert(2, new ListItem("Puranik City", "2"));
            //ddrproject.Items.Insert(3, new ListItem("Puranik Home Town", "3"));
            //ddrproject.Items.Insert(2, new ListItem("Capitol", "4"));
            //ddrproject.Items.Insert(3, new ListItem("Aldea Espanola", "5"));


        }
        catch (Exception ex)
        {
            LblErrorMessage.Text = ex.Message;
        }
    }
    private void loadChart()
    {
        DataBase12 obj = new DataBase12();
        IFormatProvider cult = new CultureInfo("en-us");

        DataSet ds_chart = new DataSet();
        if (txtFromdt.Text.Trim() == "")
        {
            txtFromdt.Text = null;

        }
        else
        { hdffromdate.Value = txtFromdt.Text;
        }

        if (txtTodt.Text.Trim() == "")
        {
            txtTodt.Text = null;

        }
        else
        {
            hdftodate.Value = txtTodt.Text; 
        }
        if (ddrproject.SelectedIndex > 0)
        {

            hdfprojectid.Value = Convert.ToInt32(ddrproject.SelectedItem.Value).ToString();


        }
        else
        { hdfprojectid.Value = "0";
        }
       // hdfprojectid.Value = ddrproject.SelectedIndex.ToString();
        ds_chart = obj.GetFeedBackChartReport(DateTime.ParseExact(txtFromdt.Text.ToString(), "dd/MM/yyyy", cult), DateTime.ParseExact(txtTodt.Text.ToString(), "dd/MM/yyyy", cult), Convert.ToInt32(hdfprojectid.Value));
        if (ds_chart.Tables!=null)
        {
            Chart1.Series["Series1"].XValueMember = "qname";
            Chart1.Series["Series1"].YValueMembers = "yesct";
            Chart1.Series["Series1"].Color = ColorTranslator.FromHtml("#26c48e");
           
            Chart1.Series["Series2"].XValueMember = "qname";
            Chart1.Series["Series2"].YValueMembers = "noct";
            Chart1.Series["Series2"].Color = ColorTranslator.FromHtml("#de3438");


            Chart1.Series["Series1"].IsValueShownAsLabel = true;
            Chart1.Series["Series2"].IsValueShownAsLabel = true;


            Chart1.DataSource = ds_chart;

            Chart1.DataBind();
            pnl_Chart.Visible = true;
           
          

        }
        else
        {

            pnl_Chart.Visible = false;
            LblErrorMessage.Text = "No records found !";           
          
           
        }
        
       
      


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        loadChart();

    }
    protected void lnkfeedback_Click(object sender, EventArgs e)
    {

        Response.Redirect("Feedbackreport.aspx?fromDate=" + hdffromdate.Value + "&todat=" + hdftodate.Value+"&id=" + hdfprojectid.Value);

    }
}
