﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Puraniks;
using System.Text.RegularExpressions;
using System.Data;
using System.Globalization;

public partial class Forms_Feedbackreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        try
        {

           
          
            
             if (!IsPostBack)
             {
                 FillProjectDropDown();
                    txtTodt.Text = Convert.ToDateTime(DateTime.UtcNow).ToString("dd/MM/yyyy");
                    txtFromdt.Text = Convert.ToDateTime(DateTime.UtcNow.AddDays(-30)).ToString("dd/MM/yyyy");
                    if (Request.QueryString["fromDate"] != null)
                    {

                        txtFromdt.Text = Request.QueryString["fromDate"];
                    }

                    if (Request.QueryString["todat"] != null)
                    {
                        txtTodt.Text = Request.QueryString["todat"];

                    }
                    if (Request.QueryString["id"] != null)
                    {
                        //ddrproject.SelectedItem.Text = (Request.QueryString["id"]);
                        ddrproject.SelectedIndex = ddrproject.Items.IndexOf(ddrproject.Items.FindByValue(Request.QueryString["id"]));

                    }


             }
                loadReport();
            

            
        }
        catch (Exception ex)
        {

            Response.Redirect("access-denied.aspx");
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        //ProgressBarModalPopupExtender.Hide();
        try
        {
            loadReport();
        }
        catch (Exception ex)
        { }
    }

    protected void FillProjectDropDown()
    {
        try
        {
            DataSet ds = new DataSet();
            DataBase12 obj = new DataBase12(); 

            ds = obj.GetProjectList();
            ddrproject.DataSource = ds;
            ddrproject.DataTextField = "Projectname";
            ddrproject.DataValueField = "ProjectID";
            ddrproject.DataBind();

            ddrproject.Items.Insert(0, new ListItem("-- Select ALL --", "0"));
           

            //ddrproject.Items.Insert(1, new ListItem("Rumah Bali", "1"));
            //ddrproject.Items.Insert(2, new ListItem("Puranik City", "2"));
            //ddrproject.Items.Insert(3, new ListItem("Puranik Home Town", "3"));
            //ddrproject.Items.Insert(2, new ListItem("Capitol", "4"));
            //ddrproject.Items.Insert(3, new ListItem("Aldea Espanola", "5"));


        }
        catch (Exception ex)
        {
            lblUserRecored.Text = ex.Message;
        }
    }
    private void loadReport()
    {       
        try
        {
            DataBase12 obj = new DataBase12();

            DataSet ds = new DataSet();
            IFormatProvider cult = new CultureInfo("en-us");
            if (txtFromdt.Text.Trim() == "")
            {
                txtFromdt.Text = null;

            }
            else
            {
                hdffromdate.Value = txtFromdt.Text;
            }

            if (txtTodt.Text.Trim() == "")
            {
                txtTodt.Text = null;

            }
            else
            {
                hdftodate.Value = txtTodt.Text;
            }
            if (ddrproject.SelectedIndex > 0)
            {

                hdfprojectid.Value = Convert.ToInt32(ddrproject.SelectedItem.Value).ToString();


            }
            else
            {
                hdfprojectid.Value = "0";
            }
            ds = obj.GetFeedBackReport(DateTime.ParseExact(hdffromdate.Value, "dd/MM/yyyy", cult), DateTime.ParseExact(hdftodate.Value.ToString(), "dd/MM/yyyy", cult),Convert.ToInt32(hdfprojectid.Value));
            if (ds.Tables[0].Rows.Count> 0)
            {
                gvReport.DataSource = ds.Tables[0];
                gvReport.DataBind();
               
                lblUserRecored.Text = ds.Tables[0].Rows.Count +" record/s found";

                
            }
            else
            {
               
                lblUserRecored.Text = "No records found !";
              
               
               
            }
        }
        catch (Exception ex)
        { }
    }
    protected void gvReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvReport.PageIndex = e.NewPageIndex;
            this.loadReport();
        }
        catch (Exception ex)
        {

        }
    }
}
