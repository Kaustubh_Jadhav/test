﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Main : System.Web.UI.MasterPage
{
    string userName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ltr.Text = "Developed By " + "<a href='http://www.cruxbytes.com/' class='a-link-text' target='_blank' >CruxBytes</a>";

        if (Session["UserName"] != null)
        {
            userName = Session["UserName"].ToString();
        }
        else
        {
            Response.Redirect("access-denied.aspx",false);

        }
        if (userName != "")
        {
            ltrlUserName.Text = userName.ToString();

        }
        else
        {
            ltrlUserName.Text = "";
            Response.Redirect("access-denied.aspx",false);
        }



    }
    protected void lnkBtnLogOut_Click(object sender, EventArgs e)
    {

        Session.Clear();
        Session.Abandon();

        Response.Redirect("../login.aspx",false);
    }
}
