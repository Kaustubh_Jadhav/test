﻿
function trim(s) {
    return s.replace(/^\s*/, "").replace(/\s*$/, "");
}
//To check the string is empty
function not_empty_chk(obj_str, str1) {
    if (trim(str1) == "" || trim(str1) == null)
    //if(str1=="" || str1== null)
    {
        return false;
    }
    return true;
}

//To check the string is empty
function empty_chk(obj_str, str1, error_msg) {
    if (trim(str1) == "" || trim(str1) == null) {
        obj_str.focus();
        alert(error_msg)
        return false;
    }
    return true;
}


/*Function used to validate whether Second date is less than First date.*/
function validateBetweenDates(fromDate, toDate) {
    var dt1, dt2;
    var datecurr, date_Entered;
    var strDate, strMonth, strYear, strCurrFullDate, returnCode;

    dt1 = fromDate;
    if (dt1 != null) {
        dt2 = toDate;
        arrCurDate1 = dt1.split("/");
        arrCurDate2 = dt2.split("/");

        strCurrFullDate1 = arrCurDate1[0] + "/" + arrCurDate1[1] + "/" + arrCurDate1[2];
        strCurrFullDate2 = arrCurDate2[0] + "/" + arrCurDate2[1] + "/" + arrCurDate2[2];

        returnCode = fnCompareDate(strCurrFullDate1, strCurrFullDate2);
        if (1 == returnCode) {
            return true;
        }
        else if ((0 == returnCode) || (-1 == returnCode)) {
            return false;
        }
    }
}

/*Function used to compare the date entered in the Textbox with current date.*/
function fnCompareDate(Date1, Date2) {
    var arrDate1, arrDate2, dtDate1, dtDate2, intDtCmp
    intDtCmp = 0;
    if ((Date1 != "") && (Date2 != "")) {
        arrDate1 = Date1.split("/"); //splits the date entered in Textbox "-".
        arrDate2 = Date2.split("/"); //splits the current date with "-".

        dtDate1 = new Date(arrDate1[2], arrDate1[1], arrDate1[0]);
        dtDate2 = new Date(arrDate2[2], arrDate2[1], arrDate2[0]);

        if ((dtDate1.getDate() == dtDate2.getDate()) && (dtDate1.getMonth() == dtDate2.getMonth()) && (dtDate1.getFullYear() == dtDate2.getFullYear())) {
            intDtCmp = 1;
        }
        else if (dtDate1.getFullYear() < dtDate2.getFullYear()) {
            intDtCmp = -1;
        }
        else if ((dtDate1.getMonth() < dtDate2.getMonth()) && (dtDate1.getFullYear() == dtDate2.getFullYear())) {
            intDtCmp = -1;
        }
        else if ((dtDate1.getDate() < dtDate2.getDate()) && (dtDate1.getFullYear() == dtDate2.getFullYear()) && ((dtDate1.getMonth() == dtDate2.getMonth()))) {
            intDtCmp = -1;
        }
        else {
            intDtCmp = 1;
        }
    }
    return intDtCmp;
}


/**
* DHTML textbox character counter script. 
*/
//var maxL=500;
var infoText = "Characters Remaining: ";
var bName = navigator.appName;
function taLimit(taObj, maxL) {
    if (taObj.value.length >= maxL) return false;
    return true;
}

function taCount(taObj, Cnt, maxL) {
    objCnt = createObject(Cnt);
    objVal = taObj.value;
    if (objVal.length > maxL) {
        objVal = objVal.substring(0, maxL);
        taObj.value = objVal;
        alert("You have reached the text limit specified. The excess text, if any, has been truncated.");
    }
    if (objCnt) {
        if (bName == "Netscape") {
            objCnt.textContent = infoText + (maxL - objVal.length);
        }
        else { objCnt.innerText = infoText + (maxL - objVal.length); }
    }
    return true;
}
function taCountRuntime(taObjVal, Cnt, maxL) {
    objCnt = createObject(Cnt);
    objVal = taObjVal;
    if (objVal.length > maxL) objVal = objVal.substring(0, maxL);
    if (objCnt) {
        if (bName == "Netscape") {
            objCnt.textContent = infoText + (maxL - objVal.length);
        }
        else { objCnt.innerText = infoText + (maxL - objVal.length); }
    }
    return true;
}
function createObject(objId) {
    if (document.getElementById) return document.getElementById(objId);
    else if (document.layers) return eval("document." + objId);
    else if (document.all) return eval("document.all." + objId);
    else return eval("document." + objId);
}

