﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login"  Title="Login - Puranik Builders Pvt Ltd." %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<html>
<head>    
    <title>Login - Puranik Builders Pvt Ltd. </title>
    <link  href="css/StyleSheet.css" type="text/css" rel="Stylesheet" />
         <link  href="css/StyleSheet.css" type="text/css" rel="Stylesheet" />
<script type="text/javascript" language="javascript"> 
    function validateLogin() {
            var txtUserName = document.getElementById('<%=txtUserName.ClientID%>');
            var txtPassword = document.getElementById('<%=txtPassword.ClientID%>');
            var lblUserNameErr = document.getElementById('<%=lblUserNameErr.ClientID%>');
            var lblPasswordErr = document.getElementById('<%=lblPasswordErr.ClientID%>');
            var HasError = false;
            var lblMsg = document.getElementById('<%=lblMsg.ClientID%>');
            lblMsg.innerHTML = "";
            
            if (txtUserName.value == "") {
                lblUserNameErr.innerHTML = "Enter User Name";
                HasError = true;
                txtUserName.focus();
            }
            else {
                lblUserNameErr.innerHTML = "";                                 
            }

            if (txtPassword.value == "") {
                lblPasswordErr.innerHTML = "Enter Password";
                HasError = true;                
            }
            else {
                lblPasswordErr.innerHTML = "";                               
            }
            if (HasError == true) {
                return false;
            }
            else {
                return true;            
            }         
        }
</script>   
     
</head>

<body><form runat="server">

          <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
          <table width="100%" cellpadding="0px" cellspacing="0px" border="0px" id="wapper">
     <tr>
    <td style="background-color:#fff;">
    <table width="90%;" cellpadding="0px" cellspacing="0px" border="0px">
    <tr>
    <td colspan="3" 
            style="padding-left:20px;vertical-align:bottom;padding-top:5px;">
       
   &nbsp;<img src="Images/logo.jpg" alt="Client Logo" title="Logo" border="0px" />
    </td>
 <td style="width:90%;">
       
  </td>
   </tr>

    </table>    
    </td>
    </tr>
    <tr>
    <td colspan="3" class="menu" valign="bottom";>
     <table width="100%" cellpadding="0px" cellspacing="0px" border="0px">
    <tr>
    <td style="padding-top:15px;">
    &nbsp;
    </td>
    </tr>
<tr>
    <td colspan="3" style="padding-left:12px;padding-right:12px;">
    <table width="100%" border="0px" cellpadding="0px" cellspacing="0px">
    <tr style="vertical-align:top;">
    <td class="bg-top-left">   
    &nbsp;
    </td>
    <td class="bg-top">     
    &nbsp;
    </td>
    <td class="bg-top-right">
    &nbsp;
    </td>
    </tr>
    <tr style="height:17px;">
    <td class="bg-left">
    &nbsp;
    </td>
    <td align="left" valign="top">
    <div style="clear:both;">
    
    
    
   
    
   
   
    <div id="ui-MainContent">    
        
        <table border="0px" cellpadding="0px" cellspacing="0" width="100%" class="rounded-corners">
            <tr>
                <td>
                   <div id="page-heading">Login To Puranik Builders Pvt Ltd.</div></td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-top:10px;">
                    <span>
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-orange" Text=""></asp:Label>
                    </span>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" valign="bottom">
                    <table  border="0px" cellpadding="0px" cellspacing="0" 
                        style="width: 54%; margin-right: 0px;"
                        >
                        <tr>
                            <td align="left" style="padding-top:15px;padding-left:40px;" valign="middle">
                                <b>
                                <asp:Label ID="lblUserName" runat="server" Text="Username" CssClass="txt"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                             <td align="left" style="padding-left:30px;" valign="middle">
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="bigTxt" MaxLength="50" 
                                    Text=""></asp:TextBox>
                                <asp:Label ID="lblUserNameErr" runat="server" CssClass="text-orange"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-top:15px;padding-left:40px;" valign="middle">
                                <b>
                                <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="txt"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-left:30px;">
                                <asp:TextBox ID="txtPassword" runat="server" autocomplete="off" CssClass="bigTxt"
                                     MaxLength="20" Text="" TextMode="Password"></asp:TextBox>
                                <asp:Label ID="lblPasswordErr" runat="server" CssClass="text-orange"></asp:Label>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                         
                            <td align="left" style="padding-left:40px;">
                                <asp:Button ID="btnLogin" runat="server" CssClass="Button-bg" Width="100px"
                                   Height="32px" onclick="btnLogin_Click" OnClientClick="return validateLogin();" Text="Login" />
                            </td>
                        </tr>
                       
                    </table>
                </td>
            </tr>
        </table>
        
    </div> 
   
 
  
  
    
    
    
    
    
    
    
   
    </div>
    </td>
    <td  class="bg-right">
    
    </td>
    </tr>
    
    <tr>
    <td class="bg-bottom-left">
    &nbsp;
    </td>
    <td class="bg-bottom">
    &nbsp;
    </td>
    <td class="bg-bottom-right">
    &nbsp;
    </td>    
    </tr>    
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="3">&nbsp;</td>
    </tr>
    <tr>     
    <td colspan="3">
    <table width="100%" cellpadding="0px" cellspacing="0px" border="0px">
    <tr>
    <td valign="bottom" style="width:450px;">
    <div class="footer-left">&nbsp;</div>
    </td>
    
    <td style="width:147px;">
    <img src="images/footer-middle.png" alt="" style="vertical-align:bottom;"/>
    </td>
    
    <td valign="bottom">
    <div class="footer-right" style="text-align:right;"><br /><span class="footer-text" ><asp:literal ID="ltr" runat="server" Text=""></asp:literal></span> </div>
    
    </td>
    </tr>
    </table>
    </td>    
    </tr>
    </table>
     </td>
     </tr>
     </table>
     </ContentTemplate>
     </asp:UpdatePanel>
     
    </form>
    </body>
    </html>
  