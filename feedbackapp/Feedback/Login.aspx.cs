﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
public partial class Login : System.Web.UI.Page
{
    string username, pwd;
    protected void Page_Load(object sender, EventArgs e)
    {


        ltr.Text = "Developed By " + "<a href='http://www.cruxbytes.com/' class='a-link-text' target='_blank' >CruxBytes</a>";
        username = ConfigurationManager.AppSettings["UserName"].ToString();
        pwd = ConfigurationManager.AppSettings["Password"].ToString();
        txtUserName.Focus();
       
    }
    public bool validate()
    {

        bool bStatus = true;
        if (txtUserName.Text == "")
        {

           lblMsg.Text= "Please enter Name";
            txtUserName.Focus();
            bStatus = false;
        }
        else if (txtUserName.Text.ToLower()!= username.ToLower().ToString())
        {

            lblMsg.Text = "Please enter correct UserName";
            txtUserName.Focus();
            bStatus = false;
        }
        
        if (txtPassword.Text.Trim() == "")
        {

         lblMsg.Text= "Please enter Password";
            txtPassword.Focus();
            bStatus = false;
        }
        else if (txtPassword.Text.ToLower() != pwd.ToLower().ToString())
        {

           lblMsg.Text= "Please enter correct Password";
            txtPassword.Focus();
            bStatus = false;
        }
      
       
       
        return bStatus;
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (validate())
        {
            Session["UserName"] = txtUserName.Text;
            Response.Redirect("Forms/Feedbackchart.aspx");
    
        }
        //else
        //{

        //    Response.Redirect("Login.aspx");
        //}
    }
}
