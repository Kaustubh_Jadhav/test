﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Xml;
using System.Configuration;
using Microsoft.Win32;

namespace Puraniks
{
    public class DBManager : IDisposable, Puraniks.IDbManager
    {
        /// <summary>
        /// Manager Class to interact with the database.
        /// </summary>
        #region /******Fields******/
        private SqlConnection SqlConnection;
        private SqlDataReader SqlDataReader;
        private SqlCommand SqlCommand;
        private SqlTransaction SqlTransaction = null;
        private SqlParameter[] idbParameters = null;
        private string strConnection;
        private XmlReader XmlDataReader;

        private System.Collections.Generic.List<SqlParameter> _listParam = null;
        #endregion
        //Constructor
        public DBManager()
        {
            GetConnectionString();
        }

        public void GetConnectionString()
        {
            strConnection = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        }
        #region /****Properties*****/
        public SqlConnection Connection
        {
            get
            {
                return SqlConnection;
            }
        }

        public SqlDataReader DataReader
        {
            get
            {
                return SqlDataReader;
            }
            set
            {
                SqlDataReader = value;
            }
        }
        public XmlReader XMLDataReader
        {
            get
            {
                return this.XmlDataReader;
            }
            set
            {
                this.XmlDataReader = value;
            }
        }

        public string ConnectionString
        {
            get
            {
                return strConnection;
            }
            set
            {
                strConnection = value;
            }
        }

        public SqlCommand Command
        {
            get
            {
                return SqlCommand;
            }
        }

        public SqlTransaction Transaction
        {
            get
            {
                return SqlTransaction;
            }
        }

        public SqlParameter[] Parameters
        {
            get
            {
                return idbParameters;
            }
        }
        #endregion

        #region /******Methods******/

        //Initializes the parameters in the parameter array  
        public void AddAnsiStringParameters(int index, string paramName, object objValue)
        {
            if (index < idbParameters.Length)
            {
                idbParameters[index].ParameterName = paramName;
                idbParameters[index].Value = objValue;
                idbParameters[index].DbType = DbType.AnsiString;
            }
        }

        //Initializes the parameters in the parameter array  
        public void AddParameters(int index, string paramName, object objValue)
        {
            if (index < idbParameters.Length)
            {
                idbParameters[index].ParameterName = paramName;
                idbParameters[index].Value = objValue;
            }
        }

        //Overload for Add parameters to avoid Index. 
        public void AddParameters(string paramName, object objValue)
        {
            SqlParameter _param = new SqlParameter();
            _param.ParameterName = paramName;
            _param.Value = objValue;

            this._listParam.Add(_param);
        }

        //Begins a Transaction on the connection
        public void BeginTransaction()
        {
            if (this.SqlTransaction == null)
                SqlTransaction = this.SqlConnection.BeginTransaction();
            this.SqlCommand.Transaction = SqlTransaction;
        }

        //Closes the connection to the database
        public void Close()
        {
            if (SqlConnection.State != ConnectionState.Closed)
                SqlConnection.Close();
        }

        //Closes the reader
        public void CloseReader()
        {
            if (this.DataReader != null)
                this.DataReader.Close();
        }

        //Commits the Transaction
        public void CommitTransaction()
        {
            if (this.SqlTransaction != null)
                this.SqlTransaction.Commit();
            SqlTransaction = null;
        }

        //Creates the specified no. of parameters 
        public void CreateParameters(int paramsCount)
        {
            idbParameters = new SqlParameter[paramsCount];
            idbParameters = DBManagerFactory.GetParameters(paramsCount);
        }

        //Creates the parameters. OverLoad to avoid Index. 
        public void CreateParameters()
        {
            _listParam = new System.Collections.Generic.List<SqlParameter>();
        }

        //Creates the parameters. OverLoad to avoid Index. 
        public void ParametersToArray()
        {
            idbParameters = _listParam.ToArray();
        }

        //Disposes the DBManager object
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Close();
            this.SqlCommand = null;
            this.SqlTransaction = null;
            this.SqlConnection = null;
        }

        public DataSet ExecuteDataSet(CommandType commandType, string commandText)
        {
            this.SqlCommand = DBManagerFactory.GetCommand();
            PrepareCommand(SqlCommand, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            IDbDataAdapter dataAdapter = DBManagerFactory.GetDataAdapter();
            dataAdapter.SelectCommand = SqlCommand;
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            SqlCommand.Parameters.Clear();
            return dataSet;
        }

        //Executes a command and returns the no. of rows affected
        public int ExecuteNonQuery(CommandType commandType, string commandText)
        {
            this.SqlCommand = DBManagerFactory.GetCommand();
            PrepareCommand(SqlCommand, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            int returnValue = SqlCommand.ExecuteNonQuery();
            SqlCommand.Parameters.Clear();
            return returnValue;
        }

        //Executes a command and returns a reader 
        public SqlDataReader ExecuteReader(CommandType commandType, string commandText)
        {
            this.SqlCommand = DBManagerFactory.GetCommand();
            SqlCommand.Connection = this.Connection;
            PrepareCommand(SqlCommand, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            this.DataReader = SqlCommand.ExecuteReader();
            SqlCommand.Parameters.Clear();
            return this.DataReader;
        }

        //Executes a command and returns the first cell of the resultset
        public object ExecuteScalar(CommandType commandType, string commandText)
        {
            this.SqlCommand = DBManagerFactory.GetCommand();
            PrepareCommand(SqlCommand, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            object returnValue = SqlCommand.ExecuteScalar();
            SqlCommand.Parameters.Clear();
            return returnValue;
        }

        public XmlReader ExecuteXMLReader(CommandType commandType, string commandText)
        {
            this.SqlCommand = DBManagerFactory.GetCommand();
            SqlCommand.Connection = this.Connection;
            PrepareCommand(SqlCommand, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            this.XMLDataReader = SqlCommand.ExecuteXmlReader();
            SqlCommand.Parameters.Clear();
            return this.XmlDataReader;
        }

        //Opens a connection to the database   
        public void Open()
        {
            try
            {
                SqlConnection = DBManagerFactory.GetConnection();
                SqlConnection.ConnectionString = strConnection;
                if (SqlConnection.State != ConnectionState.Open)
                    SqlConnection.Open();
                this.SqlCommand = DBManagerFactory.GetCommand();
                this.SqlCommand.CommandTimeout = 1000;
            }
            catch (Exception ex)
            {
            }
        }

        //Rolls back the Transaction
        public void RollBackTransaction()
        {
            if (this.SqlTransaction != null)
                this.SqlTransaction.Rollback();
            SqlTransaction = null;
        }

        public void CloseReader(SqlDataReader objReader)
        {
            //check the status
            if (objReader != null)
            {
                //close the reader
                if (!objReader.IsClosed)
                    objReader.Close();
            }
        }

        //Helper method which attached parameters to a command
        private void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            foreach (SqlParameter idbParameter in commandParameters)
            {
                if ((idbParameter.Direction == ParameterDirection.InputOutput) && (idbParameter.Value == null))
                {
                    idbParameter.Value = DBNull.Value;
                }
                command.Parameters.Add(idbParameter);
            }
        }

        //Helper method which initializes a command and prepares it for execution
        private void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters)
        {
            command.Connection = connection;
            command.CommandText = commandText;
            command.CommandType = commandType;

            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
        }



        #endregion


        #region IDisposable Members

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDbManager Members

        void IDbManager.AddAnsiStringParameters(int index, string paramName, object objValue)
        {
            throw new NotImplementedException();
        }

        void IDbManager.AddParameters(int index, string paramName, object objValue)
        {
            throw new NotImplementedException();
        }

        void IDbManager.BeginTransaction()
        {
            throw new NotImplementedException();
        }

        void IDbManager.Close()
        {
            throw new NotImplementedException();
        }

        void IDbManager.CloseReader()
        {
            throw new NotImplementedException();
        }

        void IDbManager.CommitTransaction()
        {
            throw new NotImplementedException();
        }

        void IDbManager.CreateParameters(int paramsCount)
        {
            throw new NotImplementedException();
        }

        void IDbManager.Dispose()
        {
            throw new NotImplementedException();
        }

        DataSet IDbManager.ExecuteDataSet(CommandType commandType, string commandText)
        {
            throw new NotImplementedException();
        }

        int IDbManager.ExecuteNonQuery(CommandType commandType, string commandText)
        {
            throw new NotImplementedException();
        }

        SqlDataReader IDbManager.ExecuteReader(CommandType commandType, string commandText)
        {
            throw new NotImplementedException();
        }

        object IDbManager.ExecuteScalar(CommandType commandType, string commandText)
        {
            throw new NotImplementedException();
        }

        XmlReader IDbManager.ExecuteXMLReader(CommandType commandType, string commandText)
        {
            throw new NotImplementedException();
        }

        void IDbManager.Open()
        {
            throw new NotImplementedException();
        }

        void IDbManager.RollBackTransaction()
        {
            throw new NotImplementedException();
        }

        void IDbManager.CloseReader(SqlDataReader dreaderObj)
        {
            throw new NotImplementedException();
        }

        void IDbManager.GetConnectionString()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public sealed class DBManagerFactory
    {
        private DBManagerFactory() { }

        //Gets a sqlclient connection to the database
        public static SqlConnection GetConnection()
        {
            SqlConnection SqlConnection = null;
            SqlConnection = new SqlConnection();
            return SqlConnection;
        }

        //Returns a Sql Command Object 
        public static SqlCommand GetCommand()
        {
            return new SqlCommand();
        }

        //Returns a SqlParameter
        public static IDataParameter GetParameter()
        {
            IDataParameter iDataParameter = null;
            iDataParameter = new SqlParameter();
            return iDataParameter;
        }

        //Returns a SqlParameter array containing the specified no. of parameters
        public static SqlParameter[] GetParameters(int paramsCount)
        {
            SqlParameter[] idbParams = new SqlParameter[paramsCount];

            for (int i = 0; i < paramsCount; ++i)
            {
                idbParams[i] = new SqlParameter();
            }

            return idbParams;
        }

        public static IDbDataAdapter GetDataAdapter()
        {
            return new SqlDataAdapter();
        }

    }
}
