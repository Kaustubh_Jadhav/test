﻿using System;

namespace Puraniks
{
    interface IDbManager
    {
        void AddAnsiStringParameters(int index, string paramName, object objValue);
        void AddParameters(int index, string paramName, object objValue);
        void BeginTransaction();
        void Close();
        void CloseReader();
        void CommitTransaction();
        void CreateParameters(int paramsCount);
        void Dispose();
        System.Data.DataSet ExecuteDataSet(System.Data.CommandType commandType, string commandText);
        int ExecuteNonQuery(System.Data.CommandType commandType, string commandText);
        System.Data.SqlClient.SqlDataReader ExecuteReader(System.Data.CommandType commandType, string commandText);
        object ExecuteScalar(System.Data.CommandType commandType, string commandText);
        System.Xml.XmlReader ExecuteXMLReader(System.Data.CommandType commandType, string commandText);
        void Open();
        void RollBackTransaction();
        void CloseReader(System.Data.SqlClient.SqlDataReader dreaderObj);
        void GetConnectionString();
    }
}
