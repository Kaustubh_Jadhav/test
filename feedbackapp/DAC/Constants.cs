﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Puraniks;
namespace Puraniks
{
   public class Constants
    {
       public const string MAP_Key = "2010";
       public const string Email_Registration_From = "Cruxbytes";
       public enum ProjectType
       {
          RumahBali = 1,
          PuranikCity = 2,
          PuranikHomeTown = 3,
          Capitol = 4,
          AldeaEspanola = 5
       }
    }
}
