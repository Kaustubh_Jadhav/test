﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;

/// <summary>
/// Summary description for SendEmail
/// </summary>
public class SendEmail
{
	public SendEmail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendMail(SystemAlerts objSysAlerts)
    {
        string msg = "";
        MailMessage objEmailMsg = new MailMessage();
        MailAddressCollection sendToID = new MailAddressCollection();
        try
        {
            string eMailSmtpClient;
            string eMailSenderID;
            string eMailSenderPassword;
           
            eMailSmtpClient = ConfigurationSettings.AppSettings["EMAIL_SMTPCLIENT"];
            eMailSenderID = ConfigurationSettings.AppSettings["EMAIL_SENDERID"];
            eMailSenderPassword = ConfigurationSettings.AppSettings["EMAIL_SENDERPassword"];



           
            string[] sendEmailIDs = objSysAlerts.EmailID.Split(';');
            foreach (string toID in sendEmailIDs)
            {
                objEmailMsg.To.Add(toID);
            }
            if (objSysAlerts.Cc != null)
            {
                objEmailMsg.CC.Add(objSysAlerts.Cc);
            }
            if (objSysAlerts.BCC != null)
            {
                objEmailMsg.Bcc.Add(objSysAlerts.BCC);
            }


            if (objSysAlerts.FromDisplayName == "" || objSysAlerts.FromDisplayName == null)
                objSysAlerts.FromDisplayName = ConfigurationSettings.AppSettings["EmailAlias"];

            objEmailMsg.From = new MailAddress(eMailSenderID, objSysAlerts.FromDisplayName.ToString());

           

            if (!string.IsNullOrEmpty(objSysAlerts.ReplyToEmailID))
            {
                objEmailMsg.ReplyTo = new MailAddress(objSysAlerts.ReplyToEmailID.ToString(), objSysAlerts.FromDisplayName.ToString());
            }


            string env = ConfigurationSettings.AppSettings["Environment"].ToString().ToUpper();
            if (env == "LIVE" || env == "")
                objEmailMsg.Subject = objSysAlerts.Subject;
            else
                objEmailMsg.Subject = env + " - " + objSysAlerts.Subject;

            objEmailMsg.Body = objSysAlerts.Body;
            objEmailMsg.IsBodyHtml = true;

            if (objSysAlerts.fileAttachment != null)
            {
                if (!(objSysAlerts.fileAttachment.ToString().Equals("")))
                {
                    Attachment atc = new Attachment(objSysAlerts.fileAttachment.ToString());
                   
                    objEmailMsg.Attachments.Add(atc);
                }
            }
            //objEmailMsg.AlternateViews.Add(htmlView);
            SmtpClient emailClient;
            string port = ConfigurationSettings.AppSettings["Port"].ToString();
            if (port != "")
            {
                emailClient = new SmtpClient(eMailSmtpClient, Convert.ToInt16(port.ToString()));
                //emailClient.EnableSsl = true;
            }
            else
            {
                emailClient = new SmtpClient(eMailSmtpClient);
            }
            emailClient.Credentials = new System.Net.NetworkCredential(eMailSenderID, eMailSenderPassword);

            string EmailSSL = ConfigurationSettings.AppSettings["EmailSSL"].ToString();
            if (EmailSSL == "1")
                emailClient.EnableSsl = true;

            emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            if (System.Configuration.ConfigurationSettings.AppSettings["ByPassEmail"].ToString() != "1")
                try
                {
                    emailClient.Send(objEmailMsg);
                }
                catch (Exception ex)
                {
                    objSysAlerts.msg = "ERROR: " + ex.Message;
                    msg = objSysAlerts.msg;
                     return -999;
                }

            return 1;
        }
        catch (Exception ex)
        {
            objSysAlerts.msg = "ERROR: " + ex.Message;
            msg = objSysAlerts.msg;
            return -999;
        }
        finally
        {
            objEmailMsg.Dispose();
        }
    }
}

public class SystemAlerts
{
    public int QueryID { get; set; }
    public string EmailID { get; set; }
    public string MobileNo { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public int UserType { get; set; }
    public int AlertType { get; set; }
    public int AlertedUserID { get; set; }
    public string Sender { get; set; }
    public string Cc { get; set; }
    public string SMSBody { get; set; }
    public string BCC { get; set; }
    public string fileAttachment { get; set; }
    public string DateTimeStamp { get; set; }
    public int CreatedBy { get; set; }
    public string msg { get; set; }
    public string FromDisplayName { get; set; }
    public string ReplyToEmailID { get; set; }
}
