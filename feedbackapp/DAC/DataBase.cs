﻿using System;
using System.Collections.Generic;
using System.Web;
using Puraniks;
using System.Data;
/// <summary>
/// Summary description for DataBase
/// </summary>
namespace Puraniks
{
    public class DataBase12
    {
        private DBManager objDbMgr;
        int nResult;
        public DataBase12()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public int Save_FeedBackDetails(int FeedBackID, string name, string ContactNo, string EmaildID, int Q1, int Q2, int Q3, int Q4, string Q5, int ProjectID)
        {
            try
            {
                objDbMgr = new DBManager();
                objDbMgr.Open();

                objDbMgr.CreateParameters(10);
                objDbMgr.AddParameters(0, "@FeedBackID", FeedBackID);
                objDbMgr.AddParameters(1, "@name", name);
                objDbMgr.AddParameters(2, "@ContactNo", ContactNo);
                objDbMgr.AddParameters(3, "@EmaildID", EmaildID);
                objDbMgr.AddParameters(4, "@Q1", Q1);
                objDbMgr.AddParameters(5, "@Q2", Q2);
                objDbMgr.AddParameters(6, "@Q3", Q3);
                objDbMgr.AddParameters(7, "@Q4", Q4);
                objDbMgr.AddParameters(8, "@Q5", Q5);
                objDbMgr.AddParameters(9, "@ProjectID", @ProjectID);

                nResult = Convert.ToInt32(objDbMgr.ExecuteScalar(System.Data.CommandType.StoredProcedure, "usp_FeedBackDetail"));

                return nResult;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                objDbMgr.Close();
                objDbMgr.Dispose();
            }
        }


        public DataSet GetFeedBackChartReport(DateTime fromDate, DateTime toDate, int ProjectID)
        {
            objDbMgr = new DBManager();
            objDbMgr.Open();

            DataSet ds;

            try
            {
                //ds = objDbMgr.ExecuteDataSet(System.Data.CommandType.Text, "select dbo.ufn_MAFormatdate(createdtimestamp) [RegOn], count(*) [Users] from mausers where createdtimestamp > '2011-01-01' group by dbo.ufn_MAFormatdate(createdtimestamp) order by dbo.ufn_MAFormatdate(createdtimestamp)");

                objDbMgr.CreateParameters(3);
                objDbMgr.AddParameters(0, "FromDate", fromDate);
                objDbMgr.AddParameters(1, "ToDate", toDate);
                objDbMgr.AddParameters(2, "@ProjectID", ProjectID);


                ds = objDbMgr.ExecuteDataSet(System.Data.CommandType.StoredProcedure, "usp_GetFeedbackReport");

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                objDbMgr.Close();
                objDbMgr.Dispose();
            }

            return ds;
        }



        public DataSet GetFeedBackReport(DateTime fromDate, DateTime toDate, int ProjectID)
        {
            objDbMgr = new DBManager();
            objDbMgr.Open();

            DataSet ds;

            try
            {
                //ds = objDbMgr.ExecuteDataSet(System.Data.CommandType.Text, "select dbo.ufn_MAFormatdate(createdtimestamp) [RegOn], count(*) [Users] from mausers where createdtimestamp > '2011-01-01' group by dbo.ufn_MAFormatdate(createdtimestamp) order by dbo.ufn_MAFormatdate(createdtimestamp)");

                objDbMgr.CreateParameters(3);
                objDbMgr.AddParameters(0, "FromDate", fromDate);
                objDbMgr.AddParameters(1, "ToDate", toDate);
                objDbMgr.AddParameters(2, "@ProjectID", ProjectID);

                ds = objDbMgr.ExecuteDataSet(System.Data.CommandType.StoredProcedure, "usp_GetFeedbackDetailReport");

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                objDbMgr.Close();
                objDbMgr.Dispose();
            }

            return ds;
        }

    
        public DataSet GetProjectList()
        {
            objDbMgr = new DBManager();
            objDbMgr.Open();

            DataSet ds;

            try
            {


                ds = objDbMgr.ExecuteDataSet(System.Data.CommandType.StoredProcedure, "usp_GetProjectList");

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                objDbMgr.Close();
                objDbMgr.Dispose();
            }

            return ds;
        }

  
      
    }
}